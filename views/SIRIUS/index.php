<?php

include_once "../../vendor/autoload.php";


if(!isset($_SESSION))session_start();

use App\Message\Message;
use App\Utility\Utility;

$objDept = new App\Department\Department();

$deptData = $objDept->section();

/*
$obj = new User();
$obj->setData($_SESSION);

$singleUser = $obj->view();
*/
$admin = new App\Admin\Auth();
$status = $admin->setData($_SESSION)->is_registered();
$login = $admin->setData($_SESSION)->logged_in();


$objDept = new App\Department\Department();
$deptData = $objDept->section();


?>

<!DOCTYPE html>
<html lang="en"><head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>SIRIUS CARE</title>
    <link rel="icon" type="image/png" sizes="32x32" href="img/icon.png">

    <!-----------------Global CSS ----------------------->

    <link rel="stylesheet" type="text/css" href="css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="css/font-awesome.css">
    <link rel="stylesheet" type="text/css" href="css/animate.css">
    <link rel="stylesheet" type="text/css" href="css/all.css">
    <link rel="stylesheet" type="text/css" href="css/ihover.min.css">
    <link rel="stylesheet" type="text/css" href="css/lightbox.css">
    <link rel="stylesheet" type="text/css" href="css/select2.css">
    <link rel="stylesheet" type="text/css" href="css/bootstrap-datetimepicker.css">
    <link rel="stylesheet" type="text/css" href="css/swiper.css">
    <link rel="stylesheet" type="text/css" href="css/jquery.css">
    <link rel="stylesheet" type="text/css" href="css/layers.css">
    <link rel="stylesheet" type="text/css" href="css/navigation.css">
    <link rel="stylesheet" type="text/css" href="css/settings.css">
    <link rel="stylesheet" type="text/css" href="css/sweetalert.css">

    <!------------------Custom CSS --------------------------->

    <link rel="stylesheet" type="text/css" href="css/custom.css">

<script type="text/javascript" charset="UTF-8" src="js/common.js"></script>
<script type="text/javascript" charset="UTF-8" src="js/map.js"></script>
<script type="text/javascript" charset="UTF-8" src="js/util.js"></script>
<script type="text/javascript" charset="UTF-8" src="js/geocoder.js"></script>
<script type="text/javascript" charset="UTF-8" src="js/onion.js"></script>
<script type="text/javascript" charset="UTF-8" src="js/stats.js"></script>
<script type="text/javascript" charset="UTF-8" src="js/controls.js"></script>
<script type="text/javascript" charset="UTF-8" src="js/marker.js"></script>

</head>
<body>
<!--------------------Start of Preloader-------------------->

<div class="preloader" style="position: fixed; width: 100%; height: 100%; top: 0px; left: 0px; z-index: 100000; backface-visibility: hidden; background: rgb(255, 255, 255) none repeat scroll 0% 0%; display: none;">
    <div style=" width: 50px;
    height: 50px;
    position: absolute;
    left: 43%;
    top: 50%;
    background-position: center;
    margin:-25px 0 0 -25px;">
        <img src="img/loader.gif" alt="loading..." style="display: none;">
    </div>
</div>
<!-----------------End of Preloader------------------------->


<!----------------Start of Header-------------------------->
<div class="tp-banner">
    <div class="container">
        <div class="row">
            <div class="col-md-7 col-sm-4 col-xs-5 p-d-12 font13">
                <a href="#" class="p-d-10 text-white"><i class="fa fa-map-marker" aria-hidden="true"></i>
                    <span class="hidden-xs hidden-sm">Chittagong, Bangladesh</span></a>
                <a href="#" class="p-d-10 text-white"><i class="fa fa-phone" aria-hidden="true"></i>
                    <span class="hidden-xs hidden-sm">031-654732</span> </a>
                <a href="#" class="p-d-10 text-white"><i class="fa fa-envelope" aria-hidden="true"></i>
                    <span class="hidden-xs hidden-sm">info@sirius.org</span></a>
            </div>
            <div class="col-md-5 col-sm-8 col-xs-7 text-right">
                <div class="row">
                    <div class="col-md-6 col-sm-7 p-d-10 font13 hidden-xs">
                        <span><?php
                            if($status || $login){

                                echo '<span class="text-white hidden-xs">WELCOME <?php $_SESSION[\'email\'] ?></span>';
                            }else{
                              echo   '<a href="#" class="text-white" data-toggle="modal" data-target="#signup">
                                      <i class="fa fa-user"></i>
                                      <span class="hidden-xs">Login / Sign Up</span>
                                      </a>';
                            }
                            ?>
                        </span>
                    </div>
                    <div class="col-md-6 col-sm-5 header_top">
                        <ul class="tp-banner_social_icons list-inline">
                            <li class="facebook_hvr">
                                <a href="#"><i class="fa fa-facebook text-center"></i></a>
                            </li>
                            <li class="twitter_hvr">
                                <a href="#"><i class="fa fa-twitter text-center"></i></a>
                            </li>
                            <li class="gplus_hvr">
                                <a href="#"><i class="fa fa-google-plus text-center"></i></a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



<header class="header1_align header1">
    <!--<div class="container">-->
    <nav class="navbar navbar-default">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="navbar-header">
                        <button class="navbar-toggle" type="button" data-toggle="collapse" data-target="#myMegamenu"><span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="#">
                            <img src="img/logo.png" class="img-responsive m-t-n5" alt="medical-logo">
                        </a>
                    </div>
                    <div class="collapse navbar-collapse navbar-right" id="myMegamenu">
                        <div class="header_nav header1_nav">
                            <ul class="nav navbar-nav">
								 <li class="list2 mont"><a href="index.php">HOME</a></li>
                                 <!--<li class="dropdown blog_dropdown"><a href="#" class="dropdown-toggle menu_hover" data-toggle="dropdown">
                                    <span class="mont">HOME</span><span class="CAREt"></span></a>
                                   <ul class="sub_menu dropdown-menu blog_menu">
                                        <li>
                                            <a href="#">Home Page-1</a>
                                        </li>
                                        <li>
                                            <a href="#">Home Page-2</a>
                                        </li>
                                        <li>
                                            <a href="#">Home Page-3</a>
                                        </li>
                                        <li>
                                            <a href="#">Home Page-4</a>
                                        </li>
                                        <li>
                                            <a href="#">Home Page-5</a>
                                        </li>
                                        <li>
                                            <a href="#">Home Page-6</a>
                                        </li>
                                    </ul>
                                </li>-->
                                <li class="list2 mont"><a href="#about">ABOUT US</a></li>

                                <!--<li class="dropdown mega-dropdown"><a href="#" class="dropdown-toggle dept_menu" data-toggle="dropdown">
                                    <span class="mont">SIRIUS HUB </span> <span class="CAREt"></span></a>
                                    <ul class="row sub_menu dropdown-menu mega-dropdown-menu">
                                        <li class="col-sm-3">
                                            <ul>
                                                <li class="dropdown-header header_li_underline mont">DEPARTMENTS</li>
                                                <li class="m-t-5">
                                                    <a href="#">
                                                        <i class="fa fa-angle-double-right text-primary"></i>
                                                        Dept. of Dentology
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        <i class="fa fa-angle-double-right text-primary"></i>
                                                        Dept. of Cardiology
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        <i class="fa fa-angle-double-right text-primary"></i>
                                                        Dept. of Gastroenterology
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        <i class="fa fa-angle-double-right text-primary"></i>
                                                        Dept. of Gynaecology
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        <i class="fa fa-angle-double-right text-primary"></i>
                                                        Dept. of Laboratory
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        <i class="fa fa-angle-double-right text-primary"></i>
                                                        Area of Departments
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        <i class="fa fa-angle-double-right text-primary"></i>
                                                        Departments Single Post
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        <i class="fa fa-angle-double-right text-primary"></i>
                                                        Departments Timetable
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        <i class="fa fa-angle-double-right text-primary"></i>
                                                        About SIRIUS CARE
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        <i class="fa fa-angle-double-right text-primary"></i>
                                                        Our History
                                                    </a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="col-sm-3">
                                            <ul>
                                                <li class="dropdown-header header_li_underline mont">CORE PAGES</li>
                                                <li class="m-t-5">
                                                    <a href="#">
                                                        <i class="fa fa-angle-double-right text-primary"></i>
                                                        Our Doctors
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        <i class="fa fa-angle-double-right text-primary"></i>
                                                        Career Opportunities
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        <i class="fa fa-angle-double-right text-primary"></i>
                                                        Career Details
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        <i class="fa fa-angle-double-right text-primary"></i>
                                                        Donate Blood
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        <i class="fa fa-angle-double-right text-primary"></i>
                                                        Our Gallery
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        <i class="fa fa-angle-double-right text-primary"></i>
                                                        Our Facilities
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        <i class="fa fa-angle-double-right text-primary"></i>
                                                        Events Single Post
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        <i class="fa fa-angle-double-right text-primary"></i>
                                                        Health Checkup
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        <i class="fa fa-angle-double-right text-primary"></i>
                                                        Health Events
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        <i class="fa fa-angle-double-right text-primary"></i>
                                                        Success Stories
                                                    </a>
                                                </li>
                                            </ul>
                                        </li>

                                        <li class="col-sm-3">
                                            <ul>
                                                <li class="dropdown-header header_li_underline mont">GET IN TOUCH</li>
                                               <li class="m-t-5">
                                                    <a href="#">
                                                        <i class="fa fa-angle-double-right text-primary"></i>
                                                        404 Page
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        <i class="fa fa-angle-double-right text-primary"></i>
                                                        Styles Page
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        <i class="fa fa-angle-double-right text-primary"></i>
                                                        FAQ
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        <i class="fa fa-angle-double-right text-primary"></i>
                                                        Interview Tips
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        <i class="fa fa-angle-double-right text-primary"></i>
                                                        Terms &amp; Conditions
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        <i class="fa fa-angle-double-right text-primary"></i>
                                                        Our Branches
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        <i class="fa fa-angle-double-right text-primary"></i>
                                                        Appointment
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        <i class="fa fa-angle-double-right text-primary"></i>
                                                        Contact Us
                                                    </a>
                                                </li>
                                             <li>
                                                    <a href="#">
                                                        <i class="fa fa-angle-double-right text-primary"></i>
                                                        Contact Us 2
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        <i class="fa fa-angle-double-right text-primary"></i>
                                                        Contact Us 3
                                                    </a>
                                                </li>
                                            </ul>
                                        </li>

                                        <li class="col-sm-3">
                                            <ul>
                                                <li>
                                                    <img src="img/header1.png" class="img-responsive" alt="Image">
                                                </li>
                                            </ul>
                                        </li>
                                    </ul>
                                </li>-->

                                <li class="list2 mont"><a href="#departments">DEPARTMENTS</a></li>
								<!--<li class="list2 mont"><a href="#">GALLERY</a></li>-->
								  <li class="list2 mont"><a href="#contact">CONTACT US</a></li>
                               <!-- <li class="dropdown blog_dropdown"><a href="#" class="dropdown-toggle menu_hover" data-toggle="dropdown">
                                    <span class="mont">GALLERY</span><span class="CAREt"></span></a>
                                    <ul class="sub_menu dropdown-menu blog_menu">
                                        <li>
                                            <a href="#">Two Columns</a>
                                        </li>
                                        <li>
                                            <a href="#">Three Columns</a>
                                        </li>
                                        <li>
                                            <a href="#">Four Columns</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="dropdown blog_dropdown"><a href="#" class="dropdown-toggle menu_hover" data-toggle="dropdown">
                                    <span class="mont">BLOG</span><span class="CAREt"></span></a>
                                    <ul class="sub_menu dropdown-menu blog_menu">
                                        <li>
                                            <a href="#">Blog</a>
                                        </li>
                                        <li>
                                            <a href="#">Blog Single</a>
                                        </li>
                                        <li>
                                            <a href="#">Blog Left Sidebar</a>
                                        </li>
                                        <li>
                                            <a href="#">Blog No Sidebar</a>
                                        </li>
                                    </ul>
                                </li>-->
                                <!--<li class="list2 mont"><a href="#">CONTACT</a></li>
                                <li class="dropdown blog_dropdown"><a href="#" class="dropdown-toggle menu_hover" data-toggle="dropdown">
                                    <span class="mont">CONTACT</span><span class="CAREt"></span></a>
                                    <ul class="sub_menu dropdown-menu blog_menu contact_menu">
                                        <li>
                                            <a href="#">Contact Us-1</a>
                                        </li>
                                        <li>
                                            <a href="#">Contact Us-2</a>
                                        </li>
                                        <li>
                                            <a href="#">Contact Us-3</a>
                                        </li>
                                    </ul>
                                </li>-->
                                <li class="list2 search_icon"><a href="#"><i class="fa fa-search"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </nav>
    <div id="search_large">
        <div class="search_wrap">
            <form class="search p-l-10">
                    <span class="text">
                        <input name="search" id="search_input" class="input-search" placeholder="Search..." type="text">
                    </span>
                <button type="submit" class="search-submit font20" id="submit_search">
                    <i class="fa fa-search text-white"></i>
                </button>
            </form>
            <button id="search-close">
                <i class="fa fa-times font20 text-white" aria-hidden="true"></i>
            </button>
        </div>
    </div>
</header>
<!------------------------End of Header----------------------->


<!-------------------------Start of Body Section------------------>
<section class="home2_revolution_slider">
    <div class="tp-banner-container rev_slider_wrapper fullwidthbanner-container" data-alias="news-hero72">
        <div class="tp-banner-slider">
            <ul>
                <li data-index="rs-82" data-transition="random"
                    data-slotamount="7" data-masterspeed="500"
                    data-saveperformance="on">
                    <img src="img/index2_rev_bg3.jpg" alt="image-missing" data-bgfit="cover">
                    <div class="tp-caption medium_bg_darkblue skewfromleft customout"
                         data-x="30"
                         data-y="100"
                         data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                         data-speed="1000"
                         data-start="1000"
                         data-easing="Power4.easeOut"
                         data-endspeed="300"
                         data-endeasing="Power1.easeIn"
                         data-captionhidden="on"
                         style="z-index: 6;font-size: 30px;font-family: 'Noto Sans';">It's not just
                    </div>

                    <div class="tp-caption medium_bg_darkblue skewfromleft customout"
                         data-x="30"
                         data-y="150"
                         data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                         data-speed="1000"
                         data-start="1000"
                         data-easing="Power4.easeOut"
                         data-endspeed="300"
                         data-endeasing="Power1.easeIn"
                         data-captionhidden="on"
                         style="z-index: 6;font-size: 30px;font-family: 'Noto Sans';">a heart transplant
                    </div>
                    <div class="tp-caption medium_bg_darkblue skewfromleft customout"
                         data-x="30"
                         data-y="200"
                         data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                         data-speed="1000"
                         data-start="1000"
                         data-easing="Power4.easeOut"
                         data-endspeed="300"
                         data-endeasing="Power1.easeIn"
                         data-captionhidden="on"
                         style="z-index: 6;font-size: 30px;font-family: 'Noto Sans';">but its a renaissance of life.
                    </div>

                    <div class="tp-caption sfl hidden-xs"
                         data-x="30"
                         data-y="250"
                         data-speed="1000"
                         data-start="1000"
                         data-easing="Back.easeInOut"
                         data-endspeed="300"
                         style="z-index:7; color: #111; font-size: 20px; letter-spacing: 1px; ">"If you have a heartbeat
                    </div>
                    <div class="tp-caption sfl hidden-xs"
                         data-x="30"
                         data-y="290"
                         data-speed="1000"
                         data-start="1000"
                         data-easing="Back.easeInOut"
                         data-endspeed="300"
                         style="z-index:7; color: #111; font-size: 20px; letter-spacing: 1px; ">there's still time for your dreams"
                    </div>
                    <div class="tp-caption sfl tp-resizeme rs-parallaxlevel-0 hidden-xs" data-x="30"
                         data-y="345" data-speed="2500" data-start="1000" data-easing="Power3.easeInOut"
                         data-splitout="none" data-elementdelay="0.1" data-endelementdelay="0.1"
                         style="z-index: 5;color: #fff">

                        <a href="index3.html#" class="btn btn-primary btn-md text-center rev_button_hover" data-toggle="modal" data-target="#appointment"
                           style="z-index: 5;color: #111;border-color: #111;">Book an Appointment</a>
                    </div>
                </li>
                <li data-transition="parallaxtoright" data-slotamount="7" data-masterspeed="600" >
                    <!-- MAIN IMAGE -->
                    <img src="img/index2_rev_bg1.jpg"  alt="kenburns5"  data-bgposition="right bottom" data-kenburns="on" data-duration="11000" data-ease="Linear.easeNone" data-bgfit="100" data-bgfitend="130" data-bgpositionend="left top">
                    <!-- LAYERS -->

                    <!-- LAYER NR. 1 -->
                    <div class="tp-caption customin customout tp-resizeme"
                         data-x="right" data-hoffset="-60"
                         data-y="center" data-voffset="0"
                         data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:5;scaleY:5;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                         data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                         data-speed="600"
                         data-start="500"
                         data-easing="Power3.easeInOut"
                         data-endspeed="300"
                         style="z-index: 2">
                    </div>
                    <div class="tp-caption customin customout tp-resizeme hidden-xs"
                         data-x="center"
                         data-y="50"
                         data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:5;scaleY:5;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                         data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                         data-speed="900"
                         data-fontsize="['50', '50', '50', '50']"
                         data-visibility="['on', 'on', 'on', 'on']"
                         data-start="1000"
                         data-easing="Back.easeOut"
                         data-endspeed="300"
                         style="z-index: 3;color: #fff;letter-spacing: 3px;">SIRIUS CARE Hospital
                    </div>
                    <div class="tp-caption customin customout tp-resizeme"
                         data-x="45"
                         data-y="80"
                         data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                         data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                         data-speed="800"
                         data-start="1500"
                         data-easing="Power3.easeInOut"
                         data-endspeed="300"
                         style="z-index: 4;color: #fff;font-size: 44px;font-weight: 700;font-family: Montserrat;letter-spacing: 3px;">EVERY LIFE MENTORED BY PROFESSIONAL
                    </div>
                    <div class="tp-caption customin customout tp-resizeme"
                         data-x="370" data-hoffset="-60"
                         data-y="140" data-voffset="0"
                         data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                         data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                         data-speed="800"
                         data-start="1500"
                         data-easing="Power3.easeInOut"
                         data-endspeed="300"
                         style="z-index: 4;color: #fff;font-size: 44px;font-weight: 700;font-family: Montserrat;letter-spacing: 3px;">PHYSICIANS
                    </div>
                    <div class="tp-caption sfb hidden-xs"
                         data-x="210"
                         data-y="240"
                         data-speed="800"
                         data-start="3300"
                         data-easing="Power4.easeOut"
                         data-endspeed="300"
                         data-endeasing="Power1.easeIn"
                         data-captionhidden="off"
                         style="z-index: 6;color: #fff;font-size: 24px;letter-spacing: 3px;">"A life lived in the service to others is worth living."
                    </div>

                    <div class="tp-caption grey_heavy_72 sfl tp-resizeme rs-parallaxlevel-0 hidden-xs" data-x="470"
                         data-y="320" data-speed="2500" data-start="4000" data-easing="Power3.easeInOut"
                         data-splitout="none" data-elementdelay="0.1" data-endelementdelay="0.1"
                         style="z-index: 5;color: #fff">

                       <a href="index3.html#" class="btn btn-primary btn-md text-center rev_button_hover" data-toggle="modal" data-target="#appointment"
                           style="z-index: 5;color: #111;border-color: #111;">Book an Appointment</a>
                    </div>
                </li>

                <li data-index="rs-81" data-transition="random"
                    data-slotamount="7" data-masterspeed="500"
                    data-saveperformance="on">
                    <img src="img/index2_rev_bg2.jpg" alt="image-missing" data-bgfit="cover">
                    <div class="tp-caption sfl hidden-xs"
                         data-x="30"
                         data-y="70"
                         data-speed="1000"
                         data-start="1000"
                         data-easing="Back.easeInOut"
                         data-endspeed="300"
                         style="z-index:7; color: #6f6f6f; font-size: 26px; letter-spacing: 1px; ">Best Care In The City
                    </div>
                    <div class="tp-caption tp-resizeme sfl"
                         data-x="30"
                         data-y="100"
                         data-speed="800"
                         data-start="1000"
                         data-easing="Back.easeInOut"
                         data-endspeed="300"
                         data-captionhidden="off"
                         style="z-index: 6; color: #6f6f6f; font-size: 44px; font-family: Montserrat;letter-spacing: 3px;font-weight: 700;">
                        We are
                    </div>
                    <div class="tp-caption tp-resizeme sfl"
                         data-x="30"
                         data-y="150"
                         data-speed="800"
                         data-start="1000"
                         data-easing="Back.easeInOut"
                         data-endspeed="300"
                         data-captionhidden="off"
                         style="z-index: 6; color: #6f6f6f; font-size: 44px; font-family: Montserrat;letter-spacing: 3px;font-weight: 700;">
                        Special
                    </div>

                    <div class="tp-caption sfl hidden-xs"
                         data-x="30"
                         data-y="210"
                         data-speed="1000"
                         data-start="1000"
                         data-easing="Back.easeInOut"
                         data-endspeed="300"
                         style="z-index:7; color: #6f6f6f; font-size: 26px; letter-spacing: 1px; ">"SIRIUS CARE provides a full spectrum of specialties 
                    </div>
                    <div class="tp-caption sfl hidden-xs"
                         data-x="35"
                         data-y="250"
                         data-speed="1000"
                         data-start="1000"
                         data-easing="Back.easeInOut"
                         data-endspeed="300"
                         style="z-index:7; color: #6f6f6f; font-size: 26px; letter-spacing: 1px; ">and programs to meet your all medical needs"
                    </div>
                    <div class="tp-caption sfl hidden-xs"
                         data-x="230"
                         data-y="280"
                         data-speed="1000"
                         data-start="1000"
                         data-easing="Back.easeInOut"
                         data-endspeed="300"
                         style="z-index:7; color: #6f6f6f; font-size: 20px; letter-spacing: 1px;font-style: italic; ">- Dr. Ruma Alam
                    </div>
                    <div class="tp-caption sfl tp-resizeme rs-parallaxlevel-0 hidden-xs" data-x="30"
                         data-y="330" data-speed="2500" data-start="1000" data-easing="Power3.easeInOut"
                         data-splitout="none" data-elementdelay="0.1" data-endelementdelay="0.1"
                         style="z-index: 5;color: #fff">

                        <a href="index3.html#" class="btn btn-primary btn-md text-center rev_button_hover" data-toggle="modal" data-target="#appointment"
                           style="z-index: 5;color: #111;border-color: #111;">Book an Appointment</a>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</section>

<section id="about">
    <div class="container">
        <div class="row m-t-60">
            <div class="col-md-5 col-md-push-7 m-t-40">
                <img src="img/index_female.png" class="img-responsive" alt="Image missing">
            </div>
            <div class="col-md-7 col-md-pull-5 m-t-10">
                <h3 class="head_left_underline">Welcome To SIRIUS CARE Hospital</h3>
                <div class="m-t-20">
                    <p>
                        When it comes to choosing a care, one thing is clear: quality counts. With more than two decades of tradition,
                        excellence and quality in providing medical care in a comfortable and convenient environment,
                        we bring peace of mind for the thousands of patients we care about every day with the help of our doctors, specialists and nurses.
                    </p>
                    <p>
                        We focus on offering a wide range of consistent high quality and up-to-date medical services
                        that allow us to improve the health of the communities we serve with compassion and sensitivity
                        to the individual needs of our patients and their families.
                    </p>
                    <p>
                        As a healthcare organization, we aspire to be the care of choice in Bangladesh and be recognized
                        for having the most satisfied patients, the best possible clinical outcomes, and the best physicians, specialists and employees.
                    </p>
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-6">
                        <div class="right_align">
                            <div class="index4_test_img">
                                <img src="img/index1_doctor1.png" class="img-responsive img-circle image_width" alt="Image missing">
                            </div>
                        </div>
                        <div class="m-l-100">
                            <h5>Tahmina</h5>
                            <span>Specialist</span>
                        </div>
                    </div>
                    <div class="col-md-6 text-right">
                        <img src="img/index5_sign.png" class="img-responsive index5_sign left_align" alt="Image missing">
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<section class="bg-primary m-t-100">
    <div class="container">
        <div class="row m-t-20 m-b-55">
            <div class="col-md-4 col-xs-12 m-t-40">
                <div class="index1_section1 text-white">
                    <div class="left_content right_align">
                        <img src="img/heart-checkup.png" alt="Image missing">
                    </div>
                    <div class="right_content m-l-100">
                        <h5 class="text-white">Regular Checkup</h5>
                        <p>
                            Our Whole Body Check-up includes a comprehensive screening of the entire body to identify
                            any diseases in the early stages.
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-xs-12 m-t-40">
                <div class="index1_section1 text-white">
                    <div class="left_content right_align">
                        <img src="img/medical-doctor.png" alt="Image missing">
                    </div>
                    <div class="right_content m-l-100">
                        <h5 class="text-white">Professional Doctors</h5>
                        <p>
                            The doctors at SIRIUS CARE is made up of highly qualified and skilled medical professionals
                            who walk with you throughout your treatment & beyond.
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-xs-12 m-t-40">
                <div class="index1_section1 text-white">
                    <div class="left_content right_align">
                        <img src="img/24-hours-service.png" alt="Image missing">
                    </div>
                    <div class="right_content m-l-100">
                        <h5 class="text-white">24 / 7 Monitor</h5>
                        <p>
                            Enjoy peace of mind knowing that we provide service 24 hours a day and 7 days a week
                            with a primary focus that ‘patients come first’.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<section>
    <div class="container">
        <div class="row m-t-30">
            <div class="col-xs-12 m-t-40">
                <h3 class="text-center head_center">Our Health Services</h3>
                <div class="health_services">
                    <div class="row">
                        <div class="col-md-4 col-sm-6 m-t-40">
                            <div class="interview_process_section2 index_box_shadow index1_health_service">
                                <div class="right_align m-t-10 p-d-20">
                                    <i class="fa fa-user-md font40 text-primary"></i>
                                </div>
                                <div class="m-l-100">
                                    <h6>Qualified Doctors</h6>
                                    <p>
                                        We have qualified doctors whom you can trust completely. Submit an appoinment
                                        and we will do the rest.
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-6 m-t-40">
                            <div class="interview_process_section2 index_box_shadow index1_health_service">
                                <div class="right_align m-t-10 p-d-20">
                                    <i class="fa fa-desktop font40 text-primary"></i>
                                </div>
                                <div class="m-l-100">
                                    <h6>Advanced Technology</h6>
                                    <p>
                                        Advanced technologies are here with us. You are welcome to give those to
                                        be at your service.
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-6 m-t-40">
                            <div class="interview_process_section2 index_box_shadow index1_health_service">
                                <div class="right_align m-t-10 p-d-20">
                                    <i class="fa fa-plus-circle font40 text-primary"></i>
                                </div>
                                <div class="m-l-100">
                                    <h6>Medical Treatment</h6>
                                    <p>
                                        Our CARE have comprehensive, evidence based guidelines for the treatment of injuries
                                        and illnesses.
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-6 m-t-40">
                            <div class="interview_process_section2 index_box_shadow index1_health_service">
                                <div class="right_align m-t-10 p-d-20">
                                    <i class="fa fa-medkit font40 text-primary"></i>
                                </div>
                                <div class="m-l-100">
                                    <h6>Emergency Help</h6>
                                    <p>
                                        We provide all sorts of Emergency help. Just Dial our Hotline at any time.
                                        We will be at your service.
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-6 m-t-40">
                            <div class="interview_process_section2 index_box_shadow index1_health_service">
                                <div class="right_align m-t-10 p-d-20">
                                    <i class="fa fa-heartbeat font40 text-primary"></i>
                                </div>
                                <div class="m-l-100">
                                    <h6>Cardio Monitoring</h6>
                                    <p>
                                        We provide the best treatment for all types of Cardiovascular diseases and disorders.
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-6 m-t-40">
                            <div class="interview_process_section2 index_box_shadow index1_health_service">
                                <div class="right_align m-t-10 p-d-20">
                                    <i class="fa fa-stethoscope font40 text-primary"></i>
                                </div>
                                <div class="m-l-100">
                                    <h6>Great Care</h6>
                                    <p>
                                        Care for the patient is our prime concern. Let our patient speak for us. We care the most.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<section class="index_bg_image m-t-100">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-sm-8 text-white m-t-40 m-b-70">
                <h2 class="head_white text-white">We have world class facilities to care you</h2>
                <div class="m-t-30">
                    <p>
                        <span><i class="fa fa-check font20"></i></span>
                        <span class="p-l-10">
                            Trained laboratory staff are providing best services which includes painless blood withdrawal.
                        </span>
                    </p>
                    <p>
                        <span><i class="fa fa-check font20"></i></span>
                        <span class="p-l-10">
                            24hours ECG services including machine report, done by trained staff.
                        </span>
                    </p>
                    <p>
                        <span><i class="fa fa-check font20"></i></span>
                        <span class="p-l-10">
                            24 hours patient transport vehicle available.
                        </span>
                    </p>
                    <p>
                        <span><i class="fa fa-check font20"></i></span>
                        <span class="p-l-10">
                            Free reliable quality medicines are available to beneficiaries on doctor prescription during OPD hours.
                        </span>
                    </p>
                    <a href="#" class="btn btn-default m-t-30">
                        Read More
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>


<section id="departments">
    <div class="container">
        <div class="row m-t-30">
            <div class="col-md-12 m-t-40">
                <h3 class="text-center head_center">Our Departments</h3>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-3 m-t-25">
                <a href="#heart" data-toggle="tab">
                    <div class="dept_heart radius20 text-center hr_primary_color p-t-b-40 hidden-sm">
                        <img src="img/heart.png" alt="heart">
                        <br>
                        <h5 class="text-primary">Heart Disease</h5>
                    </div>
                    <div class="dept_heart radius20 text-center hr_primary_color p-t-b-30 hidden-lg hidden-md hidden-xs">
                        <img src="img/heart.png" alt="heart">
                        <br>
                        <h5 class="text-primary">Heart Disease</h5>
                    </div>
                </a>
            </div>
            <div class="col-sm-3 m-t-25">
                <a href="#liver" data-toggle="tab">
                    <div class="dept_heart radius20 text-center hr_primary_color p-t-b-40 hidden-sm">
                        <img src="img/liver.png" alt="heart">
                        <br>
                        <h5 class="text-primary">Hepatology</h5>
                    </div>
                    <div class="dept_heart radius20 text-center hr_primary_color p-t-b-30 hidden-lg hidden-md hidden-xs">
                        <img src="img/liver.png" alt="heart">
                        <br>
                        <h5 class="text-primary">Hepatology</h5>
                    </div>
                </a>
            </div>
            <div class="col-sm-3 m-t-25">
                <a href="#brain" data-toggle="tab">
                    <div class="dept_heart radius20 text-center hr_primary_color p-t-b-40 hidden-sm">
                        <img src="img/brain.png" alt="heart">
                        <br>
                        <h5 class="text-primary">Neurology</h5>
                    </div>
                    <div class="dept_heart radius20 text-center hr_primary_color p-t-b-30 hidden-lg hidden-md hidden-xs">
                        <img src="img/brain.png" alt="heart">
                        <br>
                        <h5 class="text-primary">Neurology</h5>
                    </div>
                </a>
            </div>
            <div class="col-sm-3 m-t-25">
                <a href="#molar" data-toggle="tab">
                    <div class="dept_heart radius20 text-center hr_primary_color p-t-b-40 hidden-sm">
                        <img src="img/tooth1.png" alt="heart">
                        <br>
                        <h5 class="text-primary">Dental</h5>
                    </div>
                    <div class="dept_heart radius20 text-center hr_primary_color p-t-b-30 hidden-lg hidden-md hidden-xs">
                        <img src="img/tooth1.png" alt="heart">
                        <br>
                        <h5 class="text-primary">Dental</h5>
                    </div>
                </a>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-3 m-t-25">
                <a href="#medical" data-toggle="tab">
                    <div class="dept_heart radius20 text-center hr_primary_color p-t-b-40 hidden-sm">
                        <img src="img/glasses.png" alt="heart">
                        <br>
                        <h5 class="text-primary">Ophthalmology</h5>
                    </div>
                    <div class="dept_heart radius20 text-center hr_primary_color p-t-b-30 hidden-lg hidden-md hidden-xs">
                        <img src="img/glasses.png" alt="heart">
                        <br>
                        <h5 class="text-primary">Ophthalmology</h5>
                    </div>
                </a>
            </div>
            <div class="col-sm-3 m-t-25">
                <a href="#ear" data-toggle="tab">
                    <div class="dept_heart radius20 text-center hr_primary_color p-t-b-40 hidden-sm">
                        <img src="img/human-ear.png" alt="heart">
                        <br>
                        <h5 class="text-primary">ENT</h5>
                    </div>
                    <div class="dept_heart radius20 text-center hr_primary_color p-t-b-30 hidden-lg hidden-md hidden-xs">
                        <img src="img/human-ear.png" alt="heart">
                        <br>
                        <h5 class="text-primary">ENT</h5>
                    </div>
                </a>
            </div>
            <div class="col-sm-3 m-t-25">
                <a href="#heart1" data-toggle="tab">
                    <div class="dept_heart radius20 text-center hr_primary_color p-t-b-40 hidden-sm">
                        <img src="img/bone.png" alt="heart">
                        <br>
                        <h5 class="text-primary">Orthopedics</h5>
                    </div>
                    <div class="dept_heart radius20 text-center hr_primary_color p-t-b-30 hidden-lg hidden-md hidden-xs">
                        <img src="img/bone.png" alt="heart">
                        <br>
                        <h5 class="text-primary">Orthopedics</h5>
                    </div>
                </a>
            </div>
            <div class="col-sm-3 m-t-25">
                <a href="#liver1" data-toggle="tab">
                    <div class="dept_heart radius20 text-center hr_primary_color p-t-b-40 hidden-sm">
                        <img src="img/laboratory.png" alt="heart">
                        <br>
                        <h5 class="text-primary">Laboratory Analysis</h5>
                    </div>
                    <div class="dept_heart radius20 text-center hr_primary_color p-t-b-30 hidden-lg hidden-md hidden-xs">
                        <img src="img/laboratory.png" alt="heart">
                        <br>
                        <h5 class="text-primary">Laboratory Analysis</h5>
                    </div>
                </a>
            </div>
        </div>

        <div class="tab-content">
            <div class="tab-pane active" id="heart">
                <div class="row">
                    <div class="col-sm-6 m-t-50 text-center">
                        <img src="img/blog_img3.jpg" alt="heart" class="img-responsive">
                    </div>
                    <div class="col-sm-6 m-t-25">
                        <h3 class="head_left_underline">Heart Disease</h3>
                        <p class="m-t-20">
                            The Division of Cardiology at SIRIUS CARE is determined to provide compassionate, state of the art,
                            proficient care to heart patients at an affordable cost. Our highly qualified team of cardiologists
                            along with the latest technology ensures best possible care for a wide spectrum of heart diseases.<br><br>
                            Our Interventional team is available round the clock to help patients with cardiac emergencies with
                            primary and complex coronary angioplasties and Stinting. Our team of doctors, nurses, technicians
                            and other heart experts are there to ensure unparalleled excellence in patient care, education and research.
                        </p>
                        <div class="m-t-30">
                            <a href="#" class="btn btn-primary" data-toggle="modal" data-target="#appointment">
                                Make an Appointment
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane" id="liver">
                <div class="row">
                    <div class="col-sm-6 m-t-50 text-center">
                        <img src="img/blog_img4.jpg" alt="heart" class="img-responsive">
                    </div>
                    <div class="col-sm-6 m-t-25">
                        <h3 class="head_left_underline">Hepatology</h3>
                        <p class="m-t-20">
                            Welcome to the Division of Gastroenterology & Hepatology, where we strive to provide outstanding
                            and compassionate patient care.
                            We offer first-rate clinical services through the availability of a wide range of gastroenterology
                            and hepatology subspecialty faculty and programs, including hepatology, inflammatory bowel disease (IBD),
                            endoscopic ultrasound and ERCP, motility, and gastrointestinal cancer prevention and treatment.<br><br>
                            The division has more than doubled in size over the past five years resulting in a marked increase
                            in clinical volume and expansion in many areas of expertise.
                        </p>
                        <div class="m-t-30">
                            <a href="#" class="btn btn-primary" data-toggle="modal" data-target="#appointment">
                                Make an Appointment
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane" id="brain">
                <div class="row">
                    <div class="col-sm-6 m-t-50 text-center">
                        <img src="img/index1_brain.jpg" alt="heart" class="img-responsive">
                    </div>
                    <div class="col-sm-6 m-t-25">
                        <h3 class="head_left_underline">Neurology</h3>
                        <p class="m-t-20">
                            The Neurology Department provides a clinical and diagnostic service for assessment and management
                            of patients with disorders of brain, spinal cord, nerve and muscle. Our inpatient service includes
                            a dedicated four bed acute stroke unit and multidisciplinary team of nurses and allied
                            health professionals.<br><br>
                            Our general neurologists are committed to provide specialized care with a
                            generalist perspective, ensuring an innovative, effective approach, and diagnosis and treatment of a
                            wide variety of neurological conditions.
                        </p>
                        <div class="m-t-30">
                            <a href="#" class="btn btn-primary" data-toggle="modal" data-target="#appointment">
                                Make an Appointment
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane" id="molar">
                <div class="row">
                    <div class="col-sm-6 m-t-50 text-center">
                        <img src="img/index1_dental.jpg" alt="heart" class="img-responsive">
                    </div>
                    <div class="col-sm-6 m-t-25">
                        <h3 class="head_left_underline">Dental</h3>
                        <p class="m-t-20">
                            We provide complete range of dental treatments with highly experienced team of doctors.
                            We aim in providing multidisciplinary services in dental treatments at affordable cost.<br><br>
                            Services provided at Dental department of SIRIUS CARE are- Simple to complicated Root Canals,
                            Repeat Root Canal treatment, Root Canal Surgeries, Prosthetic Dentistry, Orthodontic treatment,
                            Periodaontal Surgery, Dental implants, dental braces or clips, lasers, teeth whitening,
                            tooth bleach, ceramic crowns, zirconium caps, Paediatric Dentistry.
                        </p>
                        <div class="m-t-30">
                            <a href="#" class="btn btn-primary" data-toggle="modal" data-target="#appointment">
                                Make an Appointment
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane" id="medical">
                <div class="row">
                    <div class="col-sm-6 m-t-50 text-center">
                        <img src="img/optha.jpg" alt="heart" class="img-responsive">
                    </div>
                    <div class="col-sm-6 m-t-25">
                        <h3 class="head_left_underline">Ophthalmology</h3>
                        <p class="m-t-20">
                            The Ophthalmology or eye department provides a comprehensive range of secondary and tertiary
                            eye (ophthalmic) services for adults and children. The department has a large multi professional
                            eye care team, and the team are able to diagnose, investigate, treat
                            and operate (where necessary) on a range of eye conditions, including complex eye conditions.<br>
                            There is a specialist eye casualty. We have a strong research background and are a major
                            teaching and training unit for ophthalmologists.
                        </p>
                        <div class="m-t-30">
                            <a href="#" class="btn btn-primary" data-toggle="modal" data-target="#appointment">
                                Make an Appointment
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane" id="ear">
                <div class="row">
                    <div class="col-sm-6 m-t-50 text-center">
                        <img src="img/dept_img.jpg" alt="heart" class="img-responsive">
                    </div>
                    <div class="col-sm-6 m-t-25">
                        <h3 class="head_left_underline">ENT</h3>
                        <p class="m-t-20">
                            The Department of ENT at People’s Hospital is well-equipped to handle any kind of ENT emergencies
                            and has specialized staff to handle difficult surgical situations. We track the latest developments
                            and imbibe innovative methods for better patient care. We have a team of doctors whose credentials
                            go beyond the ordinary.<br><br>
                            They are expertise in handling any kind of trauma / emergency and also
                            undertake complex surgeries on a regular basis. We treat many kinds of disorders like Deafness,
                            Discharge in ears, Sinus infection, Tonsil infections, Patients with difficulty in swallowing,
                            breathing and headache etc.
                        </p>
                        <div class="m-t-30">
                            <a href="#" class="btn btn-primary" data-toggle="modal" data-target="#appointment">
                                Make an Appointment
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane" id="heart1">
                <div class="row">
                    <div class="col-sm-6 m-t-50 text-center">
                        <img src="img/index1_ortho.jpg" alt="heart" class="img-responsive">
                    </div>
                    <div class="col-sm-6 m-t-25">
                        <h3 class="head_left_underline">Orthopedics</h3>
                        <p class="m-t-20">
                            Department of Orthopedics at SIRIUS CARE provides end-to-end services and management of
                            all kinds of bone and joint problems. We offer comprehensive orthopedic care for patients making
                            the diagnosis, treatment and recovery phases less stressful and more efficient our patients.<br><br>
                            We have a dedicated team of doctors available round the clock to attend to all aspects of
                            trauma including polytrauma, compound fractures and complex injuries besides treating simple ones.
                            We provide comprehensive care for complex orthopedic problems.
                        </p>
                        <div class="m-t-30">
                            <a href="#" class="btn btn-primary" data-toggle="modal" data-target="#appointment">
                                Make an Appointment
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane" id="liver1">
                <div class="row">
                    <div class="col-sm-6 m-t-50 text-center">
                        <img src="img/index6_news_img1.jpg" alt="heart" class="img-responsive">
                    </div>
                    <div class="col-sm-6 m-t-25">
                        <h3 class="head_left_underline">Laboratory Analysis</h3>
                        <p class="m-t-20">
                            The SIRIUS CARE has a multi-disciplinary, well-equipped and comprehensive laboratory. The department
                            conducts and processes a wide range of tests and analyses, ensuring teams within the hospital have
                            access to all the relevant information required to make diagnoses and monitor patient progress while
                            patients undergo therapy.<br><br>
                            The department consists of a committed team of skilled pathologists and medical technologists/technicians,
                            with expertise in the use of sophisticated laboratory equipment.
                        </p>
                        <div class="m-t-30">
                            <a href="#" class="btn btn-primary" data-toggle="modal" data-target="#appointment">
                                Make an Appointment
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<section>
	<div class="container">
		<div class="container">
    <div class="row m-t-60">
        <div class="col-sm-6 col-sm-push-6 m-t-40">
            <img src="img/professional1.jpg" alt="loading" class="img-responsive">
        </div>
        <div class="col-sm-6 col-sm-pull-6 m-t-10">
            <h3>We Are Experts In All The Departments</h3>
            <p class="text-justify m-t-30">We have a team of highly qualified, well experienced senior consultants,
                surgeons who are supported by highly skilled, experienced and dedicated nursing staff and other para
                medical staff at the service of the patient round the clock, so that we can offer you a wide range of options to improve
                your quality of life. <br><br>
                Our ability to provide excellent care to our patients, often with complex illnesses, is possible only with the
                quality and commitment of our entire clinical team. Our physicians, psychologists and therapists, whose clinical
                expertise is enriched by their research and teaching, reach across medical disciplines to deliver comprehensive
                clinical care.
            </p>
        </div>
    </div>
    <div class="row panel_top m-t-100 services_list">
        <div class="col-sm-6 col-xs-12 accordian_alignment">
            <div class="panel-group" id="toggle" role="tablist" aria-multiselectable="true">
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="title-one">
                        <a class="collapsed accordion-section-title" data-toggle="collapse" data-parent="#accordion"
                           href="departments.html#panel-data-one" aria-expanded="false">
                            <div class="panel-title text-primary">Who We Are?
                                <i class="fa fa-plus pull-right"></i>
                            </div>
                        </a>
                    </div>
                    <div id="panel-data-one" class="panel-collapse collapse" role="tabpanel">
                        <div class="panel-body">
                            <div class="row">
                               <div class="col-md-12">
                                   SIRIUS CARE is a full service, acute care hospital committed to providing exceptional health care to the communities of                                      Chittagong. We employ 280 staff, 65 physicians and over 100 volunteers.
                               </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="title-two">
                        <a class="collapsed accordion-section-title" role="button" data-toggle="collapse"
                           data-parent="#accordion" href="departments.html#panel-data-two" aria-expanded="false">
                            <div class="panel-title text-primary">What We Do?
                                <i class="fa fa-plus pull-right"></i>
                            </div>
                        </a>
                    </div>
                    <div id="panel-data-two" class="panel-collapse collapse" role="tabpanel">
                        <div class="panel-body">
                            <div class="row">
                               <div class="col-md-12">
                                   We provide general hospital services to our local populations in and around Chittagong.
                                   Our specialised services include neurosciences, arterial vascular surgery, neonatal, paediatrics,
                                   cardiac, cancer, renal and infectious diseases.
                               </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-6 col-xs-12  accordian_alignment">
            <div class="panel-group" id="toggle1" role="tablist" aria-multiselectable="true">
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="title-three">
                        <a class="collapsed accordion-section-title" role="button" data-toggle="collapse"
                           data-parent="#accordion1" href="departments.html#panel-data-three" aria-expanded="false">
                            <div class="panel-title text-primary">Why Choose Us?
                                <i class="fa fa-plus pull-right"></i>
                            </div>
                        </a>
                    </div>
                    <div id="panel-data-three" class="panel-collapse collapse" role="tabpanel">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-12">
                                    * Our survival rates are higher than average.<br>
                                    * The vast majority of our patients rate us ‘good to excellent’ in survey after survey.<br>
                                    * We have a reputation for treating some of the rarest and most complex conditions.
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="title-four">
                        <a class="collapsed accordion-section-title" role="button" data-toggle="collapse"
                           data-parent="#accordion1" href="departments.html#panel-data-four" aria-expanded="false">
                            <div class="panel-title text-primary">How We CARE?
                                <i class="fa fa-plus pull-right"></i>
                            </div>
                        </a>
                    </div>
                    <div id="panel-data-four" class="panel-collapse collapse" role="tabpanel">
                        <div class="panel-body">
                            <div class="row">
                               <div class="col-md-12">
                                   We go the extra mile to provide services, to provide comfort, to provide answers, to provide assurance,
                                   and to provide support for our patients and their loved ones. SIRIUS CARE offers quality care close to home.
                               </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<section class="swiper_area">
    <div class="container-fluid m-t-45">
        <div class="row">
            <div class="col-md-12 m-t-25 text-right">
                <span class="swiper_prev cursor_pt"><img src="img/previous.png" alt="loading"></span>
                <span class="swiper_next cursor_pt p-l-10"><img src="img/next.png" alt="loading"></span>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 p-d-0">
            <div class="swiper-container swiper-container1">
                <div class="swiper-wrapper  department_swiper m-t-40 m-b-30">
                    <!-- Slides -->
                    <div class="swiper-slide swiper_block">
                        <div class="ih-item square effect6 from_top_and_bottom ihover_width_services">
                            <div class="img"><img src="img/blog_img3.jpg" alt="loading" class="img-responsive"></div>
                            <div class="info ihover_background_services">
                                <p>
                                    <a href="#" class="btn btn-default" data-toggle="modal" data-target="#appointment">Make An Appointment</a>
                                </p>
                            </div>
                        </div>
                        <div class="name_block shadow_services text-center"><h5 class="head_center">Cardiology</h5></div>
                    </div>
                    <div class="swiper-slide swiper_block">
                        <div class="ih-item square effect6 from_top_and_bottom ihover_width_services">
                            <div class="img"><img src="img/gallery_img6.jpg" alt="loading" class="img-responsive"></div>
                            <div class="info ihover_background_services">
                                <p>
                                    <a href="#" class="btn btn-default" data-toggle="modal" data-target="#appointment">Make An Appointment</a>
                                </p>
                            </div>
                        </div>
                        <div class="name_block shadow_services text-center"><h5 class="head_center">Pediatric Clinic</h5></div>
                    </div>
                    <div class="swiper-slide swiper_block">
                        <div class="ih-item square effect6 from_top_and_bottom ihover_width_services">
                            <div class="img"><img src="img/gallery_img5.jpg" alt="loading" class="img-responsive"></div>
                            <div class="info ihover_background_services">
                                <p>
                                    <a href="#" class="btn btn-default" data-toggle="modal" data-target="#appointment">Make An Appointment</a>
                                </p>
                            </div>
                        </div>
                        <div class="name_block shadow_services text-center"><h5 class="head_center">Radiology</h5></div>
                    </div>
                    <div class="swiper-slide swiper_block">
                        <div class="ih-item square effect6 from_top_and_bottom ihover_width_services">
                            <div class="img"><img src="img/gallery_img3.jpg" alt="loading" class="img-responsive"></div>
                            <div class="info ihover_background_services">
                                <p>
                                    <a href="#" class="btn btn-default" data-toggle="modal" data-target="#appointment">Make An Appointment</a>
                                </p>
                            </div>
                        </div>
                        <div class="name_block shadow_services text-center"><h5 class="head_center">Laryngological </h5></div>
                    </div>
                    <div class="swiper-slide swiper_block">
                        <div class="ih-item square effect6 from_top_and_bottom ihover_width_services">
                            <div class="img"><img src="img/dept_service_img2.jpg" alt="loading" class="img-responsive"></div>
                            <div class="info ihover_background_services">
                                <p>
                                    <a href="#" class="btn btn-default" data-toggle="modal" data-target="#appointment">Make An Appointment</a>
                                </p>
                            </div>
                        </div>
                        <div class="name_block shadow_services text-center"><h5 class="head_center">Outpatient</h5></div>
                    </div>
                    <div class="swiper-slide swiper_block">
                        <div class="ih-item square effect6 from_top_and_bottom ihover_width_services">
                            <div class="img"><img src="img/facility_img2.jpg" alt="loading" class="img-responsive"></div>
                            <div class="info ihover_background_services">
                                <p>
                                    <a href="#" class="btn btn-default" data-toggle="modal" data-target="#appointment">Make An Appointment</a>
                                </p>
                            </div>
                        </div>
                        <div class="name_block shadow_services text-center"><h5 class="head_center">Laboratory</h5></div>
                    </div>
                </div>
            </div>
            </div>
        </div>
    </div>
</section>

<!--<section>
    <div class="container">
        <div class="row m-t-40">
            <div class="col-md-12 text-center m-t-10">
                <h3 class="head_center">Find A Doctor</h3>
                <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered </p>
                <div class="m-t-40">
                <img src="img/dept_doctor.jpg" alt="loading" class="img-responsive img_margin m-t-30">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-3 col-xs-6 m-t-40">
                <div class="dept_img_effect dept_imgmod img_margin" data-dept="Cardiology Department" >
                    <div class="div_border " data-dept="Cardiology Department" data-toggle="modal" data-target="#heart">
                        <img src="img/heart1.png" alt="loading" class="img-responsive  img_dept_round">
                    </div>
                    <div>
                        <h5 class="text-center m-t-10 text_hover" data-toggle="modal" data-target=".heart">
                            Cardiology</h5>
                    </div>
                </div>
                <div class="modal fade heart" id="heart" role="dialog">
                    <div class="modal-dialog">
                        <form action="departments.html#" method="post">
                            <div class="modal-content dept_model">
                                <div class="modal-header ">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="modal-title text-white text-center">Cardiology Department</h4>
                                </div>
                                <div class="modal-body">
                                    <h5 class="m-t-30 head_left">Find A Doctor</h5>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group m-t-20">
                                                <label for="department">Center or Department</label>
                                                <select name="department" id="department" class="department_select select2 form-control"
                                                style="width: 100%;">
                                                    <option selected="" value="" disabled="">Choose</option>
                                                    <option>Departments</option>
                                                    <option>Center of Excellence</option>
                                                    <option>Center Of Research</option>
                                                    <option>Clinical Departments</option>
                                                </select>
                                            </div>
                                            <div class="form-group m-t-30">
                                                <label for="specality">Specialty or Program</label>
                                                <select name="department" id="specality" class="department_select select2 form-control"
                                                        style="width: 100%;">
                                                    <option selected="" value="" disabled="">Choose</option>
                                                    <option>Lorem ipsum 1</option>
                                                    <option>Lorem ipsum 2</option>
                                                </select>
                                            </div>

                                        </div>
                                        <div class="col-md-6 m-t-20">
                                            <div class="form-group">
                                                <label for="location">Locations</label>
                                                <select name="department" id="location" class="department_select select2 form-control"
                                                        style="width: 100%;">
                                                    <option selected="" value="" disabled="">Choose</option>
                                                    <option>Jacksonville</option>
                                                    <option>Oklahoma City</option>
                                                    <option>San Francisco</option>
                                                    <option>St. Petersburg</option>
                                                    <option>Virginia Beach</option>

                                                </select>
                                            </div>
                                            <div class="form-group m-t-30">
                                                <label for="languages">Language</label>
                                                <select name="department" id="languages" class="department_select select2 form-control"
                                                        style="width: 100%;">
                                                    <option selected="" value="" disabled="">Choose</option>
                                                    <option>Afrikaans</option>
                                                    <option>Arabic</option>
                                                    <option>Armenian</option>
                                                    <option>Azerbaijan</option>
                                                    <option>Azeri</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="form-group m-t-15">
                                                <label>Gender <br /><span class="p-r-20">
                                        <input type="radio" name="gender" class="gender blue_check" id="male"/>
                                        Male
                                    </span></label>
                                                <label> <span class="p-r-20">
                                        <input type="radio" name="gender" class="gender blue_check" id="female"/>
                                      Female
                                    </span></label>
                                                <label> <span>
                                        <input type="radio" name="gender" class="gender blue_check" id="no"/>
                                      No Preference
                                    </span></label>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <div class="modal-footer department_model_footer">
                                    <input type="submit" class="btn btn-primary" value="Submit">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-sm-3 col-xs-6 m-t-40">
                <div class="dept_img_effect dept_imgmod img_margin" data-dept="Cancer Department">
                    <div class="div_border " data-toggle="modal"
                         data-target="#heart">
                        <img src="img/cancer2.png" alt="loading" class="img-responsive  img_dept_round">
                    </div>
                    <h5 class="text-center m-t-10 text_hover" data-toggle="modal"
                        data-target="#heart">
                        Cancer
                    </h5>
                </div>
            </div>
            <div class="col-sm-3 col-xs-6 m-t-50 ">
                <div class="dept_imgmod dept_img_effect img_margin" data-dept="Neurology Department">
                    <div class="div_border "  data-toggle="modal" data-target="#heart">
                        <img src="img/brain1.png" alt="loading" class="img-responsive  img_dept_round">
                    </div>
                    <h5 class="text-center m-t-10 text_hover"  data-toggle="modal" data-target="#heart">
                        Neurology
                    </h5>
                </div>
            </div>
            <div class="col-sm-3 col-xs-6 m-t-50">
                <div class="dept_img_effect dept_imgmod img_margin" data-dept="Orthopedics Department">
                    <div class="div_border"  data-toggle="modal"
                         data-target="#heart"><img src="img/bone1.png" alt="loading" class="img-responsive  img_dept_round">
                    </div>
                    <h5 class="text-center m-t-10 text_hover" data-toggle="modal"
                        data-target="#heart">
                        Orthopedics
                    </h5>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-3 col-xs-6 m-t-40">
                <div class="dept_img_effect dept_imgmod img_margin" data-dept="Dentology Department" >
                    <div class="div_border "   data-dept="Dentology Department" data-toggle="modal"
                         data-target="#heart"><img src="img/tooth.png" alt="loading" class="img-responsive  img_dept_round"></div>
                    <h5 class="text-center m-t-10 text_hover"   data-toggle="modal"
                        data-target="#heart">Dentalogy</h5>
                </div>
            </div>
            <div class="col-sm-3 col-xs-6 m-t-40">
                <div class="dept_img_effect dept_imgmod img_margin" data-dept="Laboratory Analysis">
                    <div class="div_border " data-toggle="modal"
                         data-target="#heart"><img src="img/lab.png" alt="loading"
                                                   class="img-responsive  img_dept_round">

                    </div>
                    <h5 class="text-center m-t-10 text_hover"   data-toggle="modal"
                        data-target="#heart">Laboratory Analysis</h5>
                </div>
            </div>
            <div class="col-sm-3 col-xs-6 m-t-40"  data-dept="Critical CARE Department">
                <div class="dept_img_effect dept_imgmod img_margin" data-dept="Critical CARE Department">
                    <div class="div_border " data-toggle="modal" data-target="#heart">
                        <img src="img/cardiogram1.png" alt="loading" class="img-responsive  img_dept_round">

                    </div>
                    <h5 class="text-center m-t-10 text_hover"  data-toggle="modal"
                        data-target="#heart">
                        Critical CARE</h5>

                </div>
            </div>
            <div class="col-sm-3 col-xs-6 m-t-40">
                <div class="dept_imgmod dept_img_effect img_margin" data-dept="Gastro CARE Department">
                    <div class="div_border "  data-toggle="modal"
                         data-target="#heart"><img src="img/stomach1.png" alt="loading" class="img-responsive img_dept_round"></div>
                    <div class="my" id="my">
                        <h5 class="text-center m-t-10 text_hover"  data-toggle="modal" data-target="#heart">
                            Gastro CARE</h5>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>-->

<section class="index_testimonial m-t-100 swiper_area">
    <div class="container">
        <div class="row">
            <div class="col-md-12 m-t-45">
                <h3 class="head_center text-center">What People Say</h3>
                <div class="row">
                    <div class="col-md-offset-3 col-md-6 m-t-10">
                        <div class="text-center">
                            <p>
                                Recent surveys found family members of SIRIUS CARE patients were 100 percent
                                satisfied with their loved one’s long-term CARE, with 87 percent indicating they were extremely satisfied.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="swiper-container index1_swiper swiper-container-horizontal">
            <div class="swiper-wrapper m-b-100" style="transform: translate3d(-1560px, 0px, 0px); transition-duration: 0ms;"><div class="swiper-slide swiper-slide-duplicate" data-swiper-slide-index="0" style="width: 360px; margin-right: 30px;">
                    <div class="row">
                        <div class="col-md-12 m-t-25">
                            <div class="newsticker_border bg-white index_testimonial_padding index_box_shadow">
                                <div class="row">
                                    <div class="col-xs-4 right_align">
                                        <img src="img/index_team1.png" alt="Image missing" class="img-responsive img-circle swiper_block p-d-3">
                                    </div>
                                    <div class="col-xs-8">
                                        <h5 class="text-primary">Dr. Anup Kumar</h5>
                                        <p class="m-t-n5">Cardiologist</p>
                                    </div>
                                </div>
                                <hr class="hr_primary_color">
                                <div class="row">
                                    <div class="col-md-12 text-center">
                                        <p>“The people of this Hospital are fantastic and one of the most valuable assets of our medical arena.
                                            They are doing great job with the patients!"</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><div class="swiper-slide swiper-slide-duplicate" data-swiper-slide-index="1" style="width: 360px; margin-right: 30px;">
                    <div class="row">
                        <div class="col-md-12 m-t-25">
                            <div class="newsticker_border bg-white index_testimonial_padding index_box_shadow">
                                <div class="row">
                                    <div class="col-xs-4 right_align">
                                        <img src="img/index_team2.png" alt="Image missing" class="img-responsive img-circle swiper_block p-d-3">
                                    </div>
                                    <div class="col-xs-8">
                                        <h5 class="text-primary">Dr. Tania Hasin</h5>
                                        <p class="m-t-n5">Dentist</p>
                                    </div>
                                </div>
                                <hr class="hr_primary_color">
                                <div class="row">
                                    <div class="col-md-12 text-center">
                                        <p>“Efficient, clear, and the staff have all been very friendly and helpful. I wish all medical facilities
                                            could be like yours. Keep up the good work.”</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><div class="swiper-slide swiper-slide-duplicate" data-swiper-slide-index="2" style="width: 360px; margin-right: 30px;">
                    <div class="row">
                        <div class="col-md-12 m-t-25">
                            <div class="newsticker_border bg-white index_testimonial_padding index_box_shadow">
                                <div class="row">
                                    <div class="col-xs-4 right_align">
                                        <img src="img/index_team3.png" alt="Image missing" class="img-responsive img-circle swiper_block p-d-3">
                                    </div>
                                    <div class="col-xs-8">
                                        <h5 class="text-primary">Dr. Tarikul Islam</h5>
                                        <p class="m-t-n5">Physician</p>
                                    </div>
                                </div>
                                <hr class="hr_primary_color">
                                <div class="row">
                                    <div class="col-md-12 text-center">
                                        <p>“Physicians and staffs set the bar here.
                                            I feel there may be no better thorough medical CARE program in the country. Outstanding as always.”</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="swiper-slide swiper-slide-prev" data-swiper-slide-index="0" style="width: 360px; margin-right: 30px;">
                    <div class="row">
                        <div class="col-md-12 m-t-25">
                            <div class="newsticker_border bg-white index_testimonial_padding index_box_shadow">
                                <div class="row">
                                    <div class="col-xs-4 right_align">
                                        <img src="img/index_team1.png" alt="Image missing" class="img-responsive img-circle swiper_block p-d-3">
                                    </div>
                                    <div class="col-xs-8">
                                        <h5 class="text-primary">Dr. Anup Kumar</h5>
                                        <p class="m-t-n5">Cardiologist</p>
                                    </div>
                                </div>
                                <hr class="hr_primary_color">
                                <div class="row">
                                    <div class="col-md-12 text-center">
                                        <p>“The people of this Hospital are fantastic and one of the most valuable assets of our medical arena.
                                            They are doing great job with the patients!"</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="swiper-slide swiper-slide-active" data-swiper-slide-index="1" style="width: 360px; margin-right: 30px;">
                    <div class="row">
                        <div class="col-md-12 m-t-25">
                            <div class="newsticker_border bg-white index_testimonial_padding index_box_shadow">
                                <div class="row">
                                    <div class="col-xs-4 right_align">
                                        <img src="img/index_team2.png" alt="Image missing" class="img-responsive img-circle swiper_block p-d-3">
                                    </div>
                                    <div class="col-xs-8">
                                        <h5 class="text-primary">Dr. Tania Hasin</h5>
                                        <p class="m-t-n5">Dentist</p>
                                    </div>
                                </div>
                                <hr class="hr_primary_color">
                                <div class="row">
                                    <div class="col-md-12 text-center">
                                        <p>“Efficient, clear, and the staff have all been very friendly and helpful. I wish all medical facilities
                                            could be like yours. Keep up the good work.”</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="swiper-slide swiper-slide-next" data-swiper-slide-index="2" style="width: 360px; margin-right: 30px;">
                    <div class="row">
                        <div class="col-md-12 m-t-25">
                            <div class="newsticker_border bg-white index_testimonial_padding index_box_shadow">
                                <div class="row">
                                    <div class="col-xs-4 right_align">
                                        <img src="img/index_team3.png" alt="Image missing" class="img-responsive img-circle swiper_block p-d-3">
                                    </div>
                                    <div class="col-xs-8">
                                        <h5 class="text-primary">Dr. Tarikul Islam</h5>
                                        <p class="m-t-n5">Physician</p>
                                    </div>
                                </div>
                                <hr class="hr_primary_color">
                                <div class="row">
                                    <div class="col-md-12 text-center">
                                        <p>“Physicians and staffs set the bar here.
                                            I feel there may be no better thorough medical CARE program in the country. Outstanding as always.”</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <div class="swiper-slide swiper-slide-duplicate" data-swiper-slide-index="0" style="width: 360px; margin-right: 30px;">
                    <div class="row">
                        <div class="col-md-12 m-t-25">
                            <div class="newsticker_border bg-white index_testimonial_padding index_box_shadow">
                                <div class="row">
                                    <div class="col-xs-4 right_align">
                                        <img src="img/index_team1.png" alt="Image missing" class="img-responsive img-circle swiper_block p-d-3">
                                    </div>
                                    <div class="col-xs-8">
                                        <h5 class="text-primary">Dr. Anup Kumar</h5>
                                        <p class="m-t-n5">Cardiologist</p>
                                    </div>
                                </div>
                                <hr class="hr_primary_color">
                                <div class="row">
                                    <div class="col-md-12 text-center">
                                        <p>“The people of this Hospital are fantastic and one of the most valuable assets of our medical arena.
                                            They are doing great job with the patients!"</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><div class="swiper-slide swiper-slide-duplicate" data-swiper-slide-index="1" style="width: 360px; margin-right: 30px;">
                    <div class="row">
                        <div class="col-md-12 m-t-25">
                            <div class="newsticker_border bg-white index_testimonial_padding index_box_shadow">
                                <div class="row">
                                    <div class="col-xs-4 right_align">
                                        <img src="img/index_team2.png" alt="Image missing" class="img-responsive img-circle swiper_block p-d-3">
                                    </div>
                                    <div class="col-xs-8">
                                        <h5 class="text-primary">Dr. Tania Hasin</h5>
                                        <p class="m-t-n5">Dentist</p>
                                    </div>
                                </div>
                                <hr class="hr_primary_color">
                                <div class="row">
                                    <div class="col-md-12 text-center">
                                        <p>“Efficient, clear, and the staff have all been very friendly and helpful. I wish all medical facilities
                                            could be like yours. Keep up the good work.”</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><div class="swiper-slide swiper-slide-duplicate" data-swiper-slide-index="2" style="width: 360px; margin-right: 30px;">
                    <div class="row">
                        <div class="col-md-12 m-t-25">
                            <div class="newsticker_border bg-white index_testimonial_padding index_box_shadow">
                                <div class="row">
                                    <div class="col-xs-4 right_align">
                                        <img src="img/index_team3.png" alt="Image missing" class="img-responsive img-circle swiper_block p-d-3">
                                    </div>
                                    <div class="col-xs-8">
                                        <h5 class="text-primary">Dr. Tarikul Islam</h5>
                                        <p class="m-t-n5">Physician</p>
                                    </div>
                                </div>
                                <hr class="hr_primary_color">
                                <div class="row">
                                    <div class="col-md-12 text-center">
                                        <p>“Physicians and staffs set the bar here.
                                            I feel there may be no better thorough medical CARE program in the country. Outstanding as always.”</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div></div>
            <!--<div class="row">-->
                <!--<div class="col-md-12">-->
            <!--<div class="">-->
                    <div class="swiper-pagination index_swiper swiper-pagination-clickable swiper-pagination-bullets"><span class="swiper-pagination-bullet"></span><span class="swiper-pagination-bullet swiper-pagination-bullet-active"></span><span class="swiper-pagination-bullet"></span></div>
            <!--</div>-->
                <!--</div>-->
            <!--</div>-->
        </div>
    </div>
</section>


<section>
    <div class="container">
        <div class="row m-t-30">
            <div class="col-md-12 m-t-40">
                <h3 class="text-center head_center">Our Experts</h3>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6 m-t-40">
                <div class="experts">
                    <div class="row">
                        <div class="col-md-6 col-xs-12 experts_align">
                            <div class="aboutus_section_bgclr our_team_align">
                            <div class="p-l-r-30 experts_info right_align">
                                <div class="m-t-b-50">
                                    <h6 class="text-primary">Dr. Tahmina Banu</h6>
                                    <span>Hepatology</span>
                                    <hr>
                                    <p>MBBS (DMC), MRCP (UK)<br>
                                        Co-ordinator & Consultant<br>
                                        Internal Medicine
                                    </p>
                                </div>
                            </div>
                                </div>
                        </div>
                        <div class="col-md-6 col-xs-12 experts_padding">
                            <div class="gallery_image">
                                <div class="experts_img CARE_image">
                                    <img src="img/index1_doctor1.png" class="img-responsive" alt="Image missing">
                                    <!--<div class="hover_search">
                                        <div class="links">
                                            <span class="fa-stack fa-lg">
                                                <a href="http://dev.lorvent.com/CARE_pro/images/index1_doctor1.png" data-lightbox="image">
                                                    <i class="fa fa-circle fa-stack-2x font60 text-primary"></i>
                                                    <i class="fa fa-search-plus fa-stack-1x fa-inverse imghover_icon"></i>
                                                </a>
                                            </span>
                                        </div>
                                    </div>-->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 m-t-40">
                <div class="experts">
                    <div class="row">
                        <div class="col-md-6 col-xs-12 experts_align">
                            <div class="aboutus_section_bgclr our_team_align">
                                <div class="p-l-r-30 experts_info right_align">
                                <div class="m-t-b-50">
                                    <h6 class="text-primary">Dr. Hasina Akhtar</h6>
                                    <span>Cardiologist</span>
                                    <hr>
                                    <p>MBBS (UK), D.Card. (London)<br>
                                        Co-ordinator & Consultant<br>
                                        Clinical Cardiology
                                    </p>
                                </div>
                                    </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-xs-12 experts_padding">
                            <div class="gallery_image">
                                <div class="experts_img CARE_image">
                                    <img src="img/index_team4.png" class="img-responsive" alt="Image missing">
                                    <!--<div class="hover_search">
                                        <div class="links">
                                            <span class="fa-stack fa-lg">
                                                <a href="http://dev.lorvent.com/CARE_pro/images/index_team4.png" data-lightbox="image1">
                                                    <i class="fa fa-circle fa-stack-2x font60 text-primary"></i>
                                                    <i class="fa fa-search-plus fa-stack-1x fa-inverse imghover_icon"></i>
                                                </a>
                                            </span>
                                        </div>
                                    </div>-->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6 m-t-40">
                <div class="experts">
                    <div class="row">
                        <div class="col-md-6 col-xs-12 experts_align">
                            <div class="aboutus_section_bgclr our_team_align">
                            <div class="p-l-r-30 experts_info right_align">
                                <div class="m-t-b-50">
                                    <h6 class="text-primary">Dr. Nazmul Ahsan</h6>
                                    <span>Dental Surgeon</span>
                                    <hr>
                                    <p>BDS, FCPS, PhD (Japan)<br>
                                        Co-ordinator & Consultant<br>
                                        Dental Surgery
                                    </p>
                                </div>
                            </div>
                                </div>
                        </div>
                        <div class="col-md-6 col-xs-12 experts_padding">
                            <div class="gallery_image">
                                <div class="experts_img CARE_image">
                                    <img src="img/index_team1.png" class="img-responsive" alt="Image missing">
                                    <!--<div class="hover_search">
                                        <div class="links">
                                            <span class="fa-stack fa-lg">
                                                <a href="http://dev.lorvent.com/CARE_pro/images/index_team1.png" data-lightbox="image2">
                                                    <i class="fa fa-circle fa-stack-2x font60 text-primary"></i>
                                                    <i class="fa fa-search-plus fa-stack-1x fa-inverse imghover_icon"></i>
                                                </a>
                                            </span>
                                        </div>
                                    </div>-->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 m-t-40">
                <div class="experts">
                    <div class="row">
                        <div class="col-md-6 col-xs-12 experts_align">
                            <div class="aboutus_section_bgclr our_team_align">
                            <div class="p-l-r-30 experts_info right_align">
                                <div class="m-t-b-50">
                                    <h6 class="text-primary">Dr. Iffat Ara</h6>
                                    <span>General Surgeon</span>
                                    <hr>
                                    <p>MBBS (GS), DNB (Surgery)<br>
                                        Co-ordinator & Consultant<br>
                                        General Surgery
                                    </p>
                                </div>
                            </div>
                                </div>
                        </div>
                        <div class="col-md-6 col-xs-12 experts_padding">
                            <div class="gallery_image">
                                <div class="experts_img CARE_image">
                                    <img src="img/index_team5.png" class="img-responsive" alt="Image missing">
                                    <!--<div class="hover_search">
                                        <div class="links">
                                            <span class="fa-stack fa-lg">
                                                <a href="http://dev.lorvent.com/CARE_pro/images/index_team5.png" data-lightbox="image3">
                                                    <i class="fa fa-circle fa-stack-2x font60 text-primary"></i>
                                                    <i class="fa fa-search-plus fa-stack-1x fa-inverse imghover_icon"></i>
                                                </a>
                                            </span>
                                        </div>
                                    </div>-->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<section class="index1_rating background_image_align m-t-100">
    <div class="container">
        <div class="row">
            <div class="col-md-12 m-t-45">
                <h3 class="text-center head_center_white text-white">Service Ratings</h3>
                <div class="row">
                    <div class="col-md-offset-3 col-md-6 m-t-10">
                        <div class="text-center text-white">
                            <p>
                                The quality of the patient environment and our safety standards are consistently rated 'excellent'
                                by the Government and independent surveys regularly place us amongst the top trusts
                                in the country for standards of care.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row ratings m-b-60">
            <div class="col-sm-4 col-xs-12">
                <div class="text-center">
                    <div id="circle1"></div>
                    <h4 class="text-white text-center m-t-n15">Background Investigations</h4>
                </div>
            </div>
            <div class="col-sm-4 col-xs-12">
                <div class="text-center">
                    <div id="circle2"></div>
                    <h4 class="text-white m-t-n15">Electronic Claims Management</h4>
                </div>
            </div>
            <div class="col-sm-4 col-xs-12">
                <div class="text-center">
                    <div id="circle3"></div>
                    <h4 class="text-white m-t-n15">Insurance Services</h4>
                </div>
            </div>
        </div>
    </div>
</section>

<!--<section class="index1_rating background_image_align m-t-100">
    <div class="container">
        <div class="row">
            <div class="col-md-12 m-t-45">
                <h3 class="text-center head_center_white text-white">Service Ratings</h3>
                <div class="row">
                    <div class="col-md-offset-3 col-md-6 m-t-10">
                        <div class="text-center text-white">
                            <p>
                                Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum
                                has been the industry's standard dummy text.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
		<div class="row ratings m-b-60">
            <div class="col-sm-4 col-xs-12">
                <div class="text-center">
                    <div id="circle1" class=""><svg xmlns="http://www.w3.org/2000/svg" version="1.1" viewBox="0 0 194 186" class="circliful"></svg></div>
                    <h4 class="text-white text-center m-t-n15">Background Investigations</h4>
                </div>
            </div>
            <div class="col-sm-4 col-xs-12">
                <div class="text-center">
                    <div id="circle2" class=""><svg xmlns="http://www.w3.org/2000/svg" version="1.1" viewBox="0 0 194 186" class="circliful"></svg></div>
                    <h4 class="text-white m-t-n15">Electronic Claims Management</h4>
                </div>
            </div>
            <div class="col-sm-4 col-xs-12">
                <div class="text-center">
                    <div id="circle3" class=""><svg xmlns="http://www.w3.org/2000/svg" version="1.1" viewBox="0 0 194 186" class="circliful"></svg></div>
                    <h4 class="text-white m-t-n15">Insurance Services</h4>
                </div>
            </div>
        </div>
	
    </div>
</section>-->

<section>
    <div class="container">
        <div class="row m-t-30">
            <div class="col-md-12 m-t-40">
                <h3 class="head_center text-center">Latest News</h3>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-4 m-t-40">
                <div class="newsticker_border index_box_shadow">
                    <div class="news_img_border">
                        <img src="img/index1_news_img3.jpg" class="img-responsive" alt="Image missing">
                        <div class="radius10">
                            <div class="news_date bg-primary text-center">20 <br>Apr</div>
                            <div class="news_year bg-white text-primary text-center"> 2017</div>
                        </div>
                    </div>
                    <div class="news_info p-l-r-30 radius10">
                        <h4 class="text-center">Free Consultation</h4>
                        <p><span><i class="fa fa-user text-primary"></i></span>&nbsp;&nbsp;<a href="#"> By Admin</a>
                            <span class="p-l-10"><i class="fa fa-comments text-primary"></i></span>&nbsp;<a href="#">5 comments</a></p>
                        <p>
                            Free Consultation by Globally Renowned Liver Transplantation Surgeon at SIRIUS CARE...
                        </p>
                        <div class="text-center m-b-20">
                            <a href="#" class="btn btn-primary">
                                Read More
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-4 m-t-40">
                <div class="newsticker_border index_box_shadow">
                    <div class="news_img_border">
                        <img src="img/index1_news_img1.jpg" class="img-responsive" alt="Image missing">
                        <div class="radius10">
                            <div class="news_date bg-primary text-center">10 <br>Mar</div>
                            <div class="news_year bg-white text-primary text-center"> 2017</div>
                        </div>
                    </div>
                    <div class="news_info p-l-r-30 radius10">
                        <h4 class="text-center">New Insrument</h4>
                        <p><span><i class="fa fa-user text-primary"></i></span>&nbsp;&nbsp; <a href="#"> By Admin</a>
                            <span class="p-l-10"><i class="fa fa-comments text-primary"></i></span>&nbsp; <a href="#">6 comments</a></p>
                        <p>
                            Introducing GAMMA PROBE for the 1st Time in Bangladesh in the Treatment of Breast Cancer...
                        </p>
                        <div class="text-center m-b-20">
                            <a href="#" class="btn btn-primary">
                                Read More
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-4 m-t-40">
                <div class="newsticker_border index_box_shadow">
                    <div class="news_img_border">
                        <img src="img/index1_news_img2.jpg" class="img-responsive" alt="Image missing">
                        <div class="radius10">
                            <div class="news_date bg-primary text-center">28 <br>Feb</div>
                            <div class="news_year bg-white text-primary text-center"> 2017</div>
                        </div>
                    </div>
                    <div class="news_info p-l-r-30 radius10">
                        <h4 class="text-center">Newly Appointed</h4>
                        <p>
                            <span><i class="fa fa-user text-primary"></i></span>&nbsp;&nbsp;
                            <a href="#">By Admin</a>
                            <span class="p-l-10"><i class="fa fa-comments text-primary"></i></span>&nbsp;
                            <a href="#">8 comments</a>
                        </p>
                        <p>SIRIUS CARE welcomes new family member Dr. Kabir khan, Consultant - Urology...</p>
                        <div class="text-center m-b-20">
                            <a href="#" class="btn btn-primary">
                                Read More
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<section id="contact">
    <div class="container">
        <div class="row m-t-50">
            <div class="col-sm-8 m-t-40">
                <h4 class="head_left_underline">Have Any Questions?</h4>
                <fieldset class="form_shadow m-t-40">
                    <form class="p-d-40" action="#" method="post">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="contact_name">Name *</label>
                                    <input type="text" name="fullname" class="form-control"
                                           id="contact_name" placeholder="Enter Name" required>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="contact_email">Email *</label>
                                    <input type="email" name="email_address" class="form-control"
                                           id="contact_email" placeholder="Enter Email" required>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="subject">Subject </label>
                                    <input type="text" name="subject" class="form-control"
                                           id="subject" placeholder="Subject">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="phone">Phone *</label>
                                    <input type="text" name="contact" class="form-control"
                                           id="phone" placeholder="Enter Phone Number" required>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="message">Message</label>
                                        <textarea name="contact_text" id="message" cols="30" rows="6" class="form-control"
                                                  placeholder="Enter Message"></textarea>
                                </div>
                            </div>
                        </div>

                        <div>
                            <input type="submit" class="btn btn-primary" value="Send your Message">
                            <button type="reset" class="btn btn-primary contact_reset_btn m-l-10">
                                Reset
                            </button>
                        </div>
                    </form>
                </fieldset>
            </div>

            <div class="col-sm-4 m-t-40">
                <h4 class="head_left_underline">Get in touch with us</h4>
                <p class="m-t-30">
                    If you want to ask us a question, or make a comment about our service,
                    there are a number of ways for you to get in contact:
                </p>
                <div class="m-t-20">
                <ul class="tp-banner_social_icons contact_icons list-inline">
                    <li class="facebook_hvr">
                        <a href="contact_us_2.html#"><i class="fa fa-facebook text-center"></i></a>
                    </li>
                    <li class="twitter_hvr">
                        <a href="contact_us_2.html#"><i class="fa fa-twitter text-center"></i></a>
                    </li>
                    <li class="gplus_hvr">
                        <a href="contact_us_2.html#"><i class="fa fa-google-plus text-center"></i></a>
                    </li>
                </ul>
                </div>
                <div class="row m-t-10">
                    <div class="col-sm-2 col-xs-2 m-t-10">
                        <img src="img/contact_phn.png" alt="Image missing"/>
                    </div>
                    <div class="col-sm-10 col-xs-10">
                        <h5>Contact Number</h5>
                        <span class="font13">654732, 655791, 651242</span>
                    </div>
                </div>
                <div class="row m-t-10">
                    <div class="col-sm-2 col-xs-2 m-t-10">
                        <img src="img/contact_email.png" alt="Image missing"/>
                    </div>
                    <div class="col-sm-10 col-xs-10">
                        <h5>Email Address</h5>
                        <span class="font13">info@sirius.org</span>
                    </div>
                </div>
                <div class="row m-t-10">
                    <div class="col-xs-2 m-t-10">
                        <span><img src="img/location.png" alt="Image missing"/></span>
                    </div>
                    <div class="col-xs-10">
                        <h5>Office Location</h5>
                        <span class="font13">GEC Circle, Chittagong </span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<div class="thumbnail m-t-100">
	<div>
	<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3689.8567853513964!2d91.81904731432166!3d22.359035846411793!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x30acd8901c1285bd%3A0x7fa786f5b8d965c0!2sGEC+Circle+Bus+Stop!5e0!3m2!1sen!2s!4v1488462579839" width="100%" height="400px" frameborder="0" style="border:0" allowfullscreen></iframe>
	</div>
</div>
<!--<div id="contact_map" class="button_margin m-t-100" style="position: relative; overflow: hidden;">
	<div style="height: 100%; width: 100%; position: absolute; top: 0px; left: 0px; background-color: rgb(229, 227, 223);">
		<div>
		<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3689.8567853513964!2d91.81904731432166!3d22.359035846411793!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x30acd8901c1285bd%3A0x7fa786f5b8d965c0!2sGEC+Circle+Bus+Stop!5e0!3m2!1sen!2s!4v1488462579839" width="100%" height="400px" frameborder="0" style="border:0" allowfullscreen></iframe>
	</div>
	</div>
</div>-->

<!-----------------Start of Footer---------------------->
        <section class="footer_bg" id="footer">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6 col-md-3 m-t-40">
                        <h5 class="text-white head_white">Contact Us</h5>
                        <p class="text-justify font13 m-t-30 text-grey">There are many ways that you can make contact with us:</p>
                        <div class="row footer_homeicon_color">
                            <div class="col-xs-2 m-t-20"><a href="#"><i class="fa font20 fa-home" aria-hidden="true"></i></a>
                            </div>
                            <div class="col-xs-10 m-t-20 text-grey">
                                <p class="font13">SIRIUS CARE<br>947, O.R Nizam Road Chittagong</p>
                            </div>
                        </div>

                        <div class="row footer_link">
                            <div class="col-xs-2  m-b-15"><a href="#"><i class="fa font20 fa-phone home" aria-hidden="true"></i></a>
                            </div>
                            <div class="col-xs-10  m-b-15"><a href="#"><span class="font13 address text-grey"> 654732, 655791, 651242 </span></a>

                            </div>
                        </div>

                        <div class="row footer_link">
                            <div class="col-xs-2">
                                <a href="#">
                                    <i class="fa font20 fa-envelope home" aria-hidden="true"></i>
                                </a>
                            </div>
                            <div class="col-xs-10">
                        <span class="font13">
                            <a href="#" class="address text-grey">info@sirius.org</a>
                        </span>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-xs-12 social_icon m-b-15 footer_icons">
                                <ul class="tp-banner_social_icons list-inline">
                                    <li class="facebook_hvr">
                                        <a href="#"><i class="fa fa-facebook text-center"></i></a>
                                    </li>
                                    <li class="twitter_hvr">
                                        <a href="#"><i class="fa fa-twitter text-center"></i></a>
                                    </li>
                                    <li class="gplus_hvr">
                                        <a href="#"><i class="fa fa-google-plus text-center"></i></a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-6 col-md-3 m-t-40">
                        <h5 class="text-white head_white">Recent Posts</h5>
                        <ul class="newsticker list-unstyled m-t-30" style="height: 240px; overflow: hidden;">
                            <li style="margin-top: 0px;">
                                <div class="row ">
                                    <div class="col-xs-3 col-sm-3 col-md-4 m-b-15">
                                        <img src="img/index_team3.png" alt="loading" class="img-responsive img-circle
                                img_border image_width">
                                    </div>
                                    <div class="col-xs-9 col-sm-9 col-md-8 m-b-15 footer_link">
                                        <a href="#/CARE_pro/blog_single.html">
                                            <p class="font13 text-grey home">Breast screening <br>
                                                Mar14, 2017</p>
                                        </a>
                                    </div>
                                </div>
                            </li><li style="margin-top: 0px;">
                                <div class="row">
                                    <div class="col-xs-3 col-sm-3 col-md-4 m-b-15">
                                        <img src="img/index_team4.png" alt="loading" class="img-responsive img-circle
                                img_border image_width">
                                    </div>
                                    <div class="col-xs-9 col-sm-9 col-md-8 m-b-15 text-grey footer_link">
                                        <a href="#/CARE_pro/blog_single.html">
                                            <p class="font13 text-grey home">Diagnostic imaging <br>
                                                Jan20, 2017</p>
                                        </a>
                                    </div>
                                </div>
                            </li><li style="margin-top: 0px;">
                                <div class="row">
                                    <div class="col-xs-3 col-sm-3 col-md-4 m-b-15">
                                        <img src="img/index_team5.png" alt="loading" class="img-responsive img-circle
                                img_border image_width">
                                    </div>
                                    <div class="col-xs-9 col-sm-9 col-md-8 m-b-15 footer_link">
                                        <a href="#/CARE_pro/blog_single.html">
                                            <p class="font13 text-grey home">Endoscopy <br>
                                                Aug20, 2016</p>
                                        </a>
                                    </div>
                                </div>
                            </li><li style="margin-top: 0px;">
                                <div class="row">
                                    <div class="col-xs-3 col-sm-3 col-md-4 m-b-15">
                                        <img src="img/index1_doctor1.png" alt="loading" class="img-responsive img-circle
                                img_border image_width">
                                    </div>
                                    <div class="col-xs-9 col-sm-9 col-md-8 m-b-15 footer_link">
                                        <a href="#/CARE_pro/blog_single.html">
                                            <p class="font13 text-grey home">Adverse event <br>
                                                Jul20, 2016</p>
                                        </a>
                                    </div>
                                </div>
                            </li><li style="margin-top: 0px;">
                                <div class="row m-b-15">
                                    <div class="col-xs-3 col-sm-3 col-md-4">
                                        <img src="img/index_team1.png" alt="loading" class="img-responsive img-circle
                                img_border image_width">
                                    </div>
                                    <div class="col-xs-9 col-sm-9 col-md-8 footer_link">
                                        <a href="#/CARE_pro/blog_single.html">
                                            <p class="font13 text-grey home">Each Child is special<br>
                                                Jun11, 2016</p>
                                        </a>
                                    </div>
                                </div>
                            </li><li style="margin-top: 0px;">
                                <div class="row">
                                    <div class="col-xs-3 col-sm-3 col-md-4 m-b-15">
                                        <img src="img/index_team2.png" alt="loading" class="img-responsive img-circle
                                img_border image_width">
                                    </div>
                                    <div class="col-xs-9 col-sm-9 col-md-8 m-b-15 footer_link">
                                        <a href="#/CARE_pro/blog_single.html">
                                            <p class="font13 text-grey home">Acute CARE <br>
                                                May30, 2016
                                            </p>
                                        </a>
                                    </div>
                                </div>
                            </li></ul>
                    </div>
                    <div class="clearfix visible-sm-block"></div>
                    <div class="col-sm-6 col-md-3 business_hours m-t-40">
                        <h5 class="text-white head_white">Business Hours</h5>
                        <table class="table borderless font13 m-t-20">
                            <tbody>
                            <tr>
                                <td class="text-grey">Saturday</td>
                                <td class="text-right text-grey">10 A.M - 11 P.M</td>
                            </tr>
                            <tr>
                                <td class="text-grey">Sunday</td>
                                <td class="text-right text-grey">10 A.M - 10 P.M</td>
                            </tr>
                            <tr>
                                <td class="text-grey">Monday</td>
                                <td class="text-right text-grey">10 A.M - 10 P.M</td>
                            </tr>
                            <tr>
                                <td class="text-grey">Tuesday</td>
                                <td class="text-right text-grey">11 A.M - 10 P.M</td>
                            </tr>
                            <tr>
                                <td class="text-grey">Wednesday</td>
                                <td class="text-right text-grey">09 A.M - 08 P.M</td>
                            </tr>
                            <tr>
                                <td class="text-grey">Thursday</td>
                                <td class="text-right text-grey">10 A.M - 09 P.M</td>
                            </tr>
                            <tr>
                                <td class="text-grey">Friday</td>
                                <td class="text-right text-grey">02 P.M - 08 P.M</td>
                            </tr>
                            </tbody>
                        </table>
                        <!--</div>-->

                    </div>
                    <div class="col-sm-6 col-md-3 m-t-40">
                        <form action="#" method="post" id="subscribe">
                            <h5 class="text-white head_white">Newsletter</h5>
                            <div class="row m-t-30">
                                <div class="col-xs-2"><img src="img/mail-sent.png" class="image-responsive" alt="loading">
                                </div>
                                <div class="col-xs-10 subscribe">
                                    <span class="m-t-50 text-grey">Sign Up for our Newsletter<br>to get Fresh Updates.</span>
                                </div>
                            </div>
                            <div class="input-group">
                                <input class="form-control m-t-20" style="width: 90%" placeholder="Enter Your Name" required="" type="text">
                                <input class="form-control m-t-20" style="width: 90%" id="email" placeholder="Enter Your Email" name="email" required="" type="email">
                                <button type="submit" class="btn btn-default btn-md m-t-20 m-b-15 text-white">Subscribe</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>

<section class="bg_footer_bottom">
    <div class="container">
        <div class="row">
            <div class="col-md-6 text-grey footer_copy font13">
                <p class="footer_pad">Copyright ©2017 &nbsp; Powered by
                    <a href="#?ref=lcrm" class="text-grey"> &nbsp;TeamSirius</a></p>
            </div>

            <div class="col-md-6  footer_copy footer_pad text-left font13">
                <ul class="list-inline">
                    <li><a href="#" class="text-grey">Home &nbsp;|</a></li>
                    <li><a href="#" class="text-grey" data-toggle="modal" data-target="#appointment">Appointment &nbsp;|</a></li>
                    <li><a href="#departments" class="text-grey">Departments &nbsp;|</a></li>
                    <li><a href="#contact" class="text-grey">Contact&nbsp;</a></li><a href=
                </ul>
            </div>
        </div>
        <a href="#" id="return-to-top" style="display: none;"><img src="img/backtotop.png" alt="image-missing"></a>
    </div>
</section>

<div class="modal" id="appointment" tabindex="-1" role="dialog" aria-hidden="false">
    <div class="modal-dialog modal-lg">
        <div class="modal-content animated zoomIn">
            <div class="modal-header bg-primary">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title text-white text-center">Make An Appointment</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-6 col-xs-12 m-t-40">
                        <fieldset class="form_shadow">
                            <form class="p-d-40" action="store.php" method="post">
                                <div class="form-group aboutus_select consult_dept">
                                    <select class="form-control select2 dept_select2 select2-hidden-accessible" style="width: 100%" name="depts" required="" tabindex="-1" aria-hidden="true">
                                        <option selected="selected" value="" disabled="disabled">Choose</option>
                                        <?php
                                        foreach($deptData as $oneDept) {

                                            echo "<option value =".$oneDept->name." >$oneDept->name</option >";
                                        }?>
                                    </select><span class="select2 select2-container select2-container--default" dir="ltr" style="width: 100%;"><span class="selection"><span class="select2-selection select2-selection--single" role="combobox" aria-haspopup="true" aria-expanded="false" tabindex="0" aria-labelledby="select2-depts-8k-container"><span class="select2-selection__rendered" id="select2-depts-8k-container" title="Choose">Choose</span><span class="select2-selection__arrow" role="presentation"><b role="presentation"></b></span></span></span><span class="dropdown-wrapper" aria-hidden="true"></span></span>
                                </div>
                                <div class="form-group">
                                    <label>First Name</label>
                                    <br>
                                    <input class="form-control input_name" name="fname" placeholder="First Name" required type="text">
                                </div>
                                <div class="form-group">
                                    <label>Last Name</label>
                                    <br>
                                    <input class="form-control input_name" name="lastname" placeholder="Last Name" type="text">
                                </div>
                                <div class="form-group">
                                    <label>Phone Number</label>
                                    <br>
                                    <input class="form-control input_name" name="num" placeholder="Phone Number" required type="text">
                                </div>
                                <div class="form-group">
                                    <label>Email Address</label>
                                    <br>
                                    <input class="form-control input_name" name="emails" placeholder="Email Address" required type="email">
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-6 col-xs-12">
                                            <label>Date</label>
                                            <br>
                                            <div class="input-group date datetimepicker_single datetimepicker_res">
                                                <input class="form-control" placeholder="MM/DD/YYYY" name="dates" required type="text">
                                                <span class="input-group-addon">
                                                    <span class="glyphicon glyphicon-calendar"></span>
                                                </span>
                                            </div>
                                        </div>
                                        <div class="clearfix visible-xs-block"></div>
                                        <div class="col-md-6 col-xs-12">
                                            <label>Time</label>
                                            <div class="input-group date datetimepicker_single2 datetimepicker_res">
                                                <input class="form-control" placeholder="HH:MM" name="hours" required type="text">
                                                <span class="input-group-addon">
                                                    <span class="glyphicon glyphicon-time"></span>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="text-center m-t-20">
                                    <button type="submit" class="btn btn-primary">Submit</button>
                                </div>
                            </form>
                        </fieldset>
                    </div>
                    <div class="col-sm-6 col-xs-12 m-t-20">
                        <img src="img/about_female.png" class="img-responsive" alt="Image missing">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<section>
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="modal fade" id="signup" role="dialog">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content dept_model">
                            <div class="modal-header ">
                                <button type="button" class="close" data-dismiss="modal">×</button>
                                <h4 class="modal-title text-white text-center" > Login / Sign Up </h4 >;

                            </div>
                            <div class="modal-body p-l-r-30">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <h4 class="text-primary text-center">Welcome To SIRIUS CARE</h4>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6 p-d-25">
                                        <div class="">
                                            <fieldset class="field_form">
                                                <h4 class="text-center head_center">Login</h4>
                                                <form action="#">
                                                    <div class="row form-group m-t-30">
                                                        <div class="col-sm-4">
                                                            <label for="username" class="control-label">User
                                                                Name</label>
                                                        </div>
                                                        <div class="col-sm-8">
                                                            <input class="form-control" id="username" placeholder="User Name" type="text">
                                                        </div>
                                                    </div>
                                                    <div class="row form-group m-t-15">
                                                        <div class="col-sm-4">
                                                            <label for="pwd" class="control-label">Password</label>
                                                        </div>
                                                        <div class="col-sm-8">
                                                            <input class="form-control" id="pwd" placeholder="Password" type="password">
                                                        </div>
                                                    </div>
                                                    <div class="row form-group m-t-25">
                                                        <div class="col-sm-6">
                                                            <div class="icheckbox_minimal-blue" style="position: relative;"><input class="form-control appointment_check" id="check" style="position: absolute; opacity: 0;" type="checkbox"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins></div>
                                                            <span class="m-l-10">Remember me</span>
                                                        </div>
                                                        <div class="col-sm-6 text-right">
                                                            <a href="#" class="text-primary">Forgot Password?</a>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12 text-center m-t-10">
                                                        <button class="btn btn-primary btn-lg">
                                                            Login
                                                        </button>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-sm-12 m-t-20 text-center header_modal_icons">
                                                            <ul class="tp-banner_social_icons list-inline login_icons">
                                                                <li><span>Login With</span></li>
                                                                <li class="facebook_hvr">
                                                                    <a href="#">
                                                                        <i class="fa fa-facebook text-center"></i>
                                                                    </a>
                                                                </li>
                                                                <li class="twitter_hvr">
                                                                    <a href="#">
                                                                        <i class="fa fa-twitter text-center"></i>
                                                                    </a>
                                                                </li>
                                                                <li class="gplus_hvr">
                                                                    <a href="#">
                                                                        <i class="fa fa-google-plus text-center"></i>
                                                                    </a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </form>
                                            </fieldset>
                                        </div>
                                    </div>
                                    <div class="col-md-6 p-d-25">
                                        <fieldset>
                                            <h4 class="text-center head_center">Sign Up</h4>
                                            <form action="#">
                                                <div class="row form-group m-t-30">
                                                    <div class="col-sm-4">
                                                        <label for="firstname" class="control-label">First Name:</label>
                                                    </div>
                                                    <div class="col-sm-8">
                                                        <input class="form-control" id="firstname" type="text"></div>
                                                </div>
                                                <div class="row form-group m-t-15">
                                                    <div class="col-sm-4">
                                                        <label for="lastname" class="control-label">Last Name:</label>
                                                    </div>
                                                    <div class="col-sm-8">
                                                        <input class="form-control" id="lastname" type="text"></div>
                                                </div>
                                                <div class="row form-group m-t-15">
                                                    <div class="col-sm-4">
                                                        <label for="email" class="control-label">Email:</label></div>
                                                    <div class="col-sm-8">
                                                        <input class="form-control" id="email_1" type="email"></div>
                                                </div>
                                                <div class="row form-group m-t-15">
                                                    <div class="col-sm-4">
                                                        <label for="password" class="control-label">Password:</label>
                                                    </div>
                                                    <div class="col-sm-8">
                                                        <input class="form-control" id="password" type="password"></div>
                                                </div>
                                                <div class="row form-group m-t-15">
                                                    <div class="col-sm-4">
                                                        <label for="password_reenter" class="control-label">Confirm Password:</label>
                                                    </div>
                                                    <div class="col-sm-8">
                                                        <input class="form-control" id="password_reenter" type="password"></div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-sm-12 text-center">
                                                        <button class="btn btn-primary btn-lg">
                                                            Sign Up
                                                        </button>
                                                    </div>
                                                </div>
                                            </form>
                                        </fieldset>
                                    </div>
                                </div>
                                <div class="modal-footer department_model_footer">
                                    <!--<a href="#" class="btn btn-primary text-right">SUBMIT</a>-->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!------------------End of Footer------------------------>
<!--------------------End of Body---------------------->

<!------------------Global JS------------------------>

<script async src="js/analytics.js"></script>
<script type="text/javascript" src="js/jquery1.7.js" style=""></script>
<script type="text/javascript" src="js/swiper.js"></script>
<script type="text/javascript" src="js/lightbox.js"></script>
<script type="text/javascript" src="js/newsTicker.js"></script>
<script type="text/javascript" src="js/js.js"></script>
<script type="text/javascript" src="js/jquery_002.js"></script>
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery_003.js"></script>
<!----------------Custom JS----------------------->

<script type="text/javascript" src="js/custom.js"></script>
<script>
        $(document).ready(function(){
            $("#msg").fadeIn(600);
            $("#msg").fadeOut(600);
        })
</script>


<div id="lightboxOverlay" class="lightboxOverlay" style="display: none;"></div>
<!--<div id="lightbox" class="lightbox" style="display: none;">
<div class="lb-outerContainer">
<div class="lb-container">
<img class="lb-image" src="data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw==">
<div class="lb-nav"><a class="lb-prev" href=""></a><a class="lb-next" href=""></a></div>
<div class="lb-loader"><a class="lb-cancel"></a></div>
</div>
</div>
<div class="lb-dataContainer">
<div class="lb-data">
<div class="lb-details">
<span class="lb-caption"></span><span class="lb-number"></span>
</div>
<div class="lb-closeContainer"><a class="lb-close"></a></div>
</div>
</div>
</div>-->

<div class="swal2-container">
	<div class="swal2-modal" style="display: none" tabindex="-1">
		<ul class="swal2-progresssteps"></ul>
		<div class="swal2-icon swal2-error">
			<span class="x-mark"><span class="line left"></span>
			<span class="line right"></span></span>
		</div>
		<div class="swal2-icon swal2-question">?</div>
		<div class="swal2-icon swal2-warning">!</div>
		<div class="swal2-icon swal2-info">i</div>
		<div class="swal2-icon swal2-success">
			<span class="line tip"></span> 
			<span class="line long"></span>
			<div class="placeholder"></div> 
			<div class="fix"></div>
		</div>
		<img class="swal2-image"/>
		<h2></h2>
		<div class="swal2-content"></div>
		<input class="swal2-input"><input class="swal2-file" type="file">
		<div class="swal2-range">
			<output></output>
			<input value="50" type="range">
		</div>
		<select class="swal2-select"></select>
		<div class="swal2-radio"></div>
		<label for="swal2-checkbox" class="swal2-checkbox"><input type="checkbox"></label>
		<textarea class="swal2-textarea"></textarea>
		<div class="swal2-validationerror"></div><hr class="swal2-spacer">
		<button type="button" class="swal2-confirm">OK</button>
		<button type="button" class="swal2-cancel">Cancel</button>
		<span class="swal2-close">×</span>
	</div>
</div>
</body>
</html>