<?php

include_once "../../vendor/autoload.php";


if(!isset($_SESSION))session_start();

use App\Message\Message;
use App\Utility\Utility;

$objappoint = new App\Appointment\Appointment();
$appointData = $objappoint->view();

/*
$obj = new User();
$obj->setData($_SESSION);

$singleUser = $obj->view();
*/
$admin = new App\Admin\Auth();
$status = $admin->setData($_SESSION)->is_registered();
$login = $admin->setData($_SESSION)->logged_in();


$objDept = new App\Department\Department();
$deptData = $objDept->section();

$msg = Message::getMessage();
?>

<!DOCTYPE html>
<html lang="en"><head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>SIRIUS CARE</title>
    <link rel="icon" type="image/png" sizes="32x32" href="img/icon.png">

    <!-----------------Global CSS ----------------------->

    <link rel="stylesheet" type="text/css" href="css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="css/font-awesome.css">
    <link rel="stylesheet" type="text/css" href="css/animate.css">
    <link rel="stylesheet" type="text/css" href="css/all.css">
    <link rel="stylesheet" type="text/css" href="css/ihover.min.css">
    <link rel="stylesheet" type="text/css" href="css/lightbox.css">
    <link rel="stylesheet" type="text/css" href="css/select2.css">
    <link rel="stylesheet" type="text/css" href="css/bootstrap-datetimepicker.css">
    <link rel="stylesheet" type="text/css" href="css/swiper.css">
    <link rel="stylesheet" type="text/css" href="css/jquery.css">
    <link rel="stylesheet" type="text/css" href="css/layers.css">
    <link rel="stylesheet" type="text/css" href="css/navigation.css">
    <link rel="stylesheet" type="text/css" href="css/settings.css">
    <link rel="stylesheet" type="text/css" href="css/sweetalert.css">

    <!------------------Custom CSS --------------------------->

    <link rel="stylesheet" type="text/css" href="css/custom.css">

    <script type="text/javascript" charset="UTF-8" src="js/common.js"></script>
    <script type="text/javascript" charset="UTF-8" src="js/map.js"></script>
    <script type="text/javascript" charset="UTF-8" src="js/util.js"></script>
    <script type="text/javascript" charset="UTF-8" src="js/geocoder.js"></script>
    <script type="text/javascript" charset="UTF-8" src="js/onion.js"></script>
    <script type="text/javascript" charset="UTF-8" src="js/stats.js"></script>
    <script type="text/javascript" charset="UTF-8" src="js/controls.js"></script>
    <script type="text/javascript" charset="UTF-8" src="js/marker.js"></script>

</head>
<body>
<!--------------------Start of Preloader-------------------->

<div class="preloader" style="position: fixed; width: 100%; height: 100%; top: 0px; left: 0px; z-index: 100000; backface-visibility: hidden; background: rgb(255, 255, 255) none repeat scroll 0% 0%; display: none;">
    <div style=" width: 50px;
    height: 50px;
    position: absolute;
    left: 43%;
    top: 50%;
    background-position: center;
    margin:-25px 0 0 -25px;">
        <img src="img/loader.gif" alt="loading..." style="display: none;">
    </div>
</div>
<!-----------------End of Preloader------------------------->


<!----------------Start of Header-------------------------->
<div class="tp-banner">
    <div class="container">
        <div class="row">
            <div class="col-md-7 col-sm-4 col-xs-5 p-d-12 font13">
                <a href="#" class="p-d-10 text-white"><i class="fa fa-map-marker" aria-hidden="true"></i>
                    <span class="hidden-xs hidden-sm">Chittagong, Bangladesh</span></a>
                <a href="#" class="p-d-10 text-white"><i class="fa fa-phone" aria-hidden="true"></i>
                    <span class="hidden-xs hidden-sm">031-654732</span> </a>
                <a href="#" class="p-d-10 text-white"><i class="fa fa-envelope" aria-hidden="true"></i>
                    <span class="hidden-xs hidden-sm">info@sirius.org</span></a>
            </div>
            <div class="col-md-5 col-sm-8 col-xs-7 text-right">
                <div class="row">
                    <div class="col-md-6 col-sm-7 p-d-10 font13 hidden-xs">
                        <span><?php
                            if($status || $login){

                                echo '<span class="text-white hidden-xs">WELCOME ADMIN</span>';
                            }else{
                                echo   '<a href="#" class="text-white" data-toggle="modal" data-target="#signup">
                                      <i class="fa fa-user"></i>
                                      <span class="hidden-xs">Login / Sign Up</span>
                                      </a>';
                            }
                            ?>
                        </span>
                    </div>
                    <div class="col-md-6 col-sm-5 header_top">
                        <ul class="tp-banner_social_icons list-inline">
                            <li class="facebook_hvr">
                                <a href="#"><i class="fa fa-facebook text-center"></i></a>
                            </li>
                            <li class="twitter_hvr">
                                <a href="#"><i class="fa fa-twitter text-center"></i></a>
                            </li>
                            <li class="gplus_hvr">
                                <a href="#"><i class="fa fa-google-plus text-center"></i></a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



<header class="header1_align header1">
    <!--<div class="container">-->
    <nav class="navbar navbar-default">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="navbar-header">
                        <button class="navbar-toggle" type="button" data-toggle="collapse" data-target="#myMegamenu"><span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="http://dev.lorvent.com/CARE_pro/index.html">
                            <img src="img/logo.png" class="img-responsive m-t-n5" alt="medical-logo">
                        </a>
                    </div>
                    <div class="collapse navbar-collapse navbar-right" id="myMegamenu">
                        <div class="header_nav header1_nav">
                            <ul class="nav navbar-nav">
                                <li class="list2 mont"><a href="index.php">HOME</a></li>
                                <!--<li class="dropdown blog_dropdown"><a href="#" class="dropdown-toggle menu_hover" data-toggle="dropdown">
                                   <span class="mont">HOME</span><span class="CAREt"></span></a>
                                  <ul class="sub_menu dropdown-menu blog_menu">
                                       <li>
                                           <a href="#">Home Page-1</a>
                                       </li>
                                       <li>
                                           <a href="#">Home Page-2</a>
                                       </li>
                                       <li>
                                           <a href="#">Home Page-3</a>
                                       </li>
                                       <li>
                                           <a href="#">Home Page-4</a>
                                       </li>
                                       <li>
                                           <a href="#">Home Page-5</a>
                                       </li>
                                       <li>
                                           <a href="#">Home Page-6</a>
                                       </li>
                                   </ul>
                               </li>-->
                                <li class="list2 mont"><a href="#about">ABOUT US</a></li>

                                <!--<li class="dropdown mega-dropdown"><a href="#" class="dropdown-toggle dept_menu" data-toggle="dropdown">
                                    <span class="mont">SIRIUS HUB </span> <span class="CAREt"></span></a>
                                    <ul class="row sub_menu dropdown-menu mega-dropdown-menu">
                                        <li class="col-sm-3">
                                            <ul>
                                                <li class="dropdown-header header_li_underline mont">DEPARTMENTS</li>
                                                <li class="m-t-5">
                                                    <a href="#">
                                                        <i class="fa fa-angle-double-right text-primary"></i>
                                                        Dept. of Dentology
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        <i class="fa fa-angle-double-right text-primary"></i>
                                                        Dept. of Cardiology
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        <i class="fa fa-angle-double-right text-primary"></i>
                                                        Dept. of Gastroenterology
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        <i class="fa fa-angle-double-right text-primary"></i>
                                                        Dept. of Gynaecology
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        <i class="fa fa-angle-double-right text-primary"></i>
                                                        Dept. of Laboratory
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        <i class="fa fa-angle-double-right text-primary"></i>
                                                        Area of Departments
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        <i class="fa fa-angle-double-right text-primary"></i>
                                                        Departments Single Post
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        <i class="fa fa-angle-double-right text-primary"></i>
                                                        Departments Timetable
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        <i class="fa fa-angle-double-right text-primary"></i>
                                                        About SIRIUS CARE
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        <i class="fa fa-angle-double-right text-primary"></i>
                                                        Our History
                                                    </a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="col-sm-3">
                                            <ul>
                                                <li class="dropdown-header header_li_underline mont">CORE PAGES</li>
                                                <li class="m-t-5">
                                                    <a href="#">
                                                        <i class="fa fa-angle-double-right text-primary"></i>
                                                        Our Doctors
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        <i class="fa fa-angle-double-right text-primary"></i>
                                                        Career Opportunities
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        <i class="fa fa-angle-double-right text-primary"></i>
                                                        Career Details
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        <i class="fa fa-angle-double-right text-primary"></i>
                                                        Donate Blood
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        <i class="fa fa-angle-double-right text-primary"></i>
                                                        Our Gallery
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        <i class="fa fa-angle-double-right text-primary"></i>
                                                        Our Facilities
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        <i class="fa fa-angle-double-right text-primary"></i>
                                                        Events Single Post
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        <i class="fa fa-angle-double-right text-primary"></i>
                                                        Health Checkup
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        <i class="fa fa-angle-double-right text-primary"></i>
                                                        Health Events
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        <i class="fa fa-angle-double-right text-primary"></i>
                                                        Success Stories
                                                    </a>
                                                </li>
                                            </ul>
                                        </li>

                                        <li class="col-sm-3">
                                            <ul>
                                                <li class="dropdown-header header_li_underline mont">GET IN TOUCH</li>
                                               <li class="m-t-5">
                                                    <a href="#">
                                                        <i class="fa fa-angle-double-right text-primary"></i>
                                                        404 Page
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        <i class="fa fa-angle-double-right text-primary"></i>
                                                        Styles Page
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        <i class="fa fa-angle-double-right text-primary"></i>
                                                        FAQ
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        <i class="fa fa-angle-double-right text-primary"></i>
                                                        Interview Tips
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        <i class="fa fa-angle-double-right text-primary"></i>
                                                        Terms &amp; Conditions
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        <i class="fa fa-angle-double-right text-primary"></i>
                                                        Our Branches
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        <i class="fa fa-angle-double-right text-primary"></i>
                                                        Appointment
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        <i class="fa fa-angle-double-right text-primary"></i>
                                                        Contact Us
                                                    </a>
                                                </li>
                                             <li>
                                                    <a href="#">
                                                        <i class="fa fa-angle-double-right text-primary"></i>
                                                        Contact Us 2
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        <i class="fa fa-angle-double-right text-primary"></i>
                                                        Contact Us 3
                                                    </a>
                                                </li>
                                            </ul>
                                        </li>

                                        <li class="col-sm-3">
                                            <ul>
                                                <li>
                                                    <img src="img/header1.png" class="img-responsive" alt="Image">
                                                </li>
                                            </ul>
                                        </li>
                                    </ul>
                                </li>-->

                                <li class="list2 mont"><a href="#departments">DEPARTMENTS</a></li>
                                <!--<li class="list2 mont"><a href="#">GALLERY</a></li>-->
                                <li class="list2 mont"><a href="#contact">CONTACT US</a></li>
                                <!-- <li class="dropdown blog_dropdown"><a href="#" class="dropdown-toggle menu_hover" data-toggle="dropdown">
                                     <span class="mont">GALLERY</span><span class="CAREt"></span></a>
                                     <ul class="sub_menu dropdown-menu blog_menu">
                                         <li>
                                             <a href="#">Two Columns</a>
                                         </li>
                                         <li>
                                             <a href="#">Three Columns</a>
                                         </li>
                                         <li>
                                             <a href="#">Four Columns</a>
                                         </li>
                                     </ul>
                                 </li>
                                 <li class="dropdown blog_dropdown"><a href="#" class="dropdown-toggle menu_hover" data-toggle="dropdown">
                                     <span class="mont">BLOG</span><span class="CAREt"></span></a>
                                     <ul class="sub_menu dropdown-menu blog_menu">
                                         <li>
                                             <a href="#">Blog</a>
                                         </li>
                                         <li>
                                             <a href="#">Blog Single</a>
                                         </li>
                                         <li>
                                             <a href="#">Blog Left Sidebar</a>
                                         </li>
                                         <li>
                                             <a href="#">Blog No Sidebar</a>
                                         </li>
                                     </ul>
                                 </li>-->
                                <!--<li class="list2 mont"><a href="#">CONTACT</a></li>
                                <li class="dropdown blog_dropdown"><a href="#" class="dropdown-toggle menu_hover" data-toggle="dropdown">
                                    <span class="mont">CONTACT</span><span class="CAREt"></span></a>
                                    <ul class="sub_menu dropdown-menu blog_menu contact_menu">
                                        <li>
                                            <a href="#">Contact Us-1</a>
                                        </li>
                                        <li>
                                            <a href="#">Contact Us-2</a>
                                        </li>
                                        <li>
                                            <a href="#">Contact Us-3</a>
                                        </li>
                                    </ul>
                                </li>-->
                                <li class="list2 search_icon"><a href="#"><i class="fa fa-search"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </nav>
    <div id="search_large">
        <div class="search_wrap">
            <form class="search p-l-10">
                    <span class="text">
                        <input name="search" id="search_input" class="input-search" placeholder="Search..." type="text">
                    </span>
                <button type="submit" class="search-submit font20" id="submit_search">
                    <i class="fa fa-search text-white"></i>
                </button>
            </form>
            <button id="search-close">
                <i class="fa fa-times font20 text-white" aria-hidden="true"></i>
            </button>
        </div>
    </div>
</header>
<!------------------------End of Header----------------------->
