
<!-----------------Start of Footer---------------------->
<section class="footer_bg" id="footer">
    <div class="container">
        <div class="row">
            <div class="col-sm-6 col-md-3 m-t-40">
                <h5 class="text-white head_white">Contact Us</h5>
                <p class="text-justify font13 m-t-30 text-grey">There are many ways that you can make contact with us:</p>
                <div class="row footer_homeicon_color">
                    <div class="col-xs-2 m-t-20"><a href="#"><i class="fa font20 fa-home" aria-hidden="true"></i></a>
                    </div>
                    <div class="col-xs-10 m-t-20 text-grey">
                        <p class="font13">SIRIUS CARE<br>947, O.R Nizam Road Chittagong</p>
                    </div>
                </div>

                <div class="row footer_link">
                    <div class="col-xs-2  m-b-15"><a href="#"><i class="fa font20 fa-phone home" aria-hidden="true"></i></a>
                    </div>
                    <div class="col-xs-10  m-b-15"><a href="#"><span class="font13 address text-grey"> 654732, 655791, 651242 </span></a>

                    </div>
                </div>

                <div class="row footer_link">
                    <div class="col-xs-2">
                        <a href="#">
                            <i class="fa font20 fa-envelope home" aria-hidden="true"></i>
                        </a>
                    </div>
                    <div class="col-xs-10">
                        <span class="font13">
                            <a href="#" class="address text-grey">info@sirius.org</a>
                        </span>
                    </div>
                </div>

                <div class="row">
                    <div class="col-xs-12 social_icon m-b-15 footer_icons">
                        <ul class="tp-banner_social_icons list-inline">
                            <li class="facebook_hvr">
                                <a href="#"><i class="fa fa-facebook text-center"></i></a>
                            </li>
                            <li class="twitter_hvr">
                                <a href="#"><i class="fa fa-twitter text-center"></i></a>
                            </li>
                            <li class="gplus_hvr">
                                <a href="#"><i class="fa fa-google-plus text-center"></i></a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="col-sm-6 col-md-3 m-t-40">
                <h5 class="text-white head_white">Recent Posts</h5>
                <ul class="newsticker list-unstyled m-t-30" style="height: 240px; overflow: hidden;">
                    <li style="margin-top: 0px;">
                        <div class="row ">
                            <div class="col-xs-3 col-sm-3 col-md-4 m-b-15">
                                <img src="img/index_team3.png" alt="loading" class="img-responsive img-circle
                                img_border image_width">
                            </div>
                            <div class="col-xs-9 col-sm-9 col-md-8 m-b-15 footer_link">
                                <a href="#/CARE_pro/blog_single.html">
                                    <p class="font13 text-grey home">Breast screening <br>
                                        Mar14, 2017</p>
                                </a>
                            </div>
                        </div>
                    </li><li style="margin-top: 0px;">
                        <div class="row">
                            <div class="col-xs-3 col-sm-3 col-md-4 m-b-15">
                                <img src="img/index_team4.png" alt="loading" class="img-responsive img-circle
                                img_border image_width">
                            </div>
                            <div class="col-xs-9 col-sm-9 col-md-8 m-b-15 text-grey footer_link">
                                <a href="#/CARE_pro/blog_single.html">
                                    <p class="font13 text-grey home">Diagnostic imaging <br>
                                        Jan20, 2017</p>
                                </a>
                            </div>
                        </div>
                    </li><li style="margin-top: 0px;">
                        <div class="row">
                            <div class="col-xs-3 col-sm-3 col-md-4 m-b-15">
                                <img src="img/index_team5.png" alt="loading" class="img-responsive img-circle
                                img_border image_width">
                            </div>
                            <div class="col-xs-9 col-sm-9 col-md-8 m-b-15 footer_link">
                                <a href="#/CARE_pro/blog_single.html">
                                    <p class="font13 text-grey home">Endoscopy <br>
                                        Aug20, 2016</p>
                                </a>
                            </div>
                        </div>
                    </li><li style="margin-top: 0px;">
                        <div class="row">
                            <div class="col-xs-3 col-sm-3 col-md-4 m-b-15">
                                <img src="img/index1_doctor1.png" alt="loading" class="img-responsive img-circle
                                img_border image_width">
                            </div>
                            <div class="col-xs-9 col-sm-9 col-md-8 m-b-15 footer_link">
                                <a href="#/CARE_pro/blog_single.html">
                                    <p class="font13 text-grey home">Adverse event <br>
                                        Jul20, 2016</p>
                                </a>
                            </div>
                        </div>
                    </li><li style="margin-top: 0px;">
                        <div class="row m-b-15">
                            <div class="col-xs-3 col-sm-3 col-md-4">
                                <img src="img/index_team1.png" alt="loading" class="img-responsive img-circle
                                img_border image_width">
                            </div>
                            <div class="col-xs-9 col-sm-9 col-md-8 footer_link">
                                <a href="#/CARE_pro/blog_single.html">
                                    <p class="font13 text-grey home">Each Child is special<br>
                                        Jun11, 2016</p>
                                </a>
                            </div>
                        </div>
                    </li><li style="margin-top: 0px;">
                        <div class="row">
                            <div class="col-xs-3 col-sm-3 col-md-4 m-b-15">
                                <img src="img/index_team2.png" alt="loading" class="img-responsive img-circle
                                img_border image_width">
                            </div>
                            <div class="col-xs-9 col-sm-9 col-md-8 m-b-15 footer_link">
                                <a href="#/CARE_pro/blog_single.html">
                                    <p class="font13 text-grey home">Acute CARE <br>
                                        May30, 2016
                                    </p>
                                </a>
                            </div>
                        </div>
                    </li></ul>
            </div>
            <div class="clearfix visible-sm-block"></div>
            <div class="col-sm-6 col-md-3 business_hours m-t-40">
                <h5 class="text-white head_white">Business Hours</h5>
                <table class="table borderless font13 m-t-20">
                    <tbody>
                    <tr>
                        <td class="text-grey">Saturday</td>
                        <td class="text-right text-grey">10 A.M - 11 P.M</td>
                    </tr>
                    <tr>
                        <td class="text-grey">Sunday</td>
                        <td class="text-right text-grey">10 A.M - 10 P.M</td>
                    </tr>
                    <tr>
                        <td class="text-grey">Monday</td>
                        <td class="text-right text-grey">10 A.M - 10 P.M</td>
                    </tr>
                    <tr>
                        <td class="text-grey">Tuesday</td>
                        <td class="text-right text-grey">11 A.M - 10 P.M</td>
                    </tr>
                    <tr>
                        <td class="text-grey">Wednesday</td>
                        <td class="text-right text-grey">09 A.M - 08 P.M</td>
                    </tr>
                    <tr>
                        <td class="text-grey">Thursday</td>
                        <td class="text-right text-grey">10 A.M - 09 P.M</td>
                    </tr>
                    <tr>
                        <td class="text-grey">Friday</td>
                        <td class="text-right text-grey">02 P.M - 08 P.M</td>
                    </tr>
                    </tbody>
                </table>
                <!--</div>-->

            </div>
            <div class="col-sm-6 col-md-3 m-t-40">
                <form action="#" method="post" id="subscribe">
                    <h5 class="text-white head_white">Newsletter</h5>
                    <div class="row m-t-30">
                        <div class="col-xs-2"><img src="img/mail-sent.png" class="image-responsive" alt="loading">
                        </div>
                        <div class="col-xs-10 subscribe">
                            <span class="m-t-50 text-grey">Sign Up for our Newsletter<br>to get Fresh Updates.</span>
                        </div>
                    </div>
                    <div class="input-group">
                        <input class="form-control m-t-20" style="width: 90%" placeholder="Enter Your Name" required="" type="text">
                        <input class="form-control m-t-20" style="width: 90%" id="email" placeholder="Enter Your Email" name="email" required="" type="email">
                        <button type="submit" class="btn btn-default btn-md m-t-20 m-b-15 text-white">Subscribe</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>

<section class="bg_footer_bottom">
    <div class="container">
        <div class="row">
            <div class="col-md-6 text-grey footer_copy font13">
                <p class="footer_pad">Copyright ©2017 &nbsp; Powered by
                    <a href="#?ref=lcrm" class="text-grey"> &nbsp;TeamSirius</a></p>
            </div>

            <div class="col-md-6  footer_copy footer_pad text-left font13">
                <ul class="list-inline">
                    <li><a href="#" class="text-grey">Home &nbsp;|</a></li>
                    <li><a href="#" class="text-grey" data-toggle="modal" data-target="#appointment">Appointment &nbsp;|</a></li>
                    <li><a href="#departments" class="text-grey">Departments &nbsp;|</a></li>
                    <li><a href="#contact" class="text-grey">Contact&nbsp;</a></li><a href=
                                                                                      </ul>
            </div>
        </div>
        <a href="#" id="return-to-top" style="display: none;"><img src="img/backtotop.png" alt="image-missing"></a>
    </div>
</section>

<div class="modal" id="appointment" tabindex="-1" role="dialog" aria-hidden="false">
    <div class="modal-dialog modal-lg">
        <div class="modal-content animated zoomIn">
            <div class="modal-header bg-primary">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title text-white text-center">Make An Appointment</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-6 col-xs-12 m-t-40">
                        <fieldset class="form_shadow">
                            <form class="p-d-40" action="store.php" method="post">
                                <div class="form-group aboutus_select consult_dept">
                                    <select class="form-control select2 dept_select2 select2-hidden-accessible" style="width: 100%" name="depts" required="" tabindex="-1" aria-hidden="true">
                                        <option selected="selected" value="" disabled="disabled">Choose</option>
                                        <?php
                                        foreach($deptData as $oneDept) {

                                            echo "<option value =".$oneDept->name." >$oneDept->name</option >";
                                        }?>
                                    </select><span class="select2 select2-container select2-container--default" dir="ltr" style="width: 100%;"><span class="selection"><span class="select2-selection select2-selection--single" role="combobox" aria-haspopup="true" aria-expanded="false" tabindex="0" aria-labelledby="select2-depts-8k-container"><span class="select2-selection__rendered" id="select2-depts-8k-container" title="Choose">Choose</span><span class="select2-selection__arrow" role="presentation"><b role="presentation"></b></span></span></span><span class="dropdown-wrapper" aria-hidden="true"></span></span>
                                </div>
                                <div class="form-group">
                                    <label>First Name</label>
                                    <br>
                                    <input class="form-control input_name" name="fname" placeholder="First Name" required type="text">
                                </div>
                                <div class="form-group">
                                    <label>Last Name</label>
                                    <br>
                                    <input class="form-control input_name" name="lastname" placeholder="Last Name" type="text">
                                </div>
                                <div class="form-group">
                                    <label>Phone Number</label>
                                    <br>
                                    <input class="form-control input_name" name="num" placeholder="Phone Number" required type="text">
                                </div>
                                <div class="form-group">
                                    <label>Email Address</label>
                                    <br>
                                    <input class="form-control input_name" name="emails" placeholder="Email Address" required type="email">
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-6 col-xs-12">
                                            <label>Date</label>
                                            <br>
                                            <div class="input-group date datetimepicker_single datetimepicker_res">
                                                <input class="form-control" placeholder="MM/DD/YYYY" name="dates" required type="text">
                                                <span class="input-group-addon">
                                                    <span class="glyphicon glyphicon-calendar"></span>
                                                </span>
                                            </div>
                                        </div>
                                        <div class="clearfix visible-xs-block"></div>
                                        <div class="col-md-6 col-xs-12">
                                            <label>Time</label>
                                            <div class="input-group date datetimepicker_single2 datetimepicker_res">
                                                <input class="form-control" placeholder="HH:MM" name="hours" required type="text">
                                                <span class="input-group-addon">
                                                    <span class="glyphicon glyphicon-time"></span>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="text-center m-t-20">
                                    <button type="submit" class="btn btn-primary">Submit</button>
                                </div>
                            </form>
                        </fieldset>
                    </div>
                    <div class="col-sm-6 col-xs-12 m-t-20">
                        <img src="img/about_female.png" class="img-responsive" alt="Image missing">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<section>
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="modal fade" id="signup" role="dialog">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content dept_model">
                            <div class="modal-header ">
                                <button type="button" class="close" data-dismiss="modal">×</button>
                                <h4 class="modal-title text-white text-center" > Login / Sign Up </h4 >;

                            </div>
                            <div class="modal-body p-l-r-30">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <h4 class="text-primary text-center">Welcome To SIRIUS CARE</h4>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6 p-d-25">
                                        <div class="">
                                            <fieldset class="field_form">
                                                <h4 class="text-center head_center">Login</h4>
                                                <form action="#">
                                                    <div class="row form-group m-t-30">
                                                        <div class="col-sm-4">
                                                            <label for="username" class="control-label">User
                                                                Name</label>
                                                        </div>
                                                        <div class="col-sm-8">
                                                            <input class="form-control" id="username" placeholder="User Name" type="text">
                                                        </div>
                                                    </div>
                                                    <div class="row form-group m-t-15">
                                                        <div class="col-sm-4">
                                                            <label for="pwd" class="control-label">Password</label>
                                                        </div>
                                                        <div class="col-sm-8">
                                                            <input class="form-control" id="pwd" placeholder="Password" type="password">
                                                        </div>
                                                    </div>
                                                    <div class="row form-group m-t-25">
                                                        <div class="col-sm-6">
                                                            <div class="icheckbox_minimal-blue" style="position: relative;"><input class="form-control appointment_check" id="check" style="position: absolute; opacity: 0;" type="checkbox"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins></div>
                                                            <span class="m-l-10">Remember me</span>
                                                        </div>
                                                        <div class="col-sm-6 text-right">
                                                            <a href="#" class="text-primary">Forgot Password?</a>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12 text-center m-t-10">
                                                        <button class="btn btn-primary btn-lg">
                                                            Login
                                                        </button>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-sm-12 m-t-20 text-center header_modal_icons">
                                                            <ul class="tp-banner_social_icons list-inline login_icons">
                                                                <li><span>Login With</span></li>
                                                                <li class="facebook_hvr">
                                                                    <a href="#">
                                                                        <i class="fa fa-facebook text-center"></i>
                                                                    </a>
                                                                </li>
                                                                <li class="twitter_hvr">
                                                                    <a href="#">
                                                                        <i class="fa fa-twitter text-center"></i>
                                                                    </a>
                                                                </li>
                                                                <li class="gplus_hvr">
                                                                    <a href="#">
                                                                        <i class="fa fa-google-plus text-center"></i>
                                                                    </a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </form>
                                            </fieldset>
                                        </div>
                                    </div>
                                    <div class="col-md-6 p-d-25">
                                        <fieldset>
                                            <h4 class="text-center head_center">Sign Up</h4>
                                            <form action="#">
                                                <div class="row form-group m-t-30">
                                                    <div class="col-sm-4">
                                                        <label for="firstname" class="control-label">First Name:</label>
                                                    </div>
                                                    <div class="col-sm-8">
                                                        <input class="form-control" id="firstname" type="text"></div>
                                                </div>
                                                <div class="row form-group m-t-15">
                                                    <div class="col-sm-4">
                                                        <label for="lastname" class="control-label">Last Name:</label>
                                                    </div>
                                                    <div class="col-sm-8">
                                                        <input class="form-control" id="lastname" type="text"></div>
                                                </div>
                                                <div class="row form-group m-t-15">
                                                    <div class="col-sm-4">
                                                        <label for="email" class="control-label">Email:</label></div>
                                                    <div class="col-sm-8">
                                                        <input class="form-control" id="email_1" type="email"></div>
                                                </div>
                                                <div class="row form-group m-t-15">
                                                    <div class="col-sm-4">
                                                        <label for="password" class="control-label">Password:</label>
                                                    </div>
                                                    <div class="col-sm-8">
                                                        <input class="form-control" id="password" type="password"></div>
                                                </div>
                                                <div class="row form-group m-t-15">
                                                    <div class="col-sm-4">
                                                        <label for="password_reenter" class="control-label">Confirm Password:</label>
                                                    </div>
                                                    <div class="col-sm-8">
                                                        <input class="form-control" id="password_reenter" type="password"></div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-sm-12 text-center">
                                                        <button class="btn btn-primary btn-lg">
                                                            Sign Up
                                                        </button>
                                                    </div>
                                                </div>
                                            </form>
                                        </fieldset>
                                    </div>
                                </div>
                                <div class="modal-footer department_model_footer">
                                    <!--<a href="#" class="btn btn-primary text-right">SUBMIT</a>-->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!------------------End of Footer------------------------>
<!--------------------End of Body---------------------->

<!------------------Global JS------------------------>

<script async src="js/analytics.js"></script>
<script type="text/javascript" src="js/jquery1.7.js" style=""></script>
<script type="text/javascript" src="js/swiper.js"></script>
<script type="text/javascript" src="js/lightbox.js"></script>
<script type="text/javascript" src="js/newsTicker.js"></script>
<script type="text/javascript" src="js/js.js"></script>
<script type="text/javascript" src="js/jquery_002.js"></script>
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery_003.js"></script>
<!----------------Custom JS----------------------->

<script type="text/javascript" src="js/custom.js"></script>
<script>
    $(document).ready(function(){
        $("#msg").fadeIn(600);
        $("#msg").fadeOut(600);
    })
</script>


<div id="lightboxOverlay" class="lightboxOverlay" style="display: none;"></div>
<!--<div id="lightbox" class="lightbox" style="display: none;">
<div class="lb-outerContainer">
<div class="lb-container">
<img class="lb-image" src="data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw==">
<div class="lb-nav"><a class="lb-prev" href=""></a><a class="lb-next" href=""></a></div>
<div class="lb-loader"><a class="lb-cancel"></a></div>
</div>
</div>
<div class="lb-dataContainer">
<div class="lb-data">
<div class="lb-details">
<span class="lb-caption"></span><span class="lb-number"></span>
</div>
<div class="lb-closeContainer"><a class="lb-close"></a></div>
</div>
</div>
</div>-->

<div class="swal2-container">
    <div class="swal2-modal" style="display: none" tabindex="-1">
        <ul class="swal2-progresssteps"></ul>
        <div class="swal2-icon swal2-error">
			<span class="x-mark"><span class="line left"></span>
			<span class="line right"></span></span>
        </div>
        <div class="swal2-icon swal2-question">?</div>
        <div class="swal2-icon swal2-warning">!</div>
        <div class="swal2-icon swal2-info">i</div>
        <div class="swal2-icon swal2-success">
            <span class="line tip"></span>
            <span class="line long"></span>
            <div class="placeholder"></div>
            <div class="fix"></div>
        </div>
        <img class="swal2-image"/>
        <h2></h2>
        <div class="swal2-content"></div>
        <input class="swal2-input"><input class="swal2-file" type="file">
        <div class="swal2-range">
            <output></output>
            <input value="50" type="range">
        </div>
        <select class="swal2-select"></select>
        <div class="swal2-radio"></div>
        <label for="swal2-checkbox" class="swal2-checkbox"><input type="checkbox"></label>
        <textarea class="swal2-textarea"></textarea>
        <div class="swal2-validationerror"></div><hr class="swal2-spacer">
        <button type="button" class="swal2-confirm">OK</button>
        <button type="button" class="swal2-cancel">Cancel</button>
        <span class="swal2-close">×</span>
    </div>
</div>
</body>
</html>