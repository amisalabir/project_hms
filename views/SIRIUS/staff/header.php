<?php if(!isset($_SESSION)) {
    header('Location: login.php');
    exit;
}
 ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>SIRIUS:ADMIN</title>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" href="../css/bootstrap.min.css" />
    <link rel="stylesheet" href="../admin/css/bootstrap-responsive.min.css" />
    <link rel="stylesheet" href="../admin/css/fullcalendar.css" />
    <link rel="stylesheet" href="../admin/css/matrix-style.css" />
    <link rel="stylesheet" href="../admin/css/matrix-media.css" />
    <link href="../admin/font-awesome/css/font-awesome.css" rel="stylesheet" />
    <link rel="stylesheet" href="../admin/css/jquery.gritter.css" />
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,800' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="../../../resources/assets/css/bootstrap.min.css" />
    <link rel="stylesheet" href="../../../resources/assets/css/bootstrap-responsive.min.css" />
    <link rel="stylesheet" href="../../../resources/assets/css/fullcalendar.css" />
    <link rel="stylesheet" href="../../../resources/assets/css/matrix-style.css" />
    <link rel="stylesheet" href="../../../resources/assets/css/matrix-media.css" />
    <link href="../../../resources/assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
    <link rel="stylesheet" href="../../../resources/assets/css/jquery.gritter.css" />
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,800' rel='stylesheet' type='text/css'>
    <script src="../admin/cjs/jquery.min.js"></script>
    <script src="../admin/cjs/jquery-1.9.1.min.js"></script>
    <script src="../admin/cjs/bootstrap.min.js"></script>
    <style>
        .view-img{
            max-width: 60px;
            max-height: 40px;
        }
    </style>
</head>
<body>

<!--Header-part-->
<div id="header">
    <h1><a href="index.php">Admin</a></h1>
</div>
<!--close-Header-part-->

<!--top-Header-menu-->
<div id="user-nav" class="navbar navbar-inverse">
    <ul class="nav">
        <li  class="dropdown" id="profile-messages" ><a title="" href="#" data-target="#profile-messages" class="dropdown-toggle"><i class="icon icon-user"></i>  <span class="text">Welcome ! <?php echo $_SESSION['name']; ?></span></a>
        </li>
        <li class="dropdown" id="menu-messages"><a href="#" data-toggle="dropdown" data-target="#menu-messages" class="dropdown-toggle"><i class="icon icon-envelope"></i> <span class="text">Messages</span> <span class="label label-important">5</span> <b class="caret"></b></a>
            <ul class="dropdown-menu">
                <li><a class="sAdd" title="" href="#"><i class="icon-plus"></i> new message</a></li>
                <li class="divider"></li>
                <li><a class="sInbox" title="" href="#"><i class="icon-envelope"></i> inbox</a></li>
                <li class="divider"></li>
                <li><a class="sOutbox" title="" href="#"><i class="icon-arrow-up"></i> outbox</a></li>
                <li class="divider"></li>
                <li><a class="sTrash" title="" href="#"><i class="icon-trash"></i> trash</a></li>
            </ul>
        </li>
        <li class=""><a title="" href="#"><i class="icon icon-cog"></i> <span class="text">Settings</span></a></li>
       <li class=""><a title="" href="logout.php"><i class="icon icon-share-alt"></i> <span class="text">Logout</span></a></li>

    </ul>
</div>
<!--close-top-Header-menu-->
<!--start-top-serch-->
<div id="search">
    <input type="text" placeholder="Search here..."/>
    <button type="submit" class="tip-bottom" title="Search"><i class="icon-search icon-white"></i></button>
</div>