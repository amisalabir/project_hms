<?php
if(!isset($_SESSION))session_start();
$_SESSION['name']='Doctor';
include_once('../../../vendor/autoload.php');

use App\Doctor\Doctor;
use App\Accountant\Accountant;
use App\User\User;
use App\Message\Message;
use App\Utility\Utility;

/*
$obj = new User();
$obj->setData($_SESSION);

$singleUser = $obj->view();
*/
$admin = new App\Admin\Auth();
$status = $admin->setData($_SESSION)->logged_in();

if(!$status){
    Utility::redirect('../../admin/login.php');
}

$objdoctor = new Doctor();
$objDept = new App\Department\Department();
$allData = $objdoctor->index();
$deptData = $objDept->section();

/*
$admin = new App\Admin\Auth();
$status = $admin->setData($_SESSION)->logged_in();

if(!$status){
    Utility::redirect('../../admin/login.php');
}
 */

$objAppoint = new App\Appointment\Appointment();
$allData = $objAppoint->index();

?>
<?php
include "header.php";
?>

<!--close-top-serch-->
<!--sidebar-menu-->
<?php
include ('sidebar_doctor.php');
?>
<!--sidebar-menu-->
<div id="content">
    <!--breadcrumbs-->
    <div id="content-header">
        <div id="breadcrumb"> <a href="../admin/index.php" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a></div>
    </div>

    <!--End-Action boxes-->

    <!--Chart-box-->
    <div class="row-fluid">
        <div class="widget-box">
            <div class="widget-title bg_lg"><span class="icon"><i class="icon-signal"></i></span>
                <h5>Appoitment's List</h5>

            </div>
            <button type="button" class="btn btn-primary"><a href="today_list.php">Today's Appoitment List</a></button>
            <div class="widget-content nopadding">
                <table class="table table-bordered data-table">
                    <thead>
                    <tr>
                        <th>SL</th>
                        <th>Department Name</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Phone</th>
                        <th>Dates</th>
                        <th>Time</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $serial=1;
                    foreach ($allData as $oneData) {

                        echo "
            <tr style='' class='bg-4'>
                <td>$serial</td>
                <td>$oneData->depts</td>
                <td>$oneData->fname $oneData->lastname</td>
                <td>$oneData->emails</td>
                <td>$oneData->num</td>
                <td>$oneData->dates</td>
                <td>$oneData->hours</td>
            </tr>
        ";
                        $serial++;
                    }

                    ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
</div>
<!--End-Chart-box-->

<div class="modal fade doctor-modal" tabindex="-1" role="dialog" aria-labelledby="gridSystemModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="gridSystemModalLabel">Add Doctor</h4>
            </div>
            <div class="modal-body">

                <div class="panel-body">

                    <form role="form" class="form-horizontal form-groups-bordered" action="../store.php" method="post" enctype="multipart/form-data">
                        <input type="hidden" name='doctor' value="doctor">
                        <div class="form-group row">
                            <label for="field-1" class="col-sm-3 control-label">Name :</label>

                            <div class="col-sm-5">
                                <input name="name" class="form-control" id="field-1" type="text">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="field-1" class="col-sm-3 control-label">Email :</label>

                            <div class="col-sm-5">
                                <input name="email" class="form-control" id="field-1" type="email">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="field-1" class="col-sm-3 control-label">Password :</label>

                            <div class="col-sm-5">
                                <input name="password" class="form-control" id="field-1" type="password">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="field-ta" class="col-sm-3 control-label">Address :</label>

                            <div class="col-sm-9">
                                <textarea name="address" class="form-control" id="field-ta"></textarea>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="field-1" class="col-sm-3 control-label">Phone :</label>

                            <div class="col-sm-5">
                                <input name="phone" class="form-control" id="field-1" type="text">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="field-ta" class="col-sm-3 control-label">Department :</label>

                            <div class="col-sm-5">
                                <select name="department_id" class="form-control">
                                    <option value="">Select Department</option>
                                    <?php
                                    foreach($deptData as $oneDept) {

                                        echo "<option value =".$oneDept->name." >$oneDept->name</option >";
                                    }?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="field-ta" class="col-sm-3 control-label">Profile :</label>
                            <textarea name="profile" placeholder="Description"></textarea>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Image :</label>
                            <div class="col-sm-5">
                                <div class="fileinput fileinput-new" data-provides="fileinput"><input type="hidden">
                                    <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;" data-trigger="fileinput">
                                        <img src="http://placehold.it/200x150" alt="...">
                                    </div>
                                    <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 6px;"></div>
                                    <div>
                                    <span class="btn btn-white btn-file">
                                        <span class="fileinput-new">Select image</span>
                                        <span class="fileinput-exists">Change</span>
                                        <input name="image" type="file">
                                    </span>
                                        <a href="#" class="btn btn-orange fileinput-exists" data-dismiss="fileinput">Remove</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3 control-label col-sm-offset-2">
                            <input class="btn btn-success" value="Submit" type="submit">
                        </div>
                    </form>
                </div>
                <!--panel body end-->
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<?php include('footer.php'); ?>








