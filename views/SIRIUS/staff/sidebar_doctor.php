<?php if(!isset($_SESSION)) {
    header('Location: login.php');
    exit;
}
?>
<div id="sidebar"><a href="#" class="visible-phone"><i class="icon icon-home"></i> Dashboard</a>
    <ul>
        <li class=""><a href="index.php"><i class="icon icon-home"></i> <span>Dashboard</span></a> </li>
        <li><a href="appointment.php"><i class="icon icon-fullscreen"></i> <span>Appointment</span></a></li>

    </ul>
</div>