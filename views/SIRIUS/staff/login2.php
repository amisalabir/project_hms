<?php

if(!isset($_SESSION))session_start();
include_once("../../../vendor/autoload.php");
use App\Doctor\Doctor;
use App\Nurse\Nurse;
use App\Recieptionist\Recieptionist;

use App\Message\Message;
use App\Utility\Utility;

$logData = $_POST;

if(in_array('accountant', $logData)){
    $accountant = new App\Accountant\Auth();
    $status = $accountant->setData($_POST)->is_registered();
    if($status) {
        $_SESSION['email'] = $_POST['email'];


        Message::message("<div class=\"alert alert-success\">
        <strong>Welcome!</strong>You are Logged in.
        </div>");


        Utility::redirect('accountant.php');
    }

}elseif(in_array('doctor', $logData)){
    $doctor = new App\doctor\Auth();
    $status = $doctor->setData($_POST)->is_registered();
    if($status) {
        $_SESSION['email'] = $_POST['email'];


        Message::message("<div class=\"alert alert-success\">
        <strong>Welcome!</strong>You are Logged in.
        </div>");
        Utility::redirect('doctor.php');
    }

}elseif(in_array('laboratorist', $logData)) {
        $laboratorist = new App\laboratorist\Auth();
        $status = $laboratorist->setData($_POST)->is_registered();
        if ($status) {
            $_SESSION['email'] = $_POST['email'];


            Message::message("<div class=\"alert alert-success\">
        <strong>Welcome!</strong>You are Logged in.
        </div>");
            Utility::redirect('laboratorist.php');
        }
    }elseif(in_array('nurse', $logData)){
    $nurse = new App\nurse\Auth();
    $status = $nurse->setData($_POST)->is_registered();
            if($status) {
                $_SESSION['email'] = $_POST['email'];


                Message::message("<div class=\"alert alert-success\">
        <strong>Welcome!</strong>You are Logged in.
        </div>");
            }


                Utility::redirect('index.php');
}elseif(in_array('pharmacist', $logData)){
    $pharmacist = new App\pharmacist\Auth();
    $status = $pharmacist->setData($_POST)->is_registered();
                if($status) {
                    $_SESSION['email'] = $_POST['email'];


                    Message::message("<div class=\"alert alert-success\">
        <strong>Welcome!</strong>You are Logged in.
        </div>");



                    Utility::redirect('index.php');
}}
                elseif(in_array('receptionist', $logData)){
    $receptionist = new App\recieptionist\Auth();
    $status = $receptionist->setData($_POST)->is_registered();
                    if($status) {
                        $_SESSION['email'] = $_POST['email'];


                        Message::message("<div class=\"alert alert-success\">
        <strong>Welcome!</strong>You are Logged in.
        </div>");
           Utility::redirect('receptionist.php');
}




}else {
                    Message::message("<div class=\"alert alert-success\"></div>");
                    Utility::redirect($_SERVER['HTTP_REFERER']);
                }