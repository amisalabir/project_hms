<?php
if(!isset($_SESSION))session_start();

include_once('../../../vendor/autoload.php');

use App\Accountant\Accountant;
use App\User\User;
use App\Message\Message;
use App\Utility\Utility;

/*
$obj = new User();
$obj->setData($_SESSION);

$singleUser = $obj->view();
*/
$admin = new App\Admin\Auth();
$status = $admin->setData($_SESSION)->logged_in();

if(!$status){
    Utility::redirect('login.php');
}


$objAccount = new Accountant();
$allData = $objAccount->index();
?>
<?php
include "header.php";
?>

<!--close-top-serch-->
<!--sidebar-menu-->
<?php
include ('sidebar.php');
?>
<!--sidebar-menu-->
<div id="content">
    <!--breadcrumbs-->
    <div id="content-header">
        <div id="breadcrumb"> <a href="../admin/index.php" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a></div>
    </div>

    <!--End-Action boxes-->

    <!--Chart-box-->
    <div class="row-fluid">
        <div class="widget-box">
            <div class="widget-title bg_lg"><span class="icon"><i class="icon-signal"></i></span>

                <h5>Accountant's List</h5>

            </div>
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target=".account-modal">Add Accountant</button>
            <div class="widget-content nopadding">
                <table class="table table-bordered data-table">
                    <thead>
                    <tr>
                        <th>SL</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Address</th>
                        <th>Phone</th>
                        <th>Profile Picture</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $serial=1;
                    foreach ($allData as $oneData) {

                        echo "
            <tr style='' class='bg-4'>
                <td>$serial</td>
                <td>$oneData->name</td>
                <td>$oneData->email</td>
                <td>$oneData->address</td>
                <td>$oneData->phone</td>
                <td> <center><img class='img-circle view-img' src='../staff/images/$oneData->propic'/></center></td>
            </tr>
        ";
                        $serial++;
                    }

                    ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <!--End-Chart-box-->
    <hr/>








    <!--Add Laboratorist-->
    <div class="modal fade account-modal" tabindex="-1" role="dialog" aria-labelledby="gridSystemModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="gridSystemModalLabel">Add Accountant</h4>
                </div>
                <div class="modal-body">


                    <div class="panel-body">

                        <form id="acc" role="form" class="form-horizontal form-groups-bordered go-bottom" action="../store.php" method="post" enctype="multipart/form-data">
                            <input type="hidden" name="accountant" value="accountant">
                            <br/>
                            <div class="form-group row">
                                <label for="field-1" class="col-sm-3 control-label"><i class="fa fa-user-circle" aria-hidden="true"></i>Name :</label>
                                <div class="col-sm-8">
                                    <input type="text" name="name" class="form-control" id="field-1">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="field-1" class="col-sm-3 control-label">Email :</label>

                                <div class="col-sm-8">
                                    <input type="email" name="email" class="form-control" id="field-1">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="field-1" class="col-sm-3 control-label">Password :</label>

                                <div class="col-sm-8">
                                    <input type="password" name="password" class="form-control" id="field-1">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="field-1" class="col-sm-3 control-label">Address :</label>

                                <div class="col-sm-8">
                                    <textarea name="address" class="form-control" id="field-ta"></textarea>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="field-1" class="col-sm-3 control-label">Phone :</label>

                                <div class="col-sm-8">
                                    <input type="text" name="phone" class="form-control" id="field-1">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-3 control-label">Image :</label>

                                <div class="col-sm-8">

                                    <div class="fileinput fileinput-new" data-provides="fileinput"><input type="hidden">
                                        <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;" data-trigger="fileinput">
                                            <img src="http://placehold.it/200x150" alt="...">
                                        </div>
                                        <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 6px;"></div>
                                        <div>
                                            <span class="btn btn-white btn-file">
                                                <span class="fileinput-new">Select image</span>
                                                <span class="fileinput-exists">Change</span>
                                                <input type="file" name="image">
                                            </span>
                                            <a href="#" class="btn fileinput-exists" data-dismiss="fileinput">Remove</a>
                                        </div>
                                    </div>

                                </div>
                            </div>

                            <div class="col-sm-3 control-label col-sm-offset-2">
                                <input type="submit" class="btn btn-success" value="Submit">
                            </div>
                        </form>
                    </div><!--panel body end-->

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
<?php include('footer.php'); ?>








