<?php
if(!isset($_SESSION))session_start();

include_once('../../../vendor/autoload.php');

use App\Department\Department;
use App\User\User;
use App\Message\Message;
use App\Utility\Utility;

/*
$obj = new User();
$obj->setData($_SESSION);

$singleUser = $obj->view();
*/

$admin = new App\Admin\Auth();
$status = $admin->setData($_SESSION)->logged_in();

if(!$status){
    Utility::redirect('login.php');
}


$objDept = new Department();
$allData = $objDept->index();
?>
<?php
include "header.php";
?>

<!--close-top-serch-->
<!--sidebar-menu-->
<?php
include ('sidebar.php');
?>
<!--sidebar-menu-->
<div id="content">
    <!--breadcrumbs-->
    <div id="content-header">
        <div id="breadcrumb"> <a href="../admin/index.php" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a></div>
    </div>

    <!--End-Action boxes-->

    <!--Chart-box-->
    <div class="row-fluid">
        <div class="widget-box">
            <div class="widget-title bg_lg"><span class="icon"><i class="icon-signal"></i></span>

                <h5>Department</h5>

            </div>
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target=".dept-modal">Add Department</button>
            <div class="widget-content nopadding">
                <table class="table table-bordered data-table">
                    <thead>
                    <tr>
                        <th>SL</th>
                        <th>Name</th>
                        <th>Description</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $serial=1;
                    foreach ($allData as $oneData) {

                        echo "
            <tr style='' class='bg-4'>
                <td>$serial</td>
                <td>$oneData->name</td>
                <td>$oneData->description</td>
            </tr>
        ";
                        $serial++;
                    }

                    ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <!--End-Chart-box-->
    <hr/>




    <div class="modal fade dept-modal" tabindex="-1" role="dialog" aria-labelledby="gridSystemModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="gridSystemModalLabel">Modal title</h4>
                </div>
                <div class="modal-body">


                    <div class="panel-body">

                        <form id="acc" role="form" class="form-horizontal form-groups-bordered" action="../store.php" method="post" enctype="multipart/form-data">
                            <input type="hidden" name="dept" value="dept">
                            <div class="form-group row">
                                <label for="field-1" class="col-sm-3 control-label">Name :</label>

                                <div class="col-sm-8">
                                    <input name="name" class="form-control" id="field-1" type="text">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="field-ta" class="col-sm-3 control-label">Description :</label>

                                <div class="col-sm-8">
                                    <textarea name="description" class="form-control" id="field-ta"></textarea>
                                </div>
                            </div>

                            <div class="col-sm-3 control-label col-sm-offset-2">
                                <input class="btn btn-success" value="Submit" type="submit">
                            </div>
                        </form>
                    </div><!--panel body end-->

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
<?php include('footer.php'); ?>








