<?php

if(!isset($_SESSION))session_start();

include_once('../../../vendor/autoload.php');

use App\Accountant\Accountant;
use App\User\User;
use App\Message\Message;
use App\Utility\Utility;

/*
$obj = new User();
$obj->setData($_SESSION);

$singleUser = $obj->view();
*/
$admin = new App\Admin\Auth();
$status = $admin->setData($_SESSION)->logged_in();

if(!$status){
    Utility::redirect('../../admin/login.php');
}


$objAppoint = new App\Appointment\Appointment();
$allData = $objAppoint->today_list();
include "header.php";
?>

<!--close-top-serch-->
<!--sidebar-menu-->
<?php
include "sidebar.php";
?>

<div id="content">
    <!--breadcrumbs-->
    <div id="content-header">
        <div id="breadcrumb"> <a href="index.php" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a></div>
    </div>

    <!--End-Action boxes-->

    <!--Chart-box-->
    <div class="widget-box">
        <div class="widget-title"> <span class="icon"><i class="icon-th"></i></span>
            <h5>Appoitment's List</h5>

        </div>
        <div class="widget-content nopadding">
            <table class="table table-bordered data-table">
                <thead>
                <tr>
                    <th>SL</th>
                    <th>Department Name</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Phone</th>
                    <th>Dates</th>
                    <th>Time</th>
                </tr>
                </thead>
                <tbody>
                <?php
                $serial=1;
                foreach ($allData as $oneData) {

                    echo "
            <tr style='' class='bg-4'>
                <td>$serial</td>
                <td>$oneData->depts</td>
                <td>$oneData->fname $oneData->lastname</td>
                <td>$oneData->emails</td>
                <td>$oneData->num</td>
                <td>$oneData->dates</td>
                <td>$oneData->hours</td>
            </tr>
        ";
                    $serial++;
                }

                ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
<!--End-Chart-box-->
<hr/>


</body>
</html>