<?php
if(!isset($_SESSION))session_start();
$_SESSION['name']="User";

include_once('../../../vendor/autoload.php');

use App\User\User;
use App\User\Auth;
use App\Message\Message;
use App\Utility\Utility;

/*
$obj = new User();
$obj->setData($_SESSION);

$singleUser = $obj->view();
*/
$auth = new Auth();
$status = $auth->setData($_SESSION)->logged_in();


if(!$status){
  Utility::redirect('login.php');
}
?>

<?php
include "header.php";
?>
<!--close-top-serch-->
<!--sidebar-menu-->
<?php
include ('sidebar.php');
?>
<!--sidebar-menu-->

<!--main-container-part-->
<div id="content">
<!--breadcrumbs-->
  <div id="content-header">
    <div id="breadcrumb"> <a href="index.php" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a></div>
  </div>
<!--End-breadcrumbs-->

<!--Action boxes-->

<!--End-Action boxes-->    
<?php
//include "action.php";
?>
<!--Chart-box-->    
  <?php
  include "chartbox.php";
  ?>
<!--End-Chart-box-->

   <?php
   //include "fluid.php";
   ?>
  </div>
</div>

<!--end-main-container-part-->

<!--Footer-part-->

<?php include('footer.php'); ?>