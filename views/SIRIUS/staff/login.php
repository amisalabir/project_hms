<?php

include_once "../../../vendor/autoload.php";


if(!isset($_SESSION))session_start();

use App\Message\Message;
use App\Utility\Utility;

/*
$obj = new User();
$obj->setData($_SESSION);

$singleUser = $obj->view();
*/
$admin = new App\Accountant\Auth();
$status = $admin->setData($_SESSION)->is_registered();
$login = $admin->setData($_SESSION)->logged_in();


    if($status || $login) {
        Utility::redirect('index.php');
    }
?>

<!DOCTYPE html>
<html lang="en">
    
<head>
        <title>Staff Login</title><meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<link rel="stylesheet" href="../admin/css/bootstrap.min.css" />
		<link rel="stylesheet" href="../admin/css/bootstrap-responsive.min.css" />
        <link rel="stylesheet" href="../admin/css/matrix-login.css" />
        <link href="../admin/font-awesome/css/font-awesome.css" rel="stylesheet" />
		<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,800' rel='stylesheet' type='text/css'>

    </head>
    <body>
        <div id="loginbox">            
            <form id="loginform" class="form-vertical" action="login2.php" method="post">
				 <div class="control-group normal_text"> <h3><img src="../admin/img/logo.png" alt="Logo" /></h3></div>

                <div class="control-group normal_text">
                    <select name="department_id" class="form-control">
                        <option value="">Select Account </option>
                        <option value ="accountant" >Accountant</option>
                        <option value ="doctor" >Doctor</option>
                        <option value ="receptionist" >Receptionist</option>
                        <option value ="laboratorist" >Laboratorist</option>
                        <option value ="nurse" >Nurse</option>
                        <option value ="pharmacist" >Pharmacist</option>
                    </select>
                </div>
                <div class="control-group">
                    <div class="controls">
                        <div class="main_input_box">
                            <span class="add-on bg_lg"><i class="icon-user"> </i></span><input type="email" placeholder="email" name="email" required/>
                        </div>
                    </div>
                </div>
                <div class="control-group">
                    <div class="controls">
                        <div class="main_input_box">
                            <span class="add-on bg_ly"><i class="icon-lock"></i></span><input type="password" placeholder="Password" name="password" required/>
                        </div>
                    </div>
                </div>
                <div class="form-actions">
                    <span class="pull-left"><a href="#" class="flip-link btn btn-info" id="to-recover">Lost password?</a></span>
                    <span class="pull-right"><input type="submit" class="btn btn-success" /></span>
                </div>
            </form>
            <form id="recoverform" action="#" class="form-vertical">
				<p class="normal_text">Enter your e-mail address below and we will send you instructions how to recover a password.</p>
				
                    <div class="controls">
                        <div class="main_input_box">
                            <span class="add-on bg_lo"><i class="icon-envelope"></i></span><input type="text" placeholder="E-mail address" />
                        </div>
                    </div>
               
                <div class="form-actions">
                    <span class="pull-left"><a href="#" class="flip-link btn btn-success" id="to-login">&laquo; Back to login</a></span>
                    <span class="pull-right"><a class="btn btn-info"/>Reecover</a></span>
                </div>
            </form>
        </div>
        
        <script src="../admin/js/jquery.min.js"></script>
        <script src="../admin/js/matrix.login.js"></script>
    </body>

</html>
