<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
    <style>
        td{
            font-size: 11px;border: 3px solid grey
        }
    </style>
</head>
<body>
<?php
include_once ('../../../../vendor/autoload.php');
use App\Appointment\Appointment;

$obj= new Appointment();
$recordSet=$obj->today_list();
//var_dump($allData);
$trs="";
$sl=0;

foreach($recordSet as $row) {
    $id =  $row->id;
    $deptName = $row->depts;
    $name =$row->fname." ".$row->lastname;
    $email = $row->emails;
    $phone = $row->num;
    $time = $row->hours;

    $sl++;
    $trs .= "<tr>";
    $trs .= "<td width='8%'> $sl</td>";
    $trs .= "<td width='15%'> $deptName </td>";
    $trs .= "<td width='20%'> $name </td>";
    $trs .= "<td width='20%'> $email</td>";
    $trs .= "<td width='20%'> $phone </td>";
    $trs .= "<td width='17%'> $time</td>";


    $trs .= "</tr>";
}

$html= <<<BITM
<div class="table-responsive">
            <table class="table">
                <thead>
                <tr>
                    <th align='left'>Serial</th>
                    <th align='left' >Department Name</th>
                    <th align='left' >Person Name</th>
                    <th align='left' >Email</th>
                    <th align='left' >Phone</th>
                    <th align='left' >Time</th>

              </tr>
                </thead>
                <tbody>

                  $trs

                </tbody>
            </table>


BITM;


// Require composer autoload
require_once ('../../../../vendor/mpdf/mpdf/mpdf.php');
//Create an instance of the class:

$mpdf = new mPDF();

// Write some HTML code:

$mpdf->WriteHTML($html);

// Output a PDF file directly to the browser
$mpdf->Output('today_list.pdf', 'D');
?>

</body>
</html>