<?php
require_once("../../../vendor/autoload.php");

use App\Utility\Utility;

if(!isset($_SESSION)){
    Utility::redirect('login.php');
};

$accountantPermission = <<<SIRIUS
<li class="active"><a href="index.php"><i class="icon icon-home"></i> <span>Dashboard</span></a> </li>
    <li> <a href="../staff/doctor/doctor.php"><i class="icon icon-signal"></i> <span>Doctor</span></a> </li>
SIRIUS;

$nursePermission = <<<SIRIUS
<li class="active"><a href="index.php"><i class="icon icon-home"></i> <span>Dashboard</span></a> </li>
    <li> <a href="../staff/patient/patient.php"><i class="icon icon-signal"></i> <span>Patient</span></a> </li>
SIRIUS;



/*<li class="active"><a href="index.php"><i class="icon icon-home"></i> <span>Dashboard</span></a> </li>
    <li> <a href="../staff/doctor/doctor.php"><i class="icon icon-signal"></i> <span>Doctor</span></a> </li>

<li> <a href="../staff/Laboratorist/laboratorist.php"><i class="icon icon-signal"></i> <span>Laboratorist</span></a> </li>
<li> <a href="../staff/pharmacist/pharmacist.php"><i class="icon icon-inbox"></i> <span>Pharmacist</span></a> </li>
<li><a href="../staff/Nurse/nurse.php"><i class="icon icon-th"></i> <span>Nurse</span></a></li>
<li><a href="../staff/Receptionist/receptionist.php"><i class="icon icon-fullscreen"></i> <span>Recieptionist</span></a></li>
<!--<li class="submenu"> <a href="#"><i class="icon icon-th-list"></i> <span>Forms</span> <span class="label label-important">3</span></a>
  <ul>
    <li><a href="form-common.html">Basic Form</a></li>
    <li><a href="form-validation.html">Form with Validation</a></li>
    <li><a href="form-wizard.html">Form with Wizard</a></li>
  </ul>
</li>-->
<li><a href="../staff/Accountant/accountant.php"><i class="icon icon-tint"></i> <span>Accountant</span></a></li>
<li><a href="../staff/department/dept.php"><i class="icon icon-pencil"></i> <span>Department</span></a></li>


*/




?>