<?php

if(!isset($_SESSION))session_start();

include_once('../../../../vendor/autoload.php');

use App\Patient\Patient;
use App\Message\Message;
use App\Utility\Utility;

/*
$obj = new User();
$obj->setData($_SESSION);

$singleUser = $obj->view();
*/
$admin = new App\Admin\Auth();
$status = $admin->setData($_SESSION)->logged_in();

if(!$status){
    Utility::redirect('../../admin/login.php');
}


$objPatient = new Patient();
$allData = $objPatient->index();

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>Matrix Admin</title>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" href="../../../../resources/assets/css/bootstrap.min.css" />
    <link rel="stylesheet" href="../../../../resources/assets/css/bootstrap-responsive.min.css" />
    <link rel="stylesheet" href="../../../../resources/assets/css/fullcalendar.css" />
    <link rel="stylesheet" href="../../../../resources/assets/css/matrix-style.css" />
    <link rel="stylesheet" href="../../../../resources/assets/css/matrix-media.css" />
    <link href="../../../../resources/assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
    <link rel="stylesheet" href="../../../../resources/assets/css/jquery.gritter.css" />
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,800' rel='stylesheet' type='text/css'>
    <script src="../../admin/cjs/jquery.min.js"></script>
    <script src="../../admin/cjs/jquery-1.9.1.min.js"></script>
    <script src="../../admin/cjs/bootstrap.min.js"></script>
</head>
<body>

<!--Header-part-->
<div id="header">
    <h1><a href="../../admin/index.php"> Admin</a></h1>
</div>
<!--close-Header-part-->


<!--top-Header-menu-->
<div id="user-nav" class="navbar navbar-inverse">
    <ul class="nav">
        <li  class="dropdown" id="profile-messages" ><a title="" href="#" data-target="#profile-messages" class="dropdown-toggle"><i class="icon icon-user"></i>  <span class="text">Welcome Admin</span></a>
            <!--<ul class="dropdown-menu">
              <li><a href="#"><i class="icon-user"></i> My Profile</a></li>
              <li class="divider"></li>
              <li><a href="#"><i class="icon-check"></i> My Tasks</a></li>
              <li class="divider"></li>
              <li><a href="login.php"><i class="icon-key"></i> Log Out</a></li>
            </ul>-->
        </li>
        <li class="dropdown" id="menu-messages"><a href="#" data-toggle="dropdown" data-target="#menu-messages" class="dropdown-toggle"><i class="icon icon-envelope"></i> <span class="text">Messages</span> <span class="label label-important">5</span> <b class="caret"></b></a>
            <ul class="dropdown-menu">
                <li><a class="sAdd" title="" href="#"><i class="icon-plus"></i> new message</a></li>
                <li class="divider"></li>
                <li><a class="sInbox" title="" href="#"><i class="icon-envelope"></i> inbox</a></li>
                <li class="divider"></li>
                <li><a class="sOutbox" title="" href="#"><i class="icon-arrow-up"></i> outbox</a></li>
                <li class="divider"></li>
                <li><a class="sTrash" title="" href="#"><i class="icon-trash"></i> trash</a></li>
            </ul>
        </li>
        <li class=""><a title="" href="#"><i class="icon icon-cog"></i> <span class="text">Settings</span></a></li>
        <li class=""><a title="" href="../../admin/login.php"><i class="icon icon-share-alt"></i> <span class="text">Logout</span></a></li>
    </ul>
</div>
<!--close-top-Header-menu-->
<!--start-top-serch-->
<div id="search">
    <input type="text" placeholder="Search here..."/>
    <button type="submit" class="tip-bottom" title="Search"><i class="icon-search icon-white"></i></button>
</div>
<!--close-top-serch-->
<!--sidebar-menu-->
<div id="sidebar"><a href="#" class="visible-phone"><i class="icon icon-home"></i> Dashboard</a>
    <ul>
        <li><a href="../../admin/index.php"><i class="icon icon-home"></i> <span>Dashboard</span></a> </li>
        <li class="active"> <a href="#"><i class="icon icon-signal"></i> <span>Doctor</span></a> </li>
        <li> <a href="../Laboratorist/laboratorist.php"><i class="icon icon-signal"></i> <span>Laboratorist</span></a> </li>
        <li> <a href="../pharmacist/pharmacist.php"><i class="icon icon-inbox"></i> <span>Pharmacist</span></a> </li>
        <li><a href="../Nurse/nurse.php"><i class="icon icon-th"></i> <span>Nurse</span></a></li>
        <li><a href="../Receptionist/receptionist.php"><i class="icon icon-fullscreen"></i> <span>Recieptionist</span></a></li>
        <!--<li class="submenu"> <a href="#"><i class="icon icon-th-list"></i> <span>Forms</span> <span class="label label-important">3</span></a>
          <ul>
            <li><a href="form-common.html">Basic Form</a></li>
            <li><a href="form-validation.html">Form with Validation</a></li>
            <li><a href="form-wizard.html">Form with Wizard</a></li>
          </ul>
        </li>-->
        <li><a href="../Accountant/accountant.php"><i class="icon icon-tint"></i> <span>Accountant</span></a></li>
        <li><a href="../department/dept.php"><i class="icon icon-pencil"></i> <span>Department</span></a></li>
        <!--<li class="submenu"> <a href="#"><i class="icon icon-file"></i> <span>Addons</span> <span class="label label-important">5</span></a>
          <ul>
            <li><a href="index2.html">Dashboard2</a></li>
            <li><a href="gallery.html">Gallery</a></li>
            <li><a href="calendar.html">Calendar</a></li>
            <li><a href="invoice.html">Invoice</a></li>
            <li><a href="chat.html">Chat option</a></li>
          </ul>
        </li>
        <li class="submenu"> <a href="#"><i class="icon icon-info-sign"></i> <span>Error</span> <span class="label label-important">4</span></a>
          <ul>
            <li><a href="error403.html">Error 403</a></li>
            <li><a href="error404.html">Error 404</a></li>
            <li><a href="error405.html">Error 405</a></li>
            <li><a href="error500.html">Error 500</a></li>
          </ul>
        </li>-->
        <li class="content"> <span>Monthly Bandwidth Transfer</span>
            <div class="progress progress-mini progress-danger active progress-striped">
                <div style="width: 77%;" class="bar"></div>
            </div>
            <span class="percent">77%</span>
            <div class="stat">21419.94 / 14000 MB</div>
        </li>
        <li class="content"> <span>Disk Space Usage</span>
            <div class="progress progress-mini active progress-striped">
                <div style="width: 87%;" class="bar"></div>
            </div>
            <span class="percent">87%</span>
            <div class="stat">604.44 / 4000 MB</div>
        </li>
    </ul>
</div>

<div id="content">
    <!--breadcrumbs-->
    <div id="content-header">
        <div id="breadcrumb"> <a href="../../admin/index.php" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a></div>
    </div>

    <!--End-Action boxes-->

    <!--Chart-box-->
    <div class="row-fluid">
        <div class="widget-box">
            <div class="widget-title bg_lg"><span class="icon"><i class="icon-signal"></i></span>

                <h5>Patient</h5></div>
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target=".patient-modal">Add Patient</button>

            <div class="widget-content" >
                <div class="row-fluid">
                    <div class="span3">
                        <table class="table table-striped">

                            <tr class="bg-3">
                                <th>Name</th>
                                <th>Email</th>
                                <th>Address</th>
                                <th>Phone</th>
                                <th>Profile Pic</th>
                            </tr>
                            <?php
                            $serial=1;
                            foreach ($allData as $oneData) {

                                echo "
            <tr style='' class='bg-4'>
                <td>$oneData->name</td>
                <td>$oneData->email</td>
                <td>$oneData->address</td>
                <td>$oneData->phone</td>
                <td><img class='img-circle' src='../images/$oneData->propic'/></td>
            </tr>
        ";
                                $serial++;
                            }

                            ?>

                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--End-Chart-box-->
    <hr/>








    <div class="modal fade patient-modal" tabindex="-1" role="dialog" aria-labelledby="gridSystemModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="gridSystemModalLabel">Modal title</h4>
                </div>
                <div class="modal-body">
                        <div class="panel-body">

                            <form role="form" class="form-horizontal form-groups-bordered" action="../store.php" method="post" enctype="multipart/form-data">
                                <input type="hidden" name="patient" value="patient">
                                <div class="form-group row">
                                    <label for="field-1" class="col-sm-3 control-label">Name :</label>

                                    <div class="col-sm-8">
                                        <input name="name" class="form-control" id="field-1" type="text">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="field-1" class="col-sm-3 control-label">Email :</label>

                                    <div class="col-sm-8">
                                        <input name="email" class="form-control" id="field-1" type="email">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="field-1" class="col-sm-3 control-label">Password :</label>

                                    <div class="col-sm-8">
                                        <input name="password" class="form-control" id="field-1" type="password">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="field-ta" class="col-sm-3 control-label">Address :</label>

                                    <div class="col-sm-8">
                                        <textarea name="address" class="form-control" id="field-ta"></textarea>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="field-1" class="col-sm-3 control-label">Phone :</label>

                                    <div class="col-sm-8">
                                        <input name="phone" class="form-control" id="field-1" type="text">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="field-ta" class="col-sm-3 control-label">Sex :</label>

                                    <div class="col-sm-8">
                                        <select name="sex" class="form-control">
                                            <option value="">Select Sex</option>
                                            <option value="male">Male</option>
                                            <option value="female">Female</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="field-1" class="col-sm-3 control-label">Birth Date</label>

                                    <div class="col-sm-8">
                                        <input name="birth_date" class="form-control datepicker" id="field-1" type="text">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="field-1" class="col-sm-3 control-label">Age :</label>

                                    <div class="col-sm-8">
                                        <input name="age" class="form-control" id="field-1" type="number">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="field-ta" class="col-sm-3 control-label">Blood Group</label>

                                    <div class="col-sm-8">
                                        <select name="blood_group" class="form-control">
                                            <option value="">Select Blood Group</option>
                                            <option value="A+">A+</option>
                                            <option value="A-">A-</option>
                                            <option value="B+">B+</option>
                                            <option value="B-">B-</option>
                                            <option value="AB+">AB+</option>
                                            <option value="AB-">AB-</option>
                                            <option value="O+">O+</option>
                                            <option value="O-">O-</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Image :</label>

                                    <div class="col-sm-8">

                                        <div class="fileinput fileinput-new" data-provides="fileinput"><input type="hidden">
                                            <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;" data-trigger="fileinput">
                                                <img src="http://placehold.it/200x150" alt="...">
                                            </div>
                                            <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 6px;"></div>
                                            <div>
                                    <span class="btn btn-white btn-file">
                                        <span class="fileinput-new">Select image</span>
                                        <span class="fileinput-exists">Change</span>
                                        <input name="image" accept="image/*" type="file">
                                    </span>
                                                <a href="#" class="btn btn-orange fileinput-exists" data-dismiss="fileinput">Remove</a>
                                            </div>
                                        </div>

                                    </div>
                                </div>

                                <div class="col-sm-3 control-label col-sm-offset-2">
                                    <input class="btn btn-success" value="Submit" type="submit">
                                </div>
                            </form>
                        </div><!--panel body end-->
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
</div>
</body>
</html>





















