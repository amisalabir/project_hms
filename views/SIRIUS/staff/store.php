
<?php
require_once("../../../vendor/autoload.php");


$objAppoint = new App\Appointment\Appointment();
$objAccount = new App\Accountant\Accountant();
$objPharmacist = new App\Pharmacist\Pharmacist();
$objPatient = new App\Patient\Patient();
$objReceptionist = new App\Recieptionist\Recieptionist();
$objDoctor = new App\Doctor\Doctor();
$objLab = new App\Laboratorist\Laboratorist();
$objDept = new App\Department\Department();
$objNurse = new App\Nurse\Nurse();

$take = $_POST;
if(in_array('accountant', $take)){
    $objAccount->setData($_POST);
    $objAccount->setPropic($_FILES);
    $objAccount->store();
}elseif(in_array('doctor', $take)){
    $objDoctor->setData($_POST);
    $objDoctor->setPropic($_FILES);
    $objDoctor->store();
}elseif(in_array('dept', $take)){
    $objDept->setData($_POST);
    $objDept->store();
}elseif(in_array('laboratorist', $take)){
    $objLab->setData($_POST);
    $objLab->setPropic($_FILES);
    $objLab->store();
}elseif(in_array('nurse', $take)){
    $objNurse->setData($_POST);
    $objNurse->setPropic($_FILES);
    $objNurse->store();
}elseif(in_array('patient', $take)){
    $objPatient->setData($_POST);
    $objPatient->store();
}elseif(in_array('pharmacist', $take)){
    $objPharmacist->setData($_POST);
    $objPharmacist->setPropic($_FILES);
    $objPharmacist->store();
}elseif(in_array('receptionist', $take)){
    $objReceptionist->setData($_POST);
    $objReceptionist->setPropic($_FILES);
    $objReceptionist->store();
}
