<?php if(!isset($_SESSION)) {
    header('Location: login.php');
    exit;
}
?>
<div id="sidebar"><a href="#" class="visible-phone"><i class="icon icon-home"></i> Dashboard</a>
    <ul>
        <li class=""><a href="index.php"><i class="icon icon-home"></i> <span>Dashboard</span></a> </li>
        <li> <a href="doctor.php"><i class="icon icon-signal"></i> <span>Doctor</span></a> </li>
        <li><a href="nurse.php"><i class="icon icon-th"></i> <span>Nurse</span></a></li>
        <li><a href="receptionist.php"><i class="icon icon-fullscreen"></i> <span>Recieptionist</span></a></li>

        <!--
        <li> <a href="../staff/Laboratorist/laboratorist.php"><i class="icon icon-signal"></i> <span>Laboratorist</span></a> </li>
        <li> <a href="../staff/pharmacist/pharmacist.php"><i class="icon icon-inbox"></i> <span>Pharmacist</span></a> </li>

        <li class="submenu"> <a href="#"><i class="icon icon-th-list"></i> <span>Forms</span> <span class="label label-important">3</span></a>
          <ul>
            <li><a href="form-common.html">Basic Form</a></li>
            <li><a href="form-validation.html">Form with Validation</a></li>
            <li><a href="form-wizard.html">Form with Wizard</a></li>
          </ul>
        </li>-->
        <li><a href="accountant.php"><i class="icon icon-tint"></i> <span>Accountant</span></a></li>
        <li><a href="dept.php"><i class="icon icon-pencil"></i> <span>Department</span></a></li>

    </ul>
</div>