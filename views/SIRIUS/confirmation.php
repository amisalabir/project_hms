<?php require_once('../../vendor/autoload.php');

if(!isset($_SESSION))session_start();
use App\Message\Message;
use App\Utility\Utility;

$msg = Message::getMessage();


?>
<!DOCTYPE html>
<html lang="en"><head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>SIRIUS CARE</title>
    <link rel="icon" type="image/png" sizes="32x32" href="img/icon.png">

    <!-----------------Global CSS ----------------------->

    <link rel="stylesheet" type="text/css" href="css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="css/font-awesome.css">
    <link rel="stylesheet" type="text/css" href="css/animate.css">
    <link rel="stylesheet" type="text/css" href="css/all.css">
    <link rel="stylesheet" type="text/css" href="css/ihover.min.css">
    <link rel="stylesheet" type="text/css" href="css/lightbox.css">
    <link rel="stylesheet" type="text/css" href="css/select2.css">
    <link rel="stylesheet" type="text/css" href="css/bootstrap-datetimepicker.css">
    <link rel="stylesheet" type="text/css" href="css/swiper.css">
    <link rel="stylesheet" type="text/css" href="css/jquery.css">
    <link rel="stylesheet" type="text/css" href="css/layers.css">
    <link rel="stylesheet" type="text/css" href="css/navigation.css">
    <link rel="stylesheet" type="text/css" href="css/settings.css">
    <link rel="stylesheet" type="text/css" href="css/sweetalert.css">

    <!------------------Custom CSS --------------------------->

    <link rel="stylesheet" type="text/css" href="css/custom.css">

<script type="text/javascript" charset="UTF-8" src="js/common.js"></script>
<script type="text/javascript" charset="UTF-8" src="js/map.js"></script>
<script type="text/javascript" charset="UTF-8" src="js/util.js"></script>
<script type="text/javascript" charset="UTF-8" src="js/geocoder.js"></script>
<script type="text/javascript" charset="UTF-8" src="js/onion.js"></script>
<script type="text/javascript" charset="UTF-8" src="js/stats.js"></script>
<script type="text/javascript" charset="UTF-8" src="js/controls.js"></script>
<script type="text/javascript" charset="UTF-8" src="js/marker.js"></script>

</head>
<body>
<!--------------------Start of Preloader-------------------->

<div class="preloader" style="position: fixed; width: 100%; height: 100%; top: 0px; left: 0px; z-index: 100000; backface-visibility: hidden; background: rgb(255, 255, 255) none repeat scroll 0% 0%; display: none;">
    <div style=" width: 50px;
    height: 50px;
    position: absolute;
    left: 43%;
    top: 50%;
    background-position: center;
    margin:-25px 0 0 -25px;">
        <img src="img/loader.gif" alt="loading..." style="display: none;">
    </div>
</div>
<!-----------------End of Preloader------------------------->
<!----------------Start of Header-------------------------->

<div class="tp-banner">
    <div class="container">
        <div class="row">
            <div class="col-md-7 col-sm-4 col-xs-5 p-d-12 font13">
                <a href="#" class="p-d-10 text-white"><i class="fa fa-map-marker" aria-hidden="true"></i>
                    <span class="hidden-xs hidden-sm">Chittagong, Bangladesh</span></a>
                <a href="#" class="p-d-10 text-white"><i class="fa fa-phone" aria-hidden="true"></i>
                    <span class="hidden-xs hidden-sm">+880 17******** </span> </a>
                <a href="#" class="p-d-10 text-white"><i class="fa fa-envelope" aria-hidden="true"></i>
                    <span class="hidden-xs hidden-sm">info@sirius.org</span></a>
            </div>
            <div class="col-md-5 col-sm-8 col-xs-7 text-right">
                <div class="row">
                    <div class="col-md-6 col-sm-7 p-d-10 font13 hidden-xs">
                        <span>
                            <a href="#" class="text-white" data-toggle="modal" data-target="#signup">
                                <i class="fa fa-user"></i>
                                <span class="hidden-xs">Login / Sign Up</span>
                            </a>
                        </span>
                    </div>
                    <div class="col-md-6 col-sm-5 header_top">
                        <ul class="tp-banner_social_icons list-inline">
                            <li class="facebook_hvr">
                                <a href="#"><i class="fa fa-facebook text-center"></i></a>
                            </li>
                            <li class="twitter_hvr">
                                <a href="#"><i class="fa fa-twitter text-center"></i></a>
                            </li>
                            <li class="gplus_hvr">
                                <a href="#"><i class="fa fa-google-plus text-center"></i></a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<header class="header1_align header1">
    <!--<div class="container">-->
    <nav class="navbar navbar-default">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="navbar-header">
                        <button class="navbar-toggle" type="button" data-toggle="collapse" data-target="#myMegamenu"><span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="http://dev.lorvent.com/care_pro/index.html">
                            <img src="img/logo.png" class="img-responsive m-t-n5" alt="medical-logo">
                        </a>
                    </div>
                    <div class="collapse navbar-collapse navbar-right" id="myMegamenu">
                        <div class="header_nav header1_nav">
                            <ul class="nav navbar-nav">
								 <li class="list2 mont"><a href="index.php">HOME</a></li>
                                 <!--<li class="dropdown blog_dropdown"><a href="#" class="dropdown-toggle menu_hover" data-toggle="dropdown">
                                    <span class="mont">HOME</span><span class="caret"></span></a>
                                   <ul class="sub_menu dropdown-menu blog_menu">
                                        <li>
                                            <a href="#">Home Page-1</a>
                                        </li>
                                        <li>
                                            <a href="#">Home Page-2</a>
                                        </li>
                                        <li>
                                            <a href="#">Home Page-3</a>
                                        </li>
                                        <li>
                                            <a href="#">Home Page-4</a>
                                        </li>
                                        <li>
                                            <a href="#">Home Page-5</a>
                                        </li>
                                        <li>
                                            <a href="#">Home Page-6</a>
                                        </li>
                                    </ul>-->
                                </li>
								  <li class="list2 mont"><a href="#contact">CONTACT US</a></li>
                               <!-- <li class="dropdown blog_dropdown"><a href="#" class="dropdown-toggle menu_hover" data-toggle="dropdown">
                                    <span class="mont">GALLERY</span><span class="caret"></span></a>
                                    <ul class="sub_menu dropdown-menu blog_menu">
                                        <li>
                                            <a href="#">Two Columns</a>
                                        </li>
                                        <li>
                                            <a href="#">Three Columns</a>
                                        </li>
                                        <li>
                                            <a href="#">Four Columns</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="dropdown blog_dropdown"><a href="#" class="dropdown-toggle menu_hover" data-toggle="dropdown">
                                    <span class="mont">BLOG</span><span class="caret"></span></a>
                                    <ul class="sub_menu dropdown-menu blog_menu">
                                        <li>
                                            <a href="#">Blog</a>
                                        </li>
                                        <li>
                                            <a href="#">Blog Single</a>
                                        </li>
                                        <li>
                                            <a href="#">Blog Left Sidebar</a>
                                        </li>
                                        <li>
                                            <a href="#">Blog No Sidebar</a>
                                        </li>
                                    </ul>
                                </li>-->
                                <!--<li class="list2 mont"><a href="#">CONTACT</a></li>
                                <li class="dropdown blog_dropdown"><a href="#" class="dropdown-toggle menu_hover" data-toggle="dropdown">
                                    <span class="mont">CONTACT</span><span class="caret"></span></a>
                                    <ul class="sub_menu dropdown-menu blog_menu contact_menu">
                                        <li>
                                            <a href="#">Contact Us-1</a>
                                        </li>
                                        <li>
                                            <a href="#">Contact Us-2</a>
                                        </li>
                                        <li>
                                            <a href="#">Contact Us-3</a>
                                        </li>
                                    </ul>
                                </li>-->
                                <li class="list2 search_icon"><a href="#"><i class="fa fa-search"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </nav>
    <div id="search_large">
        <div class="search_wrap">
            <form class="search p-l-10">
                    <span class="text">
                        <input name="search" id="search_input" class="input-search" placeholder="Search..." type="text">
                    </span>
                <button type="submit" class="search-submit font20" id="submit_search">
                    <i class="fa fa-search text-white"></i>
                </button>
            </form>
            <button id="search-close">
                <i class="fa fa-times font20 text-white" aria-hidden="true"></i>
            </button>
        </div>
    </div>
</header>
<!------------------------End of Header----------------------->
<!-------------------------Start of Body Section------------------>
<section>
    <div id="message" class="text-center" >

        <?php echo "<h4 style='color:green;'>$msg</h4>";?>

    </div>
    <div class="text-center table-bordered" >

         <table align="center" width="700px" height="350px" class="table-bordered data-table">
             <thead>
             <tr>   <th>Department Name</th> <td>:</td> <td><b><?php echo $_SESSION['01']; ?></b></td></tr>
             <tr>    <th>Name</th> <td>:</td> <td> <b><?php echo $_SESSION['02']; echo $_SESSION['03']; ?></b></td></tr>
             <tr>    <th>Email</th><td>:</td> <td> <b><?php echo $_SESSION['05']; ?></b></td></tr>
             <tr>    <th>Phone</th><td>:</td> <td><b><?php echo $_SESSION['04']; ?></b></td></tr>
             <tr>    <th>Dates</th><td>:</td> <td><b><?php echo $_SESSION['06']; ?></b></td> </tr>
             <tr>   <th>Time</th><td>:</td> <td><b><?php echo $_SESSION['07']; ?></b></td> </tr>
             </thead>

       </table>

    </div>

</section>




<!-----------------Start of Footer---------------------->

<section class="footer_bg" id="footer">
    <div class="container">
        <div class="row">
            <div class="col-sm-6 col-md-3 m-t-40">
                <h5 class="text-white head_white">HMS CARE</h5>
                <p class="text-justify font13 m-t-30 text-grey">"But I must explain to you how all this mistaken idea of
                    denouncing pleasure and
                    praising pain was born and I will give you a complete account of the system.
                    There are many variations of passages."</p>
                <div class="row footer_homeicon_color">
                    <div class="col-xs-2 m-t-20"><a href="#"><i class="fa font20 fa-home" aria-hidden="true"></i></a>
                    </div>
                    <div class="col-xs-10 m-t-20 text-grey">
                        <p class="font13">HMS CARE 458 ninth Avenue <br>Sydney</p>
                    </div>
                </div>
                <div class="row footer_link">
                    <div class="col-xs-2  m-b-15"><a href="#"><i class="fa font20 fa-phone home" aria-hidden="true"></i></a>
                    </div>
                    <div class="col-xs-10  m-b-15"><a href="#"><span class="font13 address text-grey"> +1-662-705-8591 </span></a>

                    </div>
                </div>
                <div class="row footer_link">
                    <div class="col-xs-2">
                        <a href="#">
                            <i class="fa font20 fa-envelope home" aria-hidden="true"></i>
                        </a>
                    </div>
                    <div class="col-xs-10">
                        <span class="font13">
                            <a href="#" class="address text-grey">email@domain.com</a>
                        </span>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-md-3 m-t-40">
                <h5 class="text-white head_white">Recent Posts</h5>
                <ul class="newsticker list-unstyled m-t-30" style="height: 240px; overflow: hidden;">
                    
                    
                    
                    
                    
                    
                <li style="margin-top: 0px;">
                        <div class="row ">
                            <div class="col-xs-3 col-sm-3 col-md-4 m-b-15">
                                <img src="img/index_team3.png" alt="loading" class="img-responsive img-circle
                                img_border image_width">
                            </div>
                            <div class="col-xs-9 col-sm-9 col-md-8 m-b-15 footer_link">
                                <a href="http://dev.lorvent.com/care_pro/blog_single.html">
                                    <p class="font13 text-grey home">Breast screening <br>
                                        Oct20, 2016</p>
                                </a>
                            </div>
                        </div>
                    </li><li style="margin-top: 0px;">
                        <div class="row">
                            <div class="col-xs-3 col-sm-3 col-md-4 m-b-15">
                                <img src="img/index_team4.png" alt="loading" class="img-responsive img-circle
                                img_border image_width">
                            </div>
                            <div class="col-xs-9 col-sm-9 col-md-8 m-b-15 text-grey footer_link">
                                <a href="http://dev.lorvent.com/care_pro/blog_single.html">
                                    <p class="font13 text-grey home">Diagnostic imaging <br>
                                        Sep20,2016</p>
                                </a>
                            </div>
                        </div>
                    </li><li style="margin-top: 0px;">
                        <div class="row">
                            <div class="col-xs-3 col-sm-3 col-md-4 m-b-15">
                                <img src="img/index_team5.png" alt="loading" class="img-responsive img-circle
                                img_border image_width">
                            </div>
                            <div class="col-xs-9 col-sm-9 col-md-8 m-b-15 footer_link">
                                <a href="http://dev.lorvent.com/care_pro/blog_single.html">
                                    <p class="font13 text-grey home">Endoscopy <br>
                                        Aug20, 2014</p>
                                </a>
                            </div>
                        </div>
                    </li><li style="margin-top: 0px;">
                        <div class="row">
                            <div class="col-xs-3 col-sm-3 col-md-4 m-b-15">
                                <img src="img/index1_doctor1.png" alt="loading" class="img-responsive img-circle
                                img_border image_width">
                            </div>
                            <div class="col-xs-9 col-sm-9 col-md-8 m-b-15 footer_link">
                                <a href="http://dev.lorvent.com/care_pro/blog_single.html">
                                    <p class="font13 text-grey home">Adverse event <br>
                                        Oct20, 2016</p>
                                </a>
                            </div>
                        </div>
                    </li><li style="margin-top: 0px;">
                        <div class="row m-b-15">
                            <div class="col-xs-3 col-sm-3 col-md-4">
                                <img src="img/index_team1.png" alt="loading" class="img-responsive img-circle
                                img_border image_width">
                            </div>
                            <div class="col-xs-9 col-sm-9 col-md-8 footer_link">
                                <a href="http://dev.lorvent.com/care_pro/blog_single.html">
                                    <p class="font13 text-grey home">Each Child is special<br>
                                        Oct20, 2016</p>
                                </a>
                            </div>
                        </div>
                    </li><li style="margin-top: 0px;">
                        <div class="row">
                            <div class="col-xs-3 col-sm-3 col-md-4 m-b-15">
                                <img src="img/index_team2.png" alt="loading" class="img-responsive img-circle
                                img_border image_width">
                            </div>
                            <div class="col-xs-9 col-sm-9 col-md-8 m-b-15 footer_link">
                                <a href="http://dev.lorvent.com/care_pro/blog_single.html">
                                    <p class="font13 text-grey home">Acute care <br>
                                        Oct20, 2016
                                    </p>
                                </a>
                            </div>
                        </div>
                    </li></ul>
            </div>
            <div class="clearfix visible-sm-block"></div>
            <div class="col-sm-6 col-md-3 business_hours m-t-40">
                <h5 class="text-white head_white">Business Hours</h5>
                <table class="table borderless font13 m-t-20">
                    <tbody>
                    <tr>
                        <td class="text-grey">Monday</td>
                        <td class="text-right text-grey">10 A.M - 10 P.M</td>
                    </tr>
                    <tr>
                        <td class="text-grey">Tuesday</td>
                        <td class="text-right text-grey">11 A.M - 10 P.M</td>
                    </tr>
                    <tr>
                        <td class="text-grey">Wednesday</td>
                        <td class="text-right text-grey">09 A.M - 08 P.M</td>
                    </tr>
                    <tr>
                        <td class="text-grey">Thursday</td>
                        <td class="text-right text-grey">10 A.M - 09 P.M</td>
                    </tr>
                    <tr>
                        <td class="text-grey">Friday</td>
                        <td class="text-right text-grey">12 A.M - 08 P.M</td>
                    </tr>
                    <tr>
                        <td class="text-grey">Saturday</td>
                        <td class="text-right text-grey">10 A.M - 11 P.M</td>
                    </tr>
                    <tr>
                        <td class="text-grey">Sunday</td>
                        <td class="text-right text-grey">Closed</td>
                    </tr>
                    </tbody>
                </table>
                <!--</div>-->

            </div>
            <div class="col-sm-6 col-md-3 m-t-40">
                <form action="#" method="post" id="subscribe">
                    <h5 class="text-white head_white">Newsletter</h5>
                    <div class="row m-t-30">
                        <div class="col-xs-2"><img src="img/mail-sent.png" class="image-responsive" alt="loading">
                        </div>
                        <div class="col-xs-10 subscribe">
                            <span class="m-t-50 text-grey">Sign Up for Our Newsletter to get Fresh Updates.</span>
                        </div>
                    </div>
                    <div class="input-group">
                        <input class="form-control m-t-20" placeholder="Enter Your Name" required type="text">
                        <input class="form-control m-t-20" id="email" placeholder="Enter Your Email" name="email" required type="email">
                        <button type="submit" class="btn btn-default btn-md m-t-20 m-b-15 text-white">Subscribe</button>
                    </div>
                </form>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 social_icon m-b-15 footer_icons">
                <ul class="tp-banner_social_icons list-inline">
                    <li class="facebook_hvr">
                        <a href="#"><i class="fa fa-facebook text-center"></i></a>
                    </li>
                    <li class="twitter_hvr">
                        <a href="#"><i class="fa fa-twitter text-center"></i></a>
                    </li>
                    <li class="gplus_hvr">
                        <a href="#"><i class="fa fa-google-plus text-center"></i></a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</section>
<section class="bg_footer_bottom">
    <div class="container">
        <div class="row">
            <div class="col-md-6 text-grey footer_copy font13">
                <p class="footer_pad">Copyright ©2017 &nbsp; powered by
                    <a href="https://themeforest.net/user/lcrm?ref=lcrm" class="text-grey"> &nbsp;SIRIUS CARE</a></p>
            </div>
            <div class="col-md-6  footer_copy footer_pad text-right font13">
                <ul class="list-inline">
                    <li><a href="http://dev.lorvent.com/care_pro/index.html" class="text-grey">Home &nbsp;|</a></li>
                    <li><a href="#" class="text-grey" data-toggle="modal" data-target="#appointment">Appointment
                        &nbsp;|</a></li>
                    <li><a href="#" class="text-grey">Departments &nbsp;|</a></li>
                    <li><a href="#" class="text-grey">Contact&nbsp;</a></li>
                </ul>
            </div>
        </div>
        <a href="#" id="return-to-top" style="display: none;"><img src="img/backtotop.png" alt="image-missing"></a>
    </div>
</section>
<div class="modal" id="appointment" tabindex="-1" role="dialog" aria-hidden="false">
    <div class="modal-dialog modal-lg">
        <div class="modal-content animated zoomIn">
            <div class="modal-header bg-primary">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title text-white text-center">Make An Appointment</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-6 col-xs-12 m-t-40">
                        <fieldset class="form_shadow">
                            <form class="p-d-40" action="store.php" method="post">
                                <div class="form-group aboutus_select consult_dept">
                                    <select class="form-control select2 dept_select2 select2-hidden-accessible" style="width: 100%" name="depts" required="" tabindex="-1" aria-hidden="true">
                                        <option selected="selected" value="" disabled="disabled">Choose</option>
                                        <option value="cardiology">Cardiology</option>
                                        <option value="radiology">Radiology</option>
                                        <option value="neurology">Neurology</option>
                                        <option value="pediatric">Pediatric Clinic</option>
                                        <option value="laryngological">Laryngological</option>
                                    </select><span class="select2 select2-container select2-container--default" dir="ltr" style="width: 100%;"><span class="selection"><span class="select2-selection select2-selection--single" role="combobox" aria-haspopup="true" aria-expanded="false" tabindex="0" aria-labelledby="select2-depts-8k-container"><span class="select2-selection__rendered" id="select2-depts-8k-container" title="Choose">Choose</span><span class="select2-selection__arrow" role="presentation"><b role="presentation"></b></span></span></span><span class="dropdown-wrapper" aria-hidden="true"></span></span>
                                </div>
                                <div class="form-group">
                                    <label>First Name</label>
                                    <br>
                                    <input class="form-control input_name" name="fname" placeholder="First Name" required type="text">
                                </div>
                                <div class="form-group">
                                    <label>Last Name</label>
                                    <br>
                                    <input class="form-control input_name" name="lastname" placeholder="Last Name" type="text">
                                </div>
                                <div class="form-group">
                                    <label>Phone Number</label>
                                    <br>
                                    <input class="form-control input_name" name="num" placeholder="Phone Number" required type="text">
                                </div>
                                <div class="form-group">
                                    <label>Email Address</label>
                                    <br>
                                    <input class="form-control input_name" name="emails" placeholder="Email Address" required type="email">
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-6 col-xs-12">
                                            <label>Date</label>
                                            <br>
                                            <div class="input-group date datetimepicker_single datetimepicker_res">
                                                <input class="form-control" placeholder="MM/DD/YYYY" name="dates" required type="text">
                                                <span class="input-group-addon">
                                                    <span class="glyphicon glyphicon-calendar"></span>
                                                </span>
                                            </div>
                                        </div>
                                        <div class="clearfix visible-xs-block"></div>
                                        <div class="col-md-6 col-xs-12">
                                            <label>Time</label>
                                            <div class="input-group date datetimepicker_single2 datetimepicker_res">
                                                <input class="form-control" placeholder="HH:MM" name="hours" required type="text">
                                                <span class="input-group-addon">
                                                    <span class="glyphicon glyphicon-time"></span>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="text-center m-t-20">
                                    <button type="submit" class="btn btn-primary">Submit</button>
                                </div>
                            </form>
                        </fieldset>
                    </div>
                    <div class="col-sm-6 col-xs-12 m-t-20">
                        <img src="img/about_female.png" class="img-responsive" alt="Image missing">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<section>
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="modal fade" id="signup" role="dialog">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content dept_model">
                            <div class="modal-header ">
                                <button type="button" class="close" data-dismiss="modal">×</button>
                                <h4 class="modal-title text-white text-center">Login / Sign Up</h4>
                            </div>
                            <div class="modal-body p-l-r-30">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <h4 class="text-primary text-center">Welcome To Care Pro Hospital</h4>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6 p-d-25">
                                        <div class="">
                                            <fieldset class="field_form">
                                                <h4 class="text-center head_center">Login</h4>
                                                <form action="#">
                                                    <div class="row form-group m-t-30">
                                                        <div class="col-sm-4">
                                                            <label for="username" class="control-label">User
                                                                Name</label>
                                                        </div>
                                                        <div class="col-sm-8">
                                                            <input class="form-control" id="username" placeholder="User Name" type="text">
                                                        </div>
                                                    </div>
                                                    <div class="row form-group m-t-15">
                                                        <div class="col-sm-4">
                                                            <label for="pwd" class="control-label">Password</label>
                                                        </div>
                                                        <div class="col-sm-8">
                                                            <input class="form-control" id="pwd" placeholder="Password" type="password">
                                                        </div>
                                                    </div>
                                                    <div class="row form-group m-t-25">
                                                        <div class="col-sm-6">
                                                            <div class="icheckbox_minimal-blue" style="position: relative;"><input class="form-control appointment_check" id="check" style="position: absolute; opacity: 0;" type="checkbox"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins></div>
                                                            <span class="m-l-10">Remember me</span>
                                                        </div>
                                                        <div class="col-sm-6 text-right">
                                                            <a href="#" class="text-primary">Forgot Password?</a>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12 text-center m-t-10">
                                                        <button class="btn btn-primary btn-lg">
                                                            Login
                                                        </button>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-sm-12 m-t-20 text-center header_modal_icons">
                                                            <ul class="tp-banner_social_icons list-inline login_icons">
                                                                <li><span>Login With</span></li>
                                                                <li class="facebook_hvr">
                                                                    <a href="#">
                                                                        <i class="fa fa-facebook text-center"></i>
                                                                    </a>
                                                                </li>
                                                                <li class="twitter_hvr">
                                                                    <a href="#">
                                                                        <i class="fa fa-twitter text-center"></i>
                                                                    </a>
                                                                </li>
                                                                <li class="gplus_hvr">
                                                                    <a href="#">
                                                                        <i class="fa fa-google-plus text-center"></i>
                                                                    </a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </form>
                                            </fieldset>
                                        </div>
                                    </div>
                                    <div class="col-md-6 p-d-25">
                                        <fieldset>
                                            <h4 class="text-center head_center">Sign Up</h4>
                                            <form action="#">
                                                <div class="row form-group m-t-30">
                                                    <div class="col-sm-4">
                                                        <label for="firstname" class="control-label">First Name:</label>
                                                    </div>
                                                    <div class="col-sm-8">
                                                        <input class="form-control" id="firstname" type="text"></div>
                                                </div>
                                                <div class="row form-group m-t-15">
                                                    <div class="col-sm-4">
                                                        <label for="lastname" class="control-label">Last Name:</label>
                                                    </div>
                                                    <div class="col-sm-8">
                                                        <input class="form-control" id="lastname" type="text"></div>
                                                </div>
                                                <div class="row form-group m-t-15">
                                                    <div class="col-sm-4">
                                                        <label for="email" class="control-label">Email:</label></div>
                                                    <div class="col-sm-8">
                                                        <input class="form-control" id="email_1" type="email"></div>
                                                </div>
                                                <div class="row form-group m-t-15">
                                                    <div class="col-sm-4">
                                                        <label for="password" class="control-label">Password:</label>
                                                    </div>
                                                    <div class="col-sm-8">
                                                        <input class="form-control" id="password" type="password"></div>
                                                </div>
                                                <div class="row form-group m-t-15">
                                                    <div class="col-sm-4">
                                                        <label for="password_reenter" class="control-label">Confirm Password:</label>
                                                    </div>
                                                    <div class="col-sm-8">
                                                        <input class="form-control" id="password_reenter" type="password"></div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-sm-12 text-center">
                                                        <button class="btn btn-primary btn-lg">
                                                            Sign Up
                                                        </button>
                                                    </div>
                                                </div>
                                            </form>
                                        </fieldset>
                                    </div>
                                </div>
                                <div class="modal-footer department_model_footer">
                                    <!--<a href="#" class="btn btn-primary text-right">SUBMIT</a>-->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!------------------End of Footer------------------------>
<!--------------------End of Body---------------------->

<!------------------Global JS------------------------>

<script async src="js/analytics.js"></script>
<script type="text/javascript" src="js/jquery1.7.js" style=""></script>
<script type="text/javascript" src="js/swiper.js"></script>
<script type="text/javascript" src="js/lightbox.js"></script>
<script type="text/javascript" src="js/newsTicker.js"></script>
<script type="text/javascript" src="js/js.js"></script>
<script type="text/javascript" src="js/jquery_002.js"></script>
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery_003.js"></script>
<!----------------Custom JS----------------------->

<script type="text/javascript" src="js/custom.js"></script>
<script>
    $(document).ready(function(){
        $("#message").fadeIn(600);
        $("#message").fadeOut(600);
        $("#message").fadeIn(600);
        $("#message").fadeOut(600);
        $("#message").fadeIn(600);
        $("#message").fadeOut(600);
        $("#message").fadeIn(600);
        $("#message").fadeOut(600);
    })
</script>
<script>

</script>


<div id="lightboxOverlay" class="lightboxOverlay" style="display: none;"></div>
<!--<div id="lightbox" class="lightbox" style="display: none;">
<div class="lb-outerContainer">
<div class="lb-container">
<img class="lb-image" src="data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw==">
<div class="lb-nav"><a class="lb-prev" href=""></a><a class="lb-next" href=""></a></div>
<div class="lb-loader"><a class="lb-cancel"></a></div>
</div>
</div>
<div class="lb-dataContainer">
<div class="lb-data">
<div class="lb-details">
<span class="lb-caption"></span><span class="lb-number"></span>
</div>
<div class="lb-closeContainer"><a class="lb-close"></a></div>
</div>
</div>
</div>-->

<div class="swal2-container">
	<div class="swal2-modal" style="display: none" tabindex="-1">
		<ul class="swal2-progresssteps"></ul>
		<div class="swal2-icon swal2-error">
			<span class="x-mark"><span class="line left"></span>
			<span class="line right"></span></span>
		</div>
		<div class="swal2-icon swal2-question">?</div>
		<div class="swal2-icon swal2-warning">!</div>
		<div class="swal2-icon swal2-info">i</div>
		<div class="swal2-icon swal2-success">
			<span class="line tip"></span> 
			<span class="line long"></span>
			<div class="placeholder"></div> 
			<div class="fix"></div>
		</div>
		<img class="swal2-image"/>
		<h2></h2>
		<div class="swal2-content"></div>
		<input class="swal2-input"><input class="swal2-file" type="file">
		<div class="swal2-range">
			<output></output>
			<input value="50" type="range">
		</div>
		<select class="swal2-select"></select>
		<div class="swal2-radio"></div>
		<label for="swal2-checkbox" class="swal2-checkbox"><input type="checkbox"></label>
		<textarea class="swal2-textarea"></textarea>
		<div class="swal2-validationerror"></div><hr class="swal2-spacer">
		<button type="button" class="swal2-confirm">OK</button>
		<button type="button" class="swal2-cancel">Cancel</button>
		<span class="swal2-close">×</span>
	</div>
</div>
</body>
</html>