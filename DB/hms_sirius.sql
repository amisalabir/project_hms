-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Mar 15, 2017 at 07:36 AM
-- Server version: 10.1.16-MariaDB
-- PHP Version: 5.5.38

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `hms_sirius`
--

-- --------------------------------------------------------

--
-- Table structure for table `accountant`
--

CREATE TABLE `accountant` (
  `accountant_id` int(11) NOT NULL,
  `name` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `email` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `email_verified` varchar(5) NOT NULL DEFAULT 'Yes',
  `password` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `address` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `phone` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `propic` varchar(111) NOT NULL,
  `soft_delete` varchar(111) NOT NULL DEFAULT 'no'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `accountant`
--

INSERT INTO `accountant` (`accountant_id`, `name`, `email`, `email_verified`, `password`, `address`, `phone`, `propic`, `soft_delete`) VALUES
(1, 'ajlvjkaklk', 'bduj@rocketmail.com', 'Yes', '827ccb0eea8a706c4c34a16891f84e7b', 'ajlfjavlmalkm', '0145522333', 'computer-767781_960_720.jpg', 'no'),
(2, 'ajfkjailkej', 'djakfjaiee@gmail.com', 'Yes', '827ccb0eea8a706c4c34a16891f84e7b', 'jkjamldfkjklaji', '67487641876', 'walpaper.jpg', 'no'),
(15, 'dD', 'FJk@fmIL.COM', 'Yes', '827ccb0eea8a706c4c34a16891f84e7b', 'AKDJFAID', '979746628', 'F1-Fansite.com HD Wallpaper 2011 Hungary F1 GP_18.jpg', 'no'),
(14, '', '', 'Yes', 'd41d8cd98f00b204e9800998ecf8427e', '', '', 'asian_beauty_2-wallpaper-3840x2160.jpg', 'no'),
(13, 'akash', 'fkaj@fkasi.com', 'Yes', '827ccb0eea8a706c4c34a16891f84e7b', 'alfjadh', '47924987', 'bugatti_16c_galibier_wheel-wallpaper-1920x1080.jpg', 'no'),
(12, 'fadf', 'admin@teamsirius.org', 'Yes', '827ccb0eea8a706c4c34a16891f84e7b', 'afdadafd', '1441', 'aircraft_4-wallpaper-3554x1999.jpg', 'no'),
(11, 'falfjdkj', 'aksjh@gmail.com', 'Yes', '827ccb0eea8a706c4c34a16891f84e7b', 'fljajskdj', '14498989', 'dragon_fire_fantasy_girl-1920x1080.jpg', 'no'),
(10, 'FJALJD', 'akash@yahoo.com', 'Yes', '827ccb0eea8a706c4c34a16891f84e7b', 'aksha', '14444', 'city_of_dreams-wallpaper-2560x1440.jpg', 'no'),
(16, '', '', 'Yes', 'd41d8cd98f00b204e9800998ecf8427e', '', '', 'aircraft_4-wallpaper-3554x1999.jpg', 'YES');

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `admin_id` int(11) NOT NULL,
  `name` longtext COLLATE utf8_unicode_ci NOT NULL,
  `email` longtext COLLATE utf8_unicode_ci NOT NULL,
  `password` longtext COLLATE utf8_unicode_ci NOT NULL,
  `email_verified` varchar(5) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Yes',
  `soft_delete` varchar(11) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'no'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`admin_id`, `name`, `email`, `password`, `email_verified`, `soft_delete`) VALUES
(1, 'Mr. Admin', 'admin@teamsirius.org', '827ccb0eea8a706c4c34a16891f84e7b', 'Yes', 'no');

-- --------------------------------------------------------

--
-- Table structure for table `appointment`
--

CREATE TABLE `appointment` (
  `id` int(11) NOT NULL,
  `depts` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `fname` varchar(55) COLLATE utf8_unicode_ci NOT NULL,
  `lastname` varchar(55) COLLATE utf8_unicode_ci NOT NULL,
  `num` varchar(12) COLLATE utf8_unicode_ci NOT NULL,
  `emails` varchar(66) COLLATE utf8_unicode_ci NOT NULL,
  `dates` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `hours` time NOT NULL,
  `soft_delete` varchar(3) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'No'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `appointment`
--

INSERT INTO `appointment` (`id`, `depts`, `fname`, `lastname`, `num`, `emails`, `dates`, `hours`, `soft_delete`) VALUES
(2, 'neurology', 'durjoy', 'barua', '0313164546', 'akhdd@gmail.com', '0000-00-00', '00:00:00', 'No'),
(3, 'laryngological', 'ffffffffv', 'vvaerfa', '14494940940', 'alfkjalkj@gmail.com', '0000-00-00', '05:25:00', 'No'),
(4, 'pediatric', 'vaweava', 'aedasfsf', '616142132164', 'ajlfjkja@gmail.com', '0000-00-00', '06:28:00', 'No'),
(5, 'radiology', 'akash', 'fljaljk', '46546543613', 'gkaor@gmail.com', '0000-00-00', '04:32:00', 'No'),
(6, 'cardiology', 'Kazi', 'Sala ', '01714130077', 'amisalabir@gmail.com', '0000-00-00', '02:35:00', 'No'),
(7, 'cardiology', 'Kazi', 'Sala ', '01714130077', 'amisalabir@gmail.com', '29/03/2017', '02:35:00', 'No'),
(8, 'neurology', 'fr3f3f', 'sxwsx', '43444444', 'amisalabir@gmail.com', '07/03/2017', '04:30:00', 'No'),
(9, 'neurology', 'fr3f3f', 'Sala ', '43444444', 'amisalabir@gmail.com', '07/03/2017', '03:31:00', 'No'),
(10, 'pediatric', 'fr3f3f', 'Sala ', '43444444', 'amisalabir@gmail.com', '07/03/2017', '03:31:00', 'No'),
(11, 'pediatric', 'fr3f3f', 'Sala ', '43444444', 'amisalabir@gmail.com', '07/03/2017', '03:31:00', 'No'),
(12, 'pediatric', 'fr3f3f', 'Sala ', '43444444', 'amisalabir@gmail.com', '07/03/2017', '03:31:00', 'No'),
(13, 'pediatric', 'fr3f3f', 'Sala ', '43444444', 'amisalabir@gmail.com', '07/03/2017', '03:31:00', 'No'),
(14, 'radiology', 'Kazi', 'Sala ', '01714130077', 'amisalabir@gmail.com', '07/03/2017', '04:30:00', 'No'),
(15, 'radiology', 'Kazi', 'Sala ', '01714130077', 'amisalabir@gmail.com', '07/03/2017', '04:30:00', 'No'),
(16, 'radiology', 'Kazi', 'Sala ', '01714130077', 'amisalabir@gmail.com', '07/03/2017', '04:30:00', 'No'),
(17, 'radiology', 'Kazi', 'Sala ', '01714130077', 'amisalabir@gmail.com', '07/03/2017', '04:30:00', 'No'),
(18, 'radiology', 'Kazi', 'Sala ', '01714130077', 'amisalabir@gmail.com', '07/03/2017', '02:35:00', 'No'),
(19, 'neurology', 'Arman', 'Chowdhury', '014654469', 'bitmarman@gmail.com', '07/03/2017', '04:30:00', 'No'),
(20, 'radiology', 'Arman', 'Chowdhury', '01714130077', 'bitmarman@gmail.com', '08/03/2017', '03:30:00', 'No'),
(21, 'Neurology', 'Mohammad', 'Rakib', '01234', 'rakib@email.com', '15/03/2017', '06:45:00', 'No'),
(22, 'Neurology', 'Arman', 'Chowdhury', '01234', 'arman@email.com', '23/03/2017', '08:00:00', 'No'),
(23, 'Cardiology', 'Rakib', 'Hossain', '01234', 'rakib@email.com', '22/03/2017', '08:30:00', 'No'),
(24, 'Hepatology', 'Lutfur', 'Karim', '01234', 'karim@email.com', '21/03/2017', '06:00:00', 'No'),
(25, 'Dental', 'Abdul', 'Hai', '01234', 'abdul@email.com', '25/03/2017', '07:30:00', 'No'),
(26, 'ENT', 'Ratna', 'Rai', '01234', 'ratna@email.com', '28/03/2017', '06:15:00', 'No'),
(27, 'Opthalmology', 'Sabek', 'Islam', '01234', 'sabek@email.com', '27/03/2017', '09:00:00', 'No'),
(28, 'Orthopedics', 'Kalim', 'Ullah', '01234', 'kalim@email.com', '16/03/2017', '04:30:00', 'No'),
(29, 'Cardiology', ' Jubair', 'Ahmed', '01234', 'jubair@email.com', '25/03/2017', '09:30:00', 'No'),
(30, 'Opthalmology', 'Abdul', 'Hai', '01234', 'abdul@email.com', '22/03/2017', '04:45:00', 'No'),
(32, 'Cardiology', 'Habibur ', 'Rahman', '01234', 'habibur@email.com', '14/03/2017', '10:25:00', 'No'),
(33, 'Hepatology', 'Rokib', 'Hossain', '01234', 'rokib@email.com', '28/03/2017', '07:30:00', 'No'),
(34, 'Dental', 'Riama', 'Sen', '01234', 'rima@email.com', '30/03/2017', '07:45:00', 'No'),
(35, 'Cardiology', 'Kazi', 'Sala Uddn', '0329009', 'amisalabir@gmail.com', '05/04/2017', '11:35:00', 'No'),
(36, 'Cardiology', 'Kazi', 'Fairly', '0329009', 'amisalabir@gmail.com', '04/04/2017', '01:30:00', 'No'),
(37, 'Hepatology', 'Kazi', 'Datta', '0329009', 'tusahr@gmail.com', '15/03/2017', '01:30:00', 'No'),
(38, 'Hepatology', 'Arman', 'Chowdhury', '02152366', 'tusahr@gmail.com', '05/04/2017', '11:35:00', 'No'),
(39, 'Dental', 'Kazi', 'Sala Uddn', '0329009', 'amisalabir@gmail.com', '05/04/2017', '11:35:00', 'No'),
(40, 'Dental', 'Tushar', 'Barua', '0123456', 'tushar@gmail.com', '29/03/2017', '12:06:00', 'No'),
(41, 'Hepatology', 'Tushar', 'Sala Uddn', '03290098578', 'tusahrfgss@gmail.com', '04/04/2017', '11:35:00', 'No'),
(42, 'Dental', 'Tushar', 'Barua', '0123456', 'tushar@gmail.com', '05/04/2017', '12:06:00', 'No');

-- --------------------------------------------------------

--
-- Table structure for table `bed`
--

CREATE TABLE `bed` (
  `bed_id` int(11) NOT NULL,
  `bed_number` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `type` longtext NOT NULL COMMENT 'ward,cabin,ICU',
  `status` int(11) NOT NULL DEFAULT '0' COMMENT '0=unalloted;1=alloted',
  `description` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `soft_delete` varchar(11) NOT NULL DEFAULT 'no'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `bed_allotment`
--

CREATE TABLE `bed_allotment` (
  `bed_allotment_id` int(11) NOT NULL,
  `bed_id` int(11) NOT NULL,
  `patient_id` int(11) NOT NULL,
  `allotment_timestamp` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `discharge_timestamp` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `soft_delete` varchar(11) NOT NULL DEFAULT 'no'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `blood_bank`
--

CREATE TABLE `blood_bank` (
  `blood_group_id` int(11) NOT NULL,
  `blood_group` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `status` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `soft_delete` varchar(11) NOT NULL DEFAULT 'no'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `blood_donor`
--

CREATE TABLE `blood_donor` (
  `blood_donor_id` int(11) NOT NULL,
  `name` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `blood_group` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `sex` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `age` int(11) NOT NULL,
  `phone` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `email` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `address` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `last_donation_timestamp` int(11) NOT NULL,
  `soft_delete` varchar(11) NOT NULL DEFAULT 'no'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ci_sessions`
--

CREATE TABLE `ci_sessions` (
  `id` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `ip_address` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `timestamp` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `data` blob NOT NULL,
  `soft_delete` varchar(11) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'no'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `ci_sessions`
--

INSERT INTO `ci_sessions` (`id`, `ip_address`, `timestamp`, `data`, `soft_delete`) VALUES
('56c60c399b698df777b10cdee508d48546ad8dbb', '::1', 1487491178, 0x5f5f63695f6c6173745f726567656e65726174657c693a313438373439303931393b61646d696e5f6c6f67696e7c733a313a2231223b6c6f67696e5f757365725f69647c733a313a2231223b6e616d657c733a393a224d722e2041646d696e223b6c6f67696e5f747970657c733a353a2261646d696e223b, 'no'),
('79b80f2faaa2503ede5be3a6d85da63615421285', '::1', 1487490731, 0x5f5f63695f6c6173745f726567656e65726174657c693a313438373439303539323b61646d696e5f6c6f67696e7c733a313a2231223b6c6f67696e5f757365725f69647c733a313a2231223b6e616d657c733a393a224d722e2041646d696e223b6c6f67696e5f747970657c733a353a2261646d696e223b, 'no'),
('944bf670c431fc261464c133eba4c6017ed7c598', '::1', 1487490195, 0x5f5f63695f6c6173745f726567656e65726174657c693a313438373438393838313b61646d696e5f6c6f67696e7c733a313a2231223b6c6f67696e5f757365725f69647c733a313a2231223b6e616d657c733a393a224d722e2041646d696e223b6c6f67696e5f747970657c733a353a2261646d696e223b, 'no'),
('cc013468786071cc8784170acb6b0de6555323e8', '::1', 1487490461, 0x5f5f63695f6c6173745f726567656e65726174657c693a313438373439303139363b61646d696e5f6c6f67696e7c733a313a2231223b6c6f67696e5f757365725f69647c733a313a2231223b6e616d657c733a393a224d722e2041646d696e223b6c6f67696e5f747970657c733a353a2261646d696e223b, 'no');

-- --------------------------------------------------------

--
-- Table structure for table `currency`
--

CREATE TABLE `currency` (
  `currency_id` int(11) NOT NULL,
  `currency_code` longtext COLLATE utf8_unicode_ci NOT NULL,
  `currency_symbol` longtext COLLATE utf8_unicode_ci NOT NULL,
  `currency_name` longtext COLLATE utf8_unicode_ci NOT NULL,
  `soft_delete` varchar(11) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'no'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `department`
--

CREATE TABLE `department` (
  `department_id` int(11) NOT NULL,
  `name` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `description` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `soft_delete` varchar(11) NOT NULL DEFAULT 'no'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `department`
--

INSERT INTO `department` (`department_id`, `name`, `description`, `soft_delete`) VALUES
(1, 'Cardiology', 'The Division of Cardiology at SIRIUS CARE is determined to provide compassionate, state of the art, proficient care to heart patients at an affordable cost. Our highly qualified team of cardiologists along with the latest technology ensures best possible care for a wide spectrum of heart diseases.  Our Interventional team is available round the clock to help patients with cardiac emergencies with primary and complex coronary angioplasties and Stinting. Our team of doctors, nurses, technicians and other heart experts are there to ensure unparalleled excellence in patient care, education and research.', 'no'),
(2, 'Hepatology', 'Welcome to the Division of Gastroenterology & Hepatology, where we strive to provide outstanding and compassionate patient care. We offer first-rate clinical services through the availability of a wide range of gastroenterology and hepatology subspecialty faculty and programs, including hepatology, inflammatory bowel disease (IBD), endoscopic ultrasound and ERCP, motility, and gastrointestinal cancer prevention and treatment.  The division has more than doubled in size over the past five years resulting in a marked increase in clinical volume and expansion in many areas of expertise.', 'no'),
(3, 'Dental', 'We provide complete range of dental treatments with highly experienced team of doctors. We aim in providing multidisciplinary services in dental treatments at affordable cost.\r\n\r\nServices provided at Dental department of SIRIUS CARE are- Simple to complicated Root Canals, Repeat Root Canal treatment, Root Canal Surgeries, Prosthetic Dentistry, Orthodontic treatment, Periodaontal Surgery, Dental implants, dental braces or clips, lasers, teeth whitening, tooth bleach, ceramic crowns, zirconium caps, Paediatric Dentistry.', 'no'),
(8, 'ENT', 'The Department of ENT at Peopleâ€™s Hospital is well-equipped to handle any kind of ENT emergencies and has specialized staff to handle difficult surgical situations. We track the latest developments and imbibe innovative methods for better patient care. We have a team of doctors whose credentials go beyond the ordinary.\r\n\r\nThey are expertise in handling any kind of trauma / emergency and also undertake complex surgeries on a regular basis. We treat many kinds of disorders like Deafness, Discharge in ears, Sinus infection, Tonsil infections, Patients with difficulty in swallowing, breathing and headache etc.', 'no'),
(7, 'Opthalmology', 'The Ophthalmology or eye department provides a comprehensive range of secondary and tertiary eye (ophthalmic) services for adults and children. The department has a large multi professional eye care team, and the team are able to diagnose, investigate, treat and operate (where necessary) on a range of eye conditions, including complex eye conditions.\r\nThere is a specialist eye casualty. We have a strong research background and are a major teaching and training unit for ophthalmologists.', 'no'),
(6, 'Neurology', 'The Neurology Department provides a clinical and diagnostic service for assessment and management of patients with disorders of brain, spinal cord, nerve and muscle. Our inpatient service includes a dedicated four bed acute stroke unit and multidisciplinary team of nurses and allied health professionals.\r\n\r\nOur general neurologists are committed to provide specialized care with a generalist perspective, ensuring an innovative, effective approach, and diagnosis and treatment of a wide variety of neurological conditions.', 'no'),
(9, 'Orthopedics', 'Department of Orthopedics at SIRIUS CARE provides end-to-end services and management of all kinds of bone and joint problems. We offer comprehensive orthopedic care for patients making the diagnosis, treatment and recovery phases less stressful and more efficient our patients.\r\n\r\nWe have a dedicated team of doctors available round the clock to attend to all aspects of trauma including polytrauma, compound fractures and complex injuries besides treating simple ones. We provide comprehensive care for complex orthopedic problems.', 'no'),
(10, 'Laboratory Analyst', 'The SIRIUS CARE has a multi-disciplinary, well-equipped and comprehensive laboratory. The department conducts and processes a wide range of tests and analyses, ensuring teams within the hospital have access to all the relevant information required to make diagnoses and monitor patient progress while patients undergo therapy.\r\n\r\nThe department consists of a committed team of skilled pathologists and medical technologists/technicians, with expertise in the use of sophisticated laboratory equipment.', 'no');

-- --------------------------------------------------------

--
-- Table structure for table `diagnosis_report`
--

CREATE TABLE `diagnosis_report` (
  `diagnosis_report_id` int(11) NOT NULL,
  `report_type` longtext COLLATE utf8_unicode_ci NOT NULL COMMENT 'xray,blood test',
  `document_type` longtext COLLATE utf8_unicode_ci NOT NULL COMMENT 'text,photo',
  `file_name` longtext COLLATE utf8_unicode_ci NOT NULL,
  `prescription_id` int(11) NOT NULL,
  `description` longtext COLLATE utf8_unicode_ci NOT NULL,
  `timestamp` int(11) NOT NULL,
  `laboratorist_id` int(11) NOT NULL,
  `soft_delete` varchar(11) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'no'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `doctor`
--

CREATE TABLE `doctor` (
  `doctor_id` int(11) NOT NULL,
  `name` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `email` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `password` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `address` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `phone` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `department_id` varchar(44) NOT NULL,
  `profile` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `propic` varchar(255) NOT NULL,
  `soft_delete` varchar(11) NOT NULL DEFAULT 'no',
  `email_verified` varchar(5) NOT NULL DEFAULT 'YES'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `doctor`
--

INSERT INTO `doctor` (`doctor_id`, `name`, `email`, `password`, `address`, `phone`, `department_id`, `profile`, `propic`, `soft_delete`, `email_verified`) VALUES
(5, 'Dr. Tahmina Banu', 'tahmina@email.com', 'e10adc3949ba59abbe56e057f20f883e', 'Chittagong', '0123456', 'Hepatology', 'MBBS (DMC), MRCP (UK)\r\nCo-ordinator & Consultant\r\nInternal Medicine', 'index_team2.png', 'no', 'YES'),
(6, 'Dr. Nazmul Ahsan', 'nazmul@gmail.com', '827ccb0eea8a706c4c34a16891f84e7b', 'Chittagong', '01234567', 'Dental', 'BDS, FCPS, PhD (Japan)\r\nCo-ordinator & Consultant\r\nDental Surgery', 'checkup_img2.jpg', 'no', 'YES'),
(4, 'Dr. Hasina Akhtar', 'hasina@email.com', '827ccb0eea8a706c4c34a16891f84e7b', 'Chittagong', '0123456', 'Cardiology', 'MBBS (UK), D.Card. (London)\r\nCo-ordinator & Consultant\r\nClinical Cardiology', 'index_team4.png', 'no', 'YES'),
(7, 'Dr. Iffat Ara', 'iffat@gmail.com', '827ccb0eea8a706c4c34a16891f84e7b', 'Chittagong', '012345678', 'Opthalmology', 'MBBS, FCPS, MS, MPhD ( Australia ), FICS, FCPS ( Ophthalmology )', 'index_team5.png', 'no', 'YES'),
(8, 'Professor Dr. Quazi Deen Mohammad', 'quazi@gmail.com', '827ccb0eea8a706c4c34a16891f84e7b', 'Chittagong', '013695', 'Neurology', 'MBBS, FCPS, MD (Neurology)\r\nProfessor & Director\r\nNational Institute of Neuroscience', 'Neuro.jpg', 'no', 'YES'),
(9, 'Professor Dr. A.F. Mohiuddin Khan', 'khan@gmail.com', '827ccb0eea8a706c4c34a16891f84e7b', 'Chittagong', '012584', 'ENT', ' MBBS, DLO, MS ( ENT ) &\r\nProfessor & Head of the Departnment of ENT', 'ENT.jpg', 'no', 'YES'),
(11, '     Dr. Tipu Sultan', 'tipu@gmail.com', '827ccb0eea8a706c4c34a16891f84e7b', 'Chittagong', '025813', 'Laboratory', 'MBBS. M.PHIL (Microbiology)\r\nAssistant Professor, Chittagong Medical College . ', 'Clinical-Pathology-Laboratory-CPATH-Labs-600.jpg', 'no', 'YES'),
(12, 'Professor Dr. SK. Nurul Alam', 'nalam@gmail.com', '827ccb0eea8a706c4c34a16891f84e7b', 'Chittagong', '013694', 'Orthopedics', 'Ex - Director & Professor\r\nMBBS, MS (Ortho), D Ortho Fellow Orthopaedic Surgery (Singapore)', 'orthopedics.jpg', 'no', 'YES'),
(13, 'ARIF MAHAMUD', 'admin@olineit.com', '624ae7f91e194adb95abfe2eab6c81a6', '', '', 'Hepatology', '', '', 'no', 'YES');

-- --------------------------------------------------------

--
-- Table structure for table `email_template`
--

CREATE TABLE `email_template` (
  `email_template_id` int(11) NOT NULL,
  `task` longtext COLLATE utf8_unicode_ci NOT NULL,
  `subject` longtext COLLATE utf8_unicode_ci NOT NULL,
  `body` longtext COLLATE utf8_unicode_ci NOT NULL,
  `instruction` longtext COLLATE utf8_unicode_ci NOT NULL,
  `soft_delete` varchar(11) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'no'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `form_element`
--

CREATE TABLE `form_element` (
  `form_element_id` int(11) NOT NULL,
  `type` longtext COLLATE utf8_unicode_ci NOT NULL,
  `html` longtext COLLATE utf8_unicode_ci NOT NULL,
  `soft_delete` varchar(11) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'no'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `invoice`
--

CREATE TABLE `invoice` (
  `invoice_id` int(11) NOT NULL,
  `invoice_number` longtext COLLATE utf8_unicode_ci NOT NULL,
  `patient_id` int(11) NOT NULL,
  `title` longtext COLLATE utf8_unicode_ci NOT NULL,
  `invoice_entries` longtext COLLATE utf8_unicode_ci NOT NULL,
  `creation_timestamp` longtext COLLATE utf8_unicode_ci NOT NULL,
  `due_timestamp` longtext COLLATE utf8_unicode_ci NOT NULL,
  `status` longtext COLLATE utf8_unicode_ci NOT NULL COMMENT 'paid or unpaid',
  `vat_percentage` longtext COLLATE utf8_unicode_ci NOT NULL,
  `discount_amount` longtext COLLATE utf8_unicode_ci NOT NULL,
  `soft_delete` varchar(11) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'no'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `laboratorist`
--

CREATE TABLE `laboratorist` (
  `laboratorist_id` int(11) NOT NULL,
  `name` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `email` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `password` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `address` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `phone` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `propic` varchar(33) NOT NULL,
  `soft_delete` varchar(11) NOT NULL DEFAULT 'no'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `laboratorist`
--

INSERT INTO `laboratorist` (`laboratorist_id`, `name`, `email`, `password`, `address`, `phone`, `propic`, `soft_delete`) VALUES
(2, 'afaaaaaaaa', 'akash@gmail.com', '827ccb0eea8a706c4c34a16891f84e7b', 'aljvlavljaljjl', '0153336633', 'bdurjoy.png', 'no'),
(3, 'asif', 'asif@gmail.com', '827ccb0eea8a706c4c34a16891f84e7b', 'chittagong university', '0181755666', 'akash.jpg', 'no');

-- --------------------------------------------------------

--
-- Table structure for table `language`
--

CREATE TABLE `language` (
  `phrase_id` int(11) NOT NULL,
  `phrase` longtext COLLATE utf8_unicode_ci NOT NULL,
  `english` longtext COLLATE utf8_unicode_ci NOT NULL,
  `soft_delete` varchar(11) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'no'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `medicine`
--

CREATE TABLE `medicine` (
  `medicine_id` int(11) NOT NULL,
  `name` longtext COLLATE utf8_unicode_ci NOT NULL,
  `medicine_category_id` int(11) NOT NULL,
  `description` longtext COLLATE utf8_unicode_ci NOT NULL,
  `price` longtext COLLATE utf8_unicode_ci NOT NULL,
  `manufacturing_company` longtext COLLATE utf8_unicode_ci NOT NULL,
  `status` longtext COLLATE utf8_unicode_ci NOT NULL,
  `soft_delete` varchar(11) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'no'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `medicine_category`
--

CREATE TABLE `medicine_category` (
  `medicine_category_id` int(11) NOT NULL,
  `name` longtext COLLATE utf8_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8_unicode_ci NOT NULL,
  `soft_delete` varchar(11) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'no'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `message`
--

CREATE TABLE `message` (
  `message_id` int(11) NOT NULL,
  `message_thread_code` longtext NOT NULL,
  `message` longtext NOT NULL,
  `sender` longtext NOT NULL,
  `timestamp` longtext NOT NULL,
  `read_status` int(11) NOT NULL DEFAULT '0' COMMENT '0 unread 1 read',
  `soft_delete` varchar(11) NOT NULL DEFAULT 'no'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `message_thread`
--

CREATE TABLE `message_thread` (
  `message_thread_id` int(11) NOT NULL,
  `message_thread_code` longtext COLLATE utf8_unicode_ci NOT NULL,
  `sender` longtext COLLATE utf8_unicode_ci NOT NULL,
  `reciever` longtext COLLATE utf8_unicode_ci NOT NULL,
  `last_message_timestamp` longtext COLLATE utf8_unicode_ci NOT NULL,
  `soft_delete` varchar(11) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'no'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `note`
--

CREATE TABLE `note` (
  `note_id` int(11) NOT NULL,
  `note` longtext COLLATE utf8_unicode_ci NOT NULL,
  `user_type` longtext COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `color` longtext COLLATE utf8_unicode_ci NOT NULL,
  `timestamp_create` longtext COLLATE utf8_unicode_ci NOT NULL,
  `timestamp_last_update` longtext COLLATE utf8_unicode_ci NOT NULL,
  `soft_delete` varchar(11) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'no'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `notice`
--

CREATE TABLE `notice` (
  `notice_id` int(11) NOT NULL,
  `title` longtext COLLATE utf8_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8_unicode_ci NOT NULL,
  `start_timestamp` longtext COLLATE utf8_unicode_ci NOT NULL,
  `end_timestamp` longtext COLLATE utf8_unicode_ci NOT NULL,
  `soft_delete` varchar(11) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'no'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `noticeboard`
--

CREATE TABLE `noticeboard` (
  `notice_id` int(11) NOT NULL,
  `notice_title` longtext COLLATE utf8_unicode_ci NOT NULL,
  `notice` longtext COLLATE utf8_unicode_ci NOT NULL,
  `create_timestamp` int(11) NOT NULL,
  `soft_delete` varchar(11) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'no'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `nurse`
--

CREATE TABLE `nurse` (
  `nurse_id` int(11) NOT NULL,
  `name` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `email` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `password` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `address` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `phone` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `propic` varchar(33) NOT NULL,
  `soft_delete` varchar(11) NOT NULL DEFAULT 'no'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `nurse`
--

INSERT INTO `nurse` (`nurse_id`, `name`, `email`, `password`, `address`, `phone`, `propic`, `soft_delete`) VALUES
(1, 'ajlkjflkjmlv', 'dfjlakj@gmail.com', '827ccb0eea8a706c4c34a16891f84e7b', 'aljvkmakm', '02121465544', 'Logo-A2.png', 'no');

-- --------------------------------------------------------

--
-- Table structure for table `patient`
--

CREATE TABLE `patient` (
  `patient_id` int(11) NOT NULL,
  `name` longtext COLLATE utf8_unicode_ci NOT NULL,
  `email` longtext COLLATE utf8_unicode_ci NOT NULL,
  `password` longtext COLLATE utf8_unicode_ci NOT NULL,
  `address` longtext COLLATE utf8_unicode_ci NOT NULL,
  `phone` longtext COLLATE utf8_unicode_ci NOT NULL,
  `sex` longtext COLLATE utf8_unicode_ci NOT NULL,
  `birth_date` longtext COLLATE utf8_unicode_ci NOT NULL,
  `age` int(11) NOT NULL,
  `blood_group` longtext COLLATE utf8_unicode_ci NOT NULL,
  `account_opening_timestamp` int(11) NOT NULL,
  `propic` varchar(111) COLLATE utf8_unicode_ci NOT NULL,
  `soft_delete` varchar(11) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'no'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `payment`
--

CREATE TABLE `payment` (
  `payment_id` int(11) NOT NULL,
  `type` longtext COLLATE utf8_unicode_ci NOT NULL COMMENT 'income expense',
  `amount` longtext COLLATE utf8_unicode_ci NOT NULL,
  `title` longtext COLLATE utf8_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8_unicode_ci NOT NULL,
  `payment_method` longtext COLLATE utf8_unicode_ci NOT NULL,
  `invoice_number` longtext COLLATE utf8_unicode_ci NOT NULL,
  `timestamp` longtext COLLATE utf8_unicode_ci NOT NULL,
  `soft_delete` varchar(11) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'no'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `pharmacist`
--

CREATE TABLE `pharmacist` (
  `pharmacist_id` int(11) NOT NULL,
  `name` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `email` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `password` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `address` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `phone` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `propic` varchar(133) NOT NULL,
  `soft_delete` varchar(11) NOT NULL DEFAULT 'no'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pharmacist`
--

INSERT INTO `pharmacist` (`pharmacist_id`, `name`, `email`, `password`, `address`, `phone`, `propic`, `soft_delete`) VALUES
(1, 'afkjafldj', 'fladj@gmail.com', '827ccb0eea8a706c4c34a16891f84e7b', 'flajdkfl', '2145646532', '', 'no');

-- --------------------------------------------------------

--
-- Table structure for table `prescription`
--

CREATE TABLE `prescription` (
  `prescription_id` int(11) NOT NULL,
  `timestamp` longtext COLLATE utf8_unicode_ci NOT NULL,
  `doctor_id` int(11) NOT NULL,
  `patient_id` int(11) NOT NULL,
  `case_history` longtext COLLATE utf8_unicode_ci NOT NULL,
  `medication` longtext COLLATE utf8_unicode_ci NOT NULL,
  `note` longtext COLLATE utf8_unicode_ci NOT NULL,
  `soft_delete` varchar(11) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'no'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `receptionist`
--

CREATE TABLE `receptionist` (
  `receptionist_id` int(11) NOT NULL,
  `name` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `email` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `password` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `address` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `phone` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `propic` varchar(133) NOT NULL,
  `soft_delete` varchar(11) NOT NULL DEFAULT 'no',
  `email_verified` varchar(5) NOT NULL DEFAULT 'YES'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `receptionist`
--

INSERT INTO `receptionist` (`receptionist_id`, `name`, `email`, `password`, `address`, `phone`, `propic`, `soft_delete`, `email_verified`) VALUES
(1, 'Arman', 'arman@gmail.com', '827ccb0eea8a706c4c34a16891f84e7b', 'vjlakjriijeoi', '06546543132', 'r_logo.png', 'no', 'YES');

-- --------------------------------------------------------

--
-- Table structure for table `report`
--

CREATE TABLE `report` (
  `report_id` int(11) NOT NULL,
  `type` longtext COLLATE utf8_unicode_ci NOT NULL COMMENT 'operation,birth,death',
  `description` longtext COLLATE utf8_unicode_ci NOT NULL,
  `timestamp` longtext COLLATE utf8_unicode_ci NOT NULL,
  `doctor_id` int(11) NOT NULL,
  `patient_id` int(11) NOT NULL,
  `soft_delete` varchar(11) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'no'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `settings_id` int(11) NOT NULL,
  `type` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `description` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `soft_delete` varchar(11) NOT NULL DEFAULT 'no'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`settings_id`, `type`, `description`, `soft_delete`) VALUES
(1, 'system_name', 'Hospital Management System', 'no'),
(2, 'system_title', 'Hospital Management System', 'no'),
(3, 'address', 'Chittagong, Bangladesh', 'no'),
(4, 'phone', '+8012654159', 'no'),
(5, 'paypal_email', 'payment@teamririus.org', 'no'),
(6, 'currency', 'BDT', 'no'),
(7, 'system_email', 'admin@teamririus.org', 'no'),
(8, 'buyer', '', 'no'),
(9, 'purchase_code', '', 'no'),
(11, 'language', 'english', 'no'),
(12, 'text_align', 'left-to-right', 'no'),
(13, 'system_currency_id', '1', 'no'),
(14, 'clickatell_user', '[YOUR CLICKATELL USERNAME]', 'no'),
(15, 'clickatell_password', '[YOUR CLICKATELL PASSWORD]', 'no'),
(16, 'clickatell_api_id', '[YOUR CLICKATELL API ID]', 'no');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `accountant`
--
ALTER TABLE `accountant`
  ADD PRIMARY KEY (`accountant_id`);

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`admin_id`);

--
-- Indexes for table `appointment`
--
ALTER TABLE `appointment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bed`
--
ALTER TABLE `bed`
  ADD PRIMARY KEY (`bed_id`);

--
-- Indexes for table `bed_allotment`
--
ALTER TABLE `bed_allotment`
  ADD PRIMARY KEY (`bed_allotment_id`);

--
-- Indexes for table `blood_bank`
--
ALTER TABLE `blood_bank`
  ADD PRIMARY KEY (`blood_group_id`);

--
-- Indexes for table `blood_donor`
--
ALTER TABLE `blood_donor`
  ADD PRIMARY KEY (`blood_donor_id`);

--
-- Indexes for table `ci_sessions`
--
ALTER TABLE `ci_sessions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ci_sessions_timestamp` (`timestamp`);

--
-- Indexes for table `currency`
--
ALTER TABLE `currency`
  ADD PRIMARY KEY (`currency_id`);

--
-- Indexes for table `department`
--
ALTER TABLE `department`
  ADD PRIMARY KEY (`department_id`);

--
-- Indexes for table `diagnosis_report`
--
ALTER TABLE `diagnosis_report`
  ADD PRIMARY KEY (`diagnosis_report_id`);

--
-- Indexes for table `doctor`
--
ALTER TABLE `doctor`
  ADD PRIMARY KEY (`doctor_id`);

--
-- Indexes for table `email_template`
--
ALTER TABLE `email_template`
  ADD PRIMARY KEY (`email_template_id`);

--
-- Indexes for table `form_element`
--
ALTER TABLE `form_element`
  ADD PRIMARY KEY (`form_element_id`);

--
-- Indexes for table `invoice`
--
ALTER TABLE `invoice`
  ADD PRIMARY KEY (`invoice_id`);

--
-- Indexes for table `laboratorist`
--
ALTER TABLE `laboratorist`
  ADD PRIMARY KEY (`laboratorist_id`);

--
-- Indexes for table `language`
--
ALTER TABLE `language`
  ADD PRIMARY KEY (`phrase_id`);

--
-- Indexes for table `medicine`
--
ALTER TABLE `medicine`
  ADD PRIMARY KEY (`medicine_id`);

--
-- Indexes for table `medicine_category`
--
ALTER TABLE `medicine_category`
  ADD PRIMARY KEY (`medicine_category_id`);

--
-- Indexes for table `message`
--
ALTER TABLE `message`
  ADD PRIMARY KEY (`message_id`);

--
-- Indexes for table `message_thread`
--
ALTER TABLE `message_thread`
  ADD PRIMARY KEY (`message_thread_id`);

--
-- Indexes for table `note`
--
ALTER TABLE `note`
  ADD PRIMARY KEY (`note_id`);

--
-- Indexes for table `notice`
--
ALTER TABLE `notice`
  ADD PRIMARY KEY (`notice_id`);

--
-- Indexes for table `noticeboard`
--
ALTER TABLE `noticeboard`
  ADD PRIMARY KEY (`notice_id`);

--
-- Indexes for table `nurse`
--
ALTER TABLE `nurse`
  ADD PRIMARY KEY (`nurse_id`);

--
-- Indexes for table `patient`
--
ALTER TABLE `patient`
  ADD PRIMARY KEY (`patient_id`);

--
-- Indexes for table `payment`
--
ALTER TABLE `payment`
  ADD PRIMARY KEY (`payment_id`);

--
-- Indexes for table `pharmacist`
--
ALTER TABLE `pharmacist`
  ADD PRIMARY KEY (`pharmacist_id`);

--
-- Indexes for table `prescription`
--
ALTER TABLE `prescription`
  ADD PRIMARY KEY (`prescription_id`);

--
-- Indexes for table `receptionist`
--
ALTER TABLE `receptionist`
  ADD PRIMARY KEY (`receptionist_id`);

--
-- Indexes for table `report`
--
ALTER TABLE `report`
  ADD PRIMARY KEY (`report_id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`settings_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `accountant`
--
ALTER TABLE `accountant`
  MODIFY `accountant_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `admin_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `appointment`
--
ALTER TABLE `appointment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;
--
-- AUTO_INCREMENT for table `bed`
--
ALTER TABLE `bed`
  MODIFY `bed_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `bed_allotment`
--
ALTER TABLE `bed_allotment`
  MODIFY `bed_allotment_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `blood_bank`
--
ALTER TABLE `blood_bank`
  MODIFY `blood_group_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `blood_donor`
--
ALTER TABLE `blood_donor`
  MODIFY `blood_donor_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `currency`
--
ALTER TABLE `currency`
  MODIFY `currency_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `department`
--
ALTER TABLE `department`
  MODIFY `department_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `diagnosis_report`
--
ALTER TABLE `diagnosis_report`
  MODIFY `diagnosis_report_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `doctor`
--
ALTER TABLE `doctor`
  MODIFY `doctor_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `email_template`
--
ALTER TABLE `email_template`
  MODIFY `email_template_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `form_element`
--
ALTER TABLE `form_element`
  MODIFY `form_element_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `invoice`
--
ALTER TABLE `invoice`
  MODIFY `invoice_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `laboratorist`
--
ALTER TABLE `laboratorist`
  MODIFY `laboratorist_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `language`
--
ALTER TABLE `language`
  MODIFY `phrase_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `medicine`
--
ALTER TABLE `medicine`
  MODIFY `medicine_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `medicine_category`
--
ALTER TABLE `medicine_category`
  MODIFY `medicine_category_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `message`
--
ALTER TABLE `message`
  MODIFY `message_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `message_thread`
--
ALTER TABLE `message_thread`
  MODIFY `message_thread_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `note`
--
ALTER TABLE `note`
  MODIFY `note_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `notice`
--
ALTER TABLE `notice`
  MODIFY `notice_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `noticeboard`
--
ALTER TABLE `noticeboard`
  MODIFY `notice_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `nurse`
--
ALTER TABLE `nurse`
  MODIFY `nurse_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `patient`
--
ALTER TABLE `patient`
  MODIFY `patient_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `payment`
--
ALTER TABLE `payment`
  MODIFY `payment_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pharmacist`
--
ALTER TABLE `pharmacist`
  MODIFY `pharmacist_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `prescription`
--
ALTER TABLE `prescription`
  MODIFY `prescription_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `receptionist`
--
ALTER TABLE `receptionist`
  MODIFY `receptionist_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `report`
--
ALTER TABLE `report`
  MODIFY `report_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `settings_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
