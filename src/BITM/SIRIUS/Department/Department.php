<?php
/**
 * Created by PhpStorm.
 * User: durjoy
 * Date: 20-Feb-17
 * Time: 11:34 PM
 */

namespace App\Department;

use App\Message\Message;
use App\Utility\Utility;
use App\Model\Database as DB;
use PDO;

class Department extends DB
{
    private $id;
    private $name;
    private $description;

    public function setData($postdata=null){

        if(array_key_exists('id', $postdata)){
            $this->id=$postdata['id'];
        }
        if(array_key_exists('name', $postdata)){
            $this->name=$postdata['name'];
        }
        if(array_key_exists('description', $postdata)){
            $this->description=$postdata['description'];
        }

    }




    public function store() {


        $dataArray= array(':name'=>$this->name,':description'=>$this->description);


        $query="INSERT INTO `hms_sirius`.`department` (`name`, description) VALUES (:name, :description)";

        $STH=$this->DBH->prepare($query);

        $result = $STH->execute($dataArray);

        if ($result) {
            Message::message("
                <div class=\"alert alert-success\">
                            <strong>Success!</strong> Data has been stored successfully.
                </div>");
            Utility::redirect($_SERVER['HTTP_REFERER']);
        } else {
            Message::message("
                <div class=\"alert alert-danger\">
                            <strong>Failed!</strong> Data has not been stored successfully.
                </div>");
            Utility::redirect($_SERVER['HTTP_REFERER']);
        }
    }


    public function index(){
        $query="SELECT * From department WHERE soft_delete='NO'";
        $STH = $this->DBH->query($query);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetchAll();
    }

    public function section(){
        $query = "SELECT * From department";
        $STH = $this->DBH->query($query);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetchAll();
    }

    public function view(){
        $query=" SELECT * FROM department";
        // Utility::dd($query);
        $STH =$this->DBH->query($query);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetch();

    }// end of view()

    /*
        public function validTokenUpdate(){
            $query="UPDATE `hms_sirius`.`accountant` SET  `email_verified`='".'Yes'."' WHERE `users`.`email` ='$this->email'";
            $result=$this->DBH->prepare($query);
            $result->execute();

            if($result){
                Message::message("
                 <div class=\"alert alert-success\">
                 <strong>Success!</strong> Email verification has been successful. Please login now!
                  </div>");
            }
            else {
                echo "Error";
            }
            return Utility::redirect('../../../../views/SEIPXXXX/User/Profile/signup.php');
        }*/
}